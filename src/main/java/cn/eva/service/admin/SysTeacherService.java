package cn.eva.service.admin;

import java.util.List;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaTeacher;
import cn.eva.entity.EvaTeacherVo;

/*
 * @author wzh
 * @date 2018-09-07
 * 
 */

public interface SysTeacherService {
	
	/**
     * 添加教师
     * @param  teacher
     * @return
     */
	public ResultJosn add(EvaTeacher teacher);
	
	/**
     * 根据id删除教师
     * @param tId
     * @return
     */
	public ResultJosn delete(List<String> tId);
	
	/**
     * 修改教师信息
     * @param teacher
     * @return
     */
	public ResultJosn update(EvaTeacher teacher);
		
	/**
     * 查询所有教师 传入分页数据
     * @param evaTeacher
     * @param page
     * @param pageSize
     * @return
     */
	public LayuiTableResult select(EvaTeacherVo evaTeacherVo, Integer page, Integer pageSize);
	
	/**
     * 根据id查询单个教师
     * @param tId
     * @return
     */
	public ResultJosn selectBytId(String tId);

}
