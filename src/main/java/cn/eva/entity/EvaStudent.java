/*
*
* EvaStudent.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.entity;

import java.util.Date;

public class EvaStudent {
    /**
     * 学生唯一标识
     */
    private String sId;

    /**
     * 学生学号
     */
    private String sNo;

    /**
     * 学生姓名
     */
    private String sName;

    /**
     * 密码
     */
    private String sPass;

    /**
     * 学生性别（1男0女）
     */
    private String sSex;

    /**
     * 学生类型（1在校2退学3休学）
     */
    private String sType;

    /**
     * 关联班级id
     */
    private String classId;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 学生唯一标识
     * @return s_id 学生唯一标识
     */
    public String getsId() {
        return sId;
    }

    /**
     * 学生唯一标识
     * @param sId 学生唯一标识
     */
    public void setsId(String sId) {
        this.sId = sId == null ? null : sId.trim();
    }

    /**
     * 学生学号
     * @return s_no 学生学号
     */
    public String getsNo() {
        return sNo;
    }

    /**
     * 学生学号
     * @param sNo 学生学号
     */
    public void setsNo(String sNo) {
        this.sNo = sNo == null ? null : sNo.trim();
    }

    /**
     * 学生姓名
     * @return s_name 学生姓名
     */
    public String getsName() {
        return sName;
    }

    /**
     * 学生姓名
     * @param sName 学生姓名
     */
    public void setsName(String sName) {
        this.sName = sName == null ? null : sName.trim();
    }

    /**
     * 密码
     * @return s_pass 密码
     */
    public String getsPass() {
        return sPass;
    }

    /**
     * 密码
     * @param sPass 密码
     */
    public void setsPass(String sPass) {
        this.sPass = sPass == null ? null : sPass.trim();
    }

    /**
     * 学生性别（1男0女）
     * @return s_sex 学生性别（1男0女）
     */
    public String getsSex() {
        return sSex;
    }

    /**
     * 学生性别（1男0女）
     * @param sSex 学生性别（1男0女）
     */
    public void setsSex(String sSex) {
        this.sSex = sSex == null ? null : sSex.trim();
    }

    /**
     * 学生类型（1在校2退学3休学）
     * @return s_type 学生类型（1在校2退学3休学）
     */
    public String getsType() {
        return sType;
    }

    /**
     * 学生类型（1在校2退学3休学）
     * @param sType 学生类型（1在校2退学3休学）
     */
    public void setsType(String sType) {
        this.sType = sType == null ? null : sType.trim();
    }

    /**
     * 关联班级id
     * @return class_id 关联班级id
     */
    public String getClassId() {
        return classId;
    }

    /**
     * 关联班级id
     * @param classId 关联班级id
     */
    public void setClassId(String classId) {
        this.classId = classId == null ? null : classId.trim();
    }

    /**
     * 
     * @return create_time 
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 
     * @param createTime 
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 
     * @return update_time 
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 
     * @param updateTime 
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}