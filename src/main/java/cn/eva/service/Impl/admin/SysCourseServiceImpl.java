package cn.eva.service.Impl.admin;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaCourseMapper;
import cn.eva.entity.EvaCourse;
import cn.eva.example.EvaCourseExample;
import cn.eva.service.admin.SysCourseService;

@Service
public class SysCourseServiceImpl implements SysCourseService {
    @Autowired
    EvaCourseMapper evaCourseMapper;

    /**
     * 增加课程
     * @param course
     * @return
     */
    @Override
    @Transactional
    public ResultJosn add(EvaCourse course) {
        if (null != this.checkCourseId(course.getcId())) {
            throw new IllegalArgumentException(course.getcId() + "的课程编号已存在");
        }
        if (null != this.checkCourseName(course.getcName())) {
            throw new IllegalArgumentException(course.getcName() + "的课程名称已存在");
        }

        /*course.setcId(UUIDUtils.getUUID());*/ //这里数据库设计得id是实体数字，如课程编号201513025，所以不用复合码
        EvaCourse ul = (EvaCourse) course;
        ul.setcId(course.getcId());
        ul.setcName(course.getcName());
        ul.setcType(course.getcType());
        evaCourseMapper.insertSelective(ul);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), null);
    }

    @Override
    @Transactional
    public ResultJosn delete(List<String> cId) {
        if (CollectionUtils.isEmpty(cId)) {
            throw new IllegalArgumentException("参数错误");
        }
        EvaCourseExample example = new EvaCourseExample();
        example.createCriteria().andCIdIn(cId);
        evaCourseMapper.deleteByExample(example);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "删除成功");

    }

    @Override
    @Transactional
    public ResultJosn update(EvaCourse course) {
        if (StringUtils.isBlank(course.getcId())) {
            throw new IllegalArgumentException("cId错误");
        }
        EvaCourse check = this.checkCourseName(course.getcName());
        if (null != check && !(check.getcId()).equals(course.getcId())) {
            throw new IllegalArgumentException("课程名已存在");
        }
        evaCourseMapper.updateByPrimaryKeySelective(course);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "修改成功");
    }

    //课程名称模糊查找
    @Override
    public LayuiTableResult select(EvaCourse course, Integer page, Integer pageSize) {
        LayuiTableResult resultJosn = new LayuiTableResult();
        //构造条件
        EvaCourseExample example = new EvaCourseExample();
        example.setOrderByClause("created_time DESC");
        EvaCourseExample.Criteria criteria = example.createCriteria();
        //根据用户输入的课程id模糊查找
        if (StringUtils.isNotBlank(course.getcId())) {
            criteria.andCIdLike("%" + course.getcId() + "%");
        }
        //根据用户输入的课程名称模糊查找
        if (StringUtils.isNotBlank(course.getcName())) {
            criteria.andCNameLike("%" + course.getcName() + "%");
        }

        //开启分页查找
        PageHelper.startPage(page, pageSize);
        List<EvaCourse> list = evaCourseMapper.selectByExample(example);
        PageInfo<EvaCourse> pageInfo = new PageInfo<>(list, pageSize);
        //设置返回前台的格式和数据，如code=0/success，message，查询的总数total，和数据集合data
        resultJosn.setCode(HttpStatus.OK.value());
        resultJosn.setMessage(HttpStatus.OK.getReasonPhrase());
        resultJosn.setCount(pageInfo.getTotal());
        resultJosn.setData(pageInfo.getList());
        return resultJosn;
    }

    @Override
    public ResultJosn selectByid(String cId) {
        if (StringUtils.isBlank(cId)) {
            throw new IllegalArgumentException("cId错误");
        }
        EvaCourse result = evaCourseMapper.selectByPrimaryKey(cId);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), result);
    }

    /**
     * 根据cId查找是否重复
     * @param cId
     * @return
     */
    public EvaCourse checkCourseId(String cId) {
        EvaCourseExample example = new EvaCourseExample();
        EvaCourseExample.Criteria criteria = example.createCriteria();
        criteria.andCIdEqualTo(cId);
        List<EvaCourse> list = evaCourseMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return (EvaCourse) list.get(0);
    }

    /**
     * 根据name查找是否重复
     * @param  cName
     * @return
     */
    public EvaCourse checkCourseName(String cName) {
        EvaCourseExample example = new EvaCourseExample();
        EvaCourseExample.Criteria criteria = example.createCriteria();
        criteria.andCNameEqualTo(cName);
        List<EvaCourse> list = evaCourseMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return (EvaCourse) list.get(0);
    }

}
