package cn.eva.dto;

import java.util.List;

import cn.eva.entity.SysMenu;
import cn.eva.entity.SysRole;

public class SysRoleSelect extends SysRole {

    private static final long serialVersionUID = -3667665753132341101L;

    private List<SysMenu> menus;

    public List<SysMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<SysMenu> menus) {
        this.menus = menus;
    }

}