package cn.eva.dto;

import java.util.List;

import cn.eva.entity.EvaManager;
import io.swagger.annotations.ApiModelProperty;

/**
 * 后台用户添加vo
 * Copyright (c) 2017 by 
 * @ClassName: SystemUserAdd.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月7日 下午6:26:22
 */
public class SysManagerAdd extends EvaManager {

    private static final long serialVersionUID = 5930867791570860196L;

    @ApiModelProperty(value = "角色id集合", name = "roleIds", example = "[1,2,3]")
    private List<Long> roleIds;

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }
}