package cn.eva.dto;

import cn.eva.entity.SysMenu;

import java.util.List;

public class SysCourseSelect {
    private static final long serialVersionUID = -3667665753132341101L;

    private List<SysMenu> menus;

    public List<SysMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<SysMenu> menus) {
        this.menus = menus;
    }
}
