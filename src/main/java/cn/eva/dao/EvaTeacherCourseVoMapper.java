package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.eva.entity.EvaClassCourseVo;
import cn.eva.entity.EvaTeacherCourseVo;

@Mapper
public interface EvaTeacherCourseVoMapper {
    // 同部门
    List<EvaTeacherCourseVo> selectIndpWithMeMH(EvaTeacherCourseVo teacherCourseVo);

    // 自己信息
    List<EvaTeacherCourseVo> selectIndpMyselfMH(EvaTeacherCourseVo teacherCourseVo);

    // 自己的课程
    List<EvaClassCourseVo> selectWhthmeCourses(String t_id);
}
