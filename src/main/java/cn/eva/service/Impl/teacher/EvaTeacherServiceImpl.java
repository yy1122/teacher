package cn.eva.service.Impl.teacher;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaScoreTehMapper;
import cn.eva.dao.EvaStudentMapper;
import cn.eva.dao.EvaTeacherCourseVoMapper;
import cn.eva.dao.EvaTeacherMapper;
import cn.eva.entity.EvaClassCourseVo;
import cn.eva.entity.EvaEvaluate;
import cn.eva.entity.EvaScoreTeh;
import cn.eva.entity.EvaScoreTehVo;
import cn.eva.entity.EvaStudent;
import cn.eva.entity.EvaTeacher;
import cn.eva.entity.EvaTeacherCourseVo;
import cn.eva.example.EvaScoreTehExample;
import cn.eva.example.EvaStudentExample;
import cn.eva.service.EvaEvaluateMapService;
import cn.eva.service.teacher.EvaTeacherService;
import cn.eva.utils.ScoreStuUtils;

@Service
public class EvaTeacherServiceImpl implements EvaTeacherService {
    @Autowired
    private EvaEvaluateMapService evaEvaluateMapService;
    @Autowired
    private EvaTeacherMapper evaTeacherMapper; // 老师
    @Autowired
    private EvaTeacherCourseVoMapper eTCVoMapper; // 老师课程
    @Autowired
    private EvaScoreTehMapper eScoreTehMapper; // 计分_老师
    @Autowired
    private EvaStudentMapper evaStudentMapper;// 学生

    /**
     * 查询和当前登录老师课程信息
     */
    @Override
    public ResultJosn selectWithMeCourses(String t_id) {
        if (StringUtils.isBlank(t_id)) {
            throw new IllegalArgumentException("用户UUID错误");
        }
        // 根据t_id查询数据信息
        List<EvaClassCourseVo> list = eTCVoMapper.selectWhthmeCourses(t_id);
        // 添加人数信息
        for (EvaClassCourseVo evaClassCourseVo : list) {
            evaClassCourseVo.setSumStudnet(selectcum(evaClassCourseVo.getClassId()));
        }
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), list);
    }

    /*
     * 查询和当前登录老师相同系（部门）的老师信息、 模糊查询查询和当前登录老师相同系（部门）的老师信息
     */
    @Override
    @Transactional
    public ResultJosn selectAllTcAndalikedpMH(EvaTeacherCourseVo eTeacherCourseVo) {
        if (StringUtils.isBlank(eTeacherCourseVo.gettId())) {
            throw new IllegalArgumentException("用户UUID错误");
        }

        // 根据系统登录的userUUID 获取该教师的部门号
        EvaTeacher result = evaTeacherMapper.selectByPrimaryKey(eTeacherCourseVo.gettId());
        // new 一个teacherCourseVo对象将需要的信息存进去
        EvaTeacherCourseVo teacherCourseVo = new EvaTeacherCourseVo();
        teacherCourseVo.settId(eTeacherCourseVo.gettId());
        teacherCourseVo.setdId(result.getdId());
        if (eTeacherCourseVo.gettNo() == null) {
            teacherCourseVo.settNo(null);
        } else {
            teacherCourseVo.settNo("%" + eTeacherCourseVo.gettNo() + "%");
        }
        if (eTeacherCourseVo.gettName() == null) {
            teacherCourseVo.settName(null);
        } else {
            teacherCourseVo.settName("%" + eTeacherCourseVo.gettName() + "%");
        }
        if (eTeacherCourseVo.getcName() == null) {
            teacherCourseVo.setcName(null);
        } else {
            teacherCourseVo.setcName("%" + eTeacherCourseVo.getcName() + "%");
        }
        // 获取题目的及权重
        Map<Integer, EvaEvaluate> selectAll = evaEvaluateMapService.selectAll();
        // 根据部门查询该部门的所有信息
        List<EvaTeacherCourseVo> list = eTCVoMapper.selectIndpWithMeMH(teacherCourseVo);
        // 计算分数
        for (EvaTeacherCourseVo eTCV : list) {
            eTCV.setScoresT(ScoreStuUtils.calculateScore(eTCV.getScoresT(), selectAll));
        }
        // 返回结果集list
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), list);
    }

    /**
     * 查询自己信息、 模糊查询查询和当前登录老师信息
     */
    @Override
    @Transactional
    public ResultJosn selectMyselfTcAndalikedpMH(EvaTeacherCourseVo eTeacherCourseVo) {
        if (StringUtils.isBlank(eTeacherCourseVo.gettId())) {
            throw new IllegalArgumentException("用户UUID错误");
        }
        // new 一个teacherCourseVo对象将需要的信息存进去
        EvaTeacherCourseVo teacherCourseVo = new EvaTeacherCourseVo();
        teacherCourseVo.settId(eTeacherCourseVo.gettId());
        if (eTeacherCourseVo.gettNo() == null) {
            teacherCourseVo.settNo(null);
        } else {
            teacherCourseVo.settNo("%" + eTeacherCourseVo.gettNo() + "%");
        }
        if (eTeacherCourseVo.gettName() == null) {
            teacherCourseVo.settName(null);
        } else {
            teacherCourseVo.settName("%" + eTeacherCourseVo.gettName() + "%");
        }
        if (eTeacherCourseVo.getcName() == null) {
            teacherCourseVo.setcName(null);
        } else {
            teacherCourseVo.setcName("%" + eTeacherCourseVo.getcName() + "%");
        }
        // 获取题目的及权重
        Map<Integer, EvaEvaluate> selectAllMapEavEva = evaEvaluateMapService.selectAll();
        // 根据部门查询该部门的所有信息
        List<EvaTeacherCourseVo> list = eTCVoMapper.selectIndpMyselfMH(teacherCourseVo);
        // 计算分数
        for (EvaTeacherCourseVo eTCV : list) {
            eTCV.setScoresT(ScoreStuUtils.calculateScore(eTCV.getScoresT(), selectAllMapEavEva));
        }
        // 返回结果集list
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), list);
    }

    /**
     * 查询单条登录用户的评论详情
     */
    @Override
    @Transactional
    public ResultJosn selectOneDetail(EvaScoreTehVo evaScoreTehVo) {
        /**
         * 查询单表
         */
        EvaScoreTehExample tehExample = new EvaScoreTehExample();
        tehExample.createCriteria().andTIdOneEqualTo(evaScoreTehVo.gettIdOne())
                .andTIdTwoEqualTo(evaScoreTehVo.gettIdTwo()).andCIdEqualTo(evaScoreTehVo.getcId());
        // 查询数据
        List<EvaScoreTeh> list = eScoreTehMapper.selectByExample(tehExample);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), list);
    }

    /**
     * 老师登录，打分提交到数据库
     */
    @Override
    public ResultJosn insertByGrade(EvaScoreTehVo evaScoreTehVo) {
        // 插入数据
        eScoreTehMapper.insert(evaScoreTehVo);

        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), evaScoreTehVo);
    }

    /*
     * 根据classId查询班级人数
     */
    public String selectcum(String class_id) {
        EvaStudentExample studentExample = new EvaStudentExample();
        studentExample.createCriteria().andClassIdEqualTo(class_id);
        List<EvaStudent> list = evaStudentMapper.selectByExample(studentExample);
        return String.valueOf(list.size());
    }

    /*
     * 根据classId查询班级详情
     */
    @Override
    public ResultJosn selectsMyStudents(Integer page, Integer pageSize, String classId) {
        if (StringUtils.isAllBlank(classId)) {
            throw new IllegalArgumentException("该课程没有班级上课");
        }
        EvaStudentExample studentExample = new EvaStudentExample();
        studentExample.createCriteria().andClassIdEqualTo(classId);
        // 开启分页查找
        PageHelper.startPage(page, pageSize);

        List<EvaStudent> list = evaStudentMapper.selectByExample(studentExample);
        if (list.size() == 0) {
            throw new IllegalArgumentException("该课程没有班级上课");
        }
        PageInfo<EvaStudent> pageInfo = new PageInfo<>(list, pageSize);

        // 设置返回前台的格式和数据，如code=0/success，message，查询的总数total，和数据集合data
        LayuiTableResult resultJosn = new LayuiTableResult();
        resultJosn.setCode(HttpStatus.OK.value());
        resultJosn.setMessage(HttpStatus.OK.getReasonPhrase());
        resultJosn.setCount(pageInfo.getTotal());
        resultJosn.setData(pageInfo.getList());
        return resultJosn;
    }

}
