/*
*
* EvaEvaluateMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-29
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaEvaluate;
import cn.eva.example.EvaEvaluateExample;

@Mapper
public interface EvaEvaluateMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaEvaluateExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaEvaluateExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer eId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaEvaluate record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaEvaluate record);

    /**
     *
     * @mbg.generated
     */
    List<EvaEvaluate> selectByExample(EvaEvaluateExample example);

    /**
     *
     * @mbg.generated
     */
    EvaEvaluate selectByPrimaryKey(Integer eId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaEvaluate record, @Param("example") EvaEvaluateExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaEvaluate record, @Param("example") EvaEvaluateExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaEvaluate record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaEvaluate record);
}