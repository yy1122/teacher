package cn.eva.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import cn.eva.dto.LoginUser;

/**
 * 用户工具类
 * Copyright (c) 2017 by 
 * @ClassName: UserUtils.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月7日 上午11:25:56
 */
public class UserUtils {

    /**
     * 获取当前登录用户,真正的user在loginUser下的user
     * @Title: getLoginUser
     * @param @return
     * @return LoginUser
     * @throws
     */
    public static LoginUser getLoginUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return (LoginUser) authentication.getPrincipal();
        }
        return null;
    }

    /**
     * 获得登录用户（学生，老师，管理员）的登录账号，
     * @Title: getLoginUserAccout
     * @param @return
     * @return LoginUser
     * @throws
     */
    public static String getLoginUserAccout() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            LoginUser loginUser = (LoginUser) authentication.getPrincipal();
            return loginUser.getUsername();
        }
        return null;
    }

    /**
     * 获取当前登录用户的UUID，唯一标识
     * @Title: getLoginUserUUID
     * @param @return
     * @return String
     * @throws
     */
    public static String getLoginUserUUID() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            LoginUser loginUser = (LoginUser) authentication.getPrincipal();
            return loginUser.getUuid();
        }
        return null;
    }

    /**
     * 
     * @Title: getLoginUserType   
     * @Description: 获取登录用户类型
     * @param: @return      
     * @return: String      
     * @throws
     */
    public static String getLoginUserType() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            LoginUser loginUser = (LoginUser) authentication.getPrincipal();
            return loginUser.getType();
        }
        return null;
    }
}
