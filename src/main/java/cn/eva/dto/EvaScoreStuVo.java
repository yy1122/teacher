package cn.eva.dto;

import java.util.List;

import cn.eva.entity.EvaScoreStu;

/**
 * 评分解析中间表
 * Copyright (c) 2017 by 
 * @ClassName: EvaScoreStuVo.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月11日 上午9:59:51
 */
public class EvaScoreStuVo extends EvaScoreStu {

    List<ScoreVo> scoreVos;

    public List<ScoreVo> getScoreVos() {
        return scoreVos;
    }

    public void setScoreVos(List<ScoreVo> scoreVos) {
        this.scoreVos = scoreVos;
    }

}
