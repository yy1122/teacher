package cn.eva.entity;

/**
 * Teacher 扩展类
 * 
 * @author 王梓豪
 *
 */
public class EvaTeacherVo extends EvaTeacher {
	/**
	 * 部门名称
	 */
	private String dName;

	/**
	 * 部门名称
	 */
	public String getdName() {
		return dName;
	}

	/**
	 * 部门名称
	 */
	public void setdName(String dName) {
		this.dName = dName;
	}
}
