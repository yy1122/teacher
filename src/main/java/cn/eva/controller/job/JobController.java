package cn.eva.controller.job;

import java.util.List;

import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.SysJobBaseInfo;
import cn.eva.job.BaseJob;
import cn.eva.service.job.JobService;
import cn.eva.utils.ClassUtil;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @ClassName:  JobController   
 * @Description: 定时任务管理
 * @author: yuyong 
 * @date:   2018年9月18日 上午10:43:22   
 *     
 * @Copyright: 2018 www.xxx.com Inc. All rights reserved. 
 * @note: 注意：本内容仅限于xxx公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/quartz")
public class JobController {

    @Autowired
    private JobService jobService;

    /**
     * 
     * @Title: addjob   
     * @Description: 添加一个job 
     * @param: @param jobClassName
     * @param: @param jobGroupName
     * @param: @param cronExpression
     * @param: @throws Exception      
     * @return: void      
     * @throws
     */
    @PostMapping(value = "/addjob")
    @PreAuthorize("hasAuthority('sys:job:add')")
    public ResultJosn addjob(@RequestBody SysJobBaseInfo jobbaseInfo) throws Exception {
        if (!CronExpression.isValidExpression(jobbaseInfo.getCron())) {
            throw new IllegalArgumentException("cron表达式有误");
        }
        return jobService.saveJob(jobbaseInfo);
    }

    /**
     * 
     * @Title: pausejob   
     * @Description: 暂停一个job
     * @param: @param jobClassName
     * @param: @param jobGroupName
     * @param: @throws Exception      
     * @return: void      
     * @throws
     */
    @PostMapping(value = "/pausejob")
    @PreAuthorize("hasAuthority('sys:job:upstatus')")
    public ResultJosn pausejob(@RequestBody SysJobBaseInfo jobbaseInfo) throws Exception {
        return jobService.pause(jobbaseInfo);
    }

    /**
     * 
     * @Title: resumejob   
     * @Description: 恢复一个job
     * @param: @param jobClassName
     * @param: @param jobGroupName
     * @param: @throws Exception      
     * @return: void      
     * @throws
     */
    @PostMapping(value = "/resumejob")
    @PreAuthorize("hasAuthority('sys:job:upstatus')")
    public ResultJosn resumejob(@RequestBody SysJobBaseInfo jobbaseInfo) throws Exception {
        return jobService.resumejob(jobbaseInfo);
    }

    /**
     * 
     * @Title: rescheduleJob   
     * @Description: 更新一个job  
     * @param: @param jobClassName
     * @param: @param jobGroupName
     * @param: @param cronExpression
     * @param: @throws Exception      
     * @return: void      
     * @throws
     */
    @PostMapping(value = "/reschedulejob")
    @PreAuthorize("hasAuthority('sys:job:update')")
    public ResultJosn rescheduleJob(@RequestBody SysJobBaseInfo jobbaseInfo) throws Exception {
        return jobService.reschedulejob(jobbaseInfo);
    }

    /**
     * 
     * @Title: deletejob   
     * @Description: 删除一个job  
     * @param: @param jobClassName
     * @param: @param jobGroupName
     * @param: @throws Exception      
     * @return: void      
     * @throws
     */
    @PostMapping(value = "/deletejob")
    @PreAuthorize("hasAuthority('sys:job:del')")
    public ResultJosn deletejob(@RequestBody SysJobBaseInfo jobbaseInfo) throws Exception {
        return jobService.deletejob(jobbaseInfo);
    }

    /**
     * 
     * @Title: queryjob   
     * @Description: 分页查询job信息
     * @param: @param pageNum
     * @param: @param pageSize
     * @param: @return      
     * @return: Map<String,Object>      
     * @throws
     */
    @GetMapping(value = "/queryjob")
    @PreAuthorize("hasAnyAuthority('sys:job:query','sys:job:update','sys:job:del')")
    public LayuiTableResult queryjob(@RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize, SysJobBaseInfo jobBaseInfo) {
        return jobService.getJobAndTriggerDetails(jobBaseInfo, pageNum, pageSize);
    }

    /**
     * 
     * @Title: queryjob   
     * @Description: 根据id查找job基本信息
     * @param: @param id
     * @param: @return      
     * @return: LayuiTableResult      
     * @throws
     */
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAnyAuthority('sys:job:query','sys:job:update','sys:job:del')")
    public SysJobBaseInfo queryjob(@PathVariable(value = "id") Long id) {
        return jobService.getJobInfoById(id);
    }

    @ApiOperation(value = "springBean名字")
    @GetMapping("/beans")
    private List<Class> getAllSubclassOfTestInterface() {
        return ClassUtil.getAllClassByInterface(BaseJob.class);
    }
}
