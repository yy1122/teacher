/*
*
* EvaCourse.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-07
*/
package cn.eva.entity;

import java.util.Date;

public class EvaCourse {
    /**
     * 课程标识
     */
    private String cId;

    /**
     * 课程名称，长度40
     */
    private String cName;

    /**
     * 课程类型，1必须，2选修
     */
    private String cType;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 是否删除，默认0不删除
     */
    private String isDel;

    /**
     * 课程标识
     * @return c_id 课程标识
     */
    public String getcId() {
        return cId;
    }

    /**
     * 课程标识
     * @param cId 课程标识
     */
    public void setcId(String cId) {
        this.cId = cId == null ? null : cId.trim();
    }

    /**
     * 课程名称，长度40
     * @return c_name 课程名称，长度40
     */
    public String getcName() {
        return cName;
    }

    /**
     * 课程名称，长度40
     * @param cName 课程名称，长度40
     */
    public void setcName(String cName) {
        this.cName = cName == null ? null : cName.trim();
    }

    /**
     * 课程类型，1必须，2选修
     * @return c_type 课程类型，1必须，2选修
     */
    public String getcType() {
        return cType;
    }

    /**
     * 课程类型，1必须，2选修
     * @param cType 课程类型，1必须，2选修
     */
    public void setcType(String cType) {
        this.cType = cType == null ? null : cType.trim();
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 更新时间
     * @return updated_time 更新时间
     */
    public Date getUpdatedTime() {
        return updatedTime;
    }

    /**
     * 更新时间
     * @param updatedTime 更新时间
     */
    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    /**
     * 是否删除，默认0不删除
     * @return is_del 是否删除，默认0不删除
     */
    public String getIsDel() {
        return isDel;
    }

    /**
     * 是否删除，默认0不删除
     * @param isDel 是否删除，默认0不删除
     */
    public void setIsDel(String isDel) {
        this.isDel = isDel == null ? null : isDel.trim();
    }




}