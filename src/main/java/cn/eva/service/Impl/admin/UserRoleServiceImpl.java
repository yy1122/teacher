package cn.eva.service.Impl.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.eva.common.ResultJosn;
import cn.eva.dao.SysUserRoleMapper;
import cn.eva.entity.SysUserRoleKey;
import cn.eva.example.SysUserRoleExample;
import cn.eva.service.admin.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private SysUserRoleMapper userRoleMapper;

    @Override
    @Transactional
    public ResultJosn delByUids(List<String> uids) {
        SysUserRoleExample example = new SysUserRoleExample();
        example.createCriteria().andUidIn(uids);
        userRoleMapper.deleteByExample(example);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "操作成功");
    }

    @Override
    public ResultJosn saveUserRoles(List<SysUserRoleKey> list) {
        // TODO :批量插入
        return null;
    }

}
