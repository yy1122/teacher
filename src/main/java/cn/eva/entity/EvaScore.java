/*
*
* EvaScore.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-18
*/
package cn.eva.entity;

public class EvaScore {
    /**
     * 计分表ID
     */
    private String scoreId;

    /**
     * 分数
     */
    private Float scoreNum;

    /**
     * 教师id
     */
    private String tId;

    /**
     * 课程id
     */
    private String cId;

    /**
     * 计分表ID
     * @return score_id 计分表ID
     */
    public String getScoreId() {
        return scoreId;
    }

    /**
     * 计分表ID
     * @param scoreId 计分表ID
     */
    public void setScoreId(String scoreId) {
        this.scoreId = scoreId == null ? null : scoreId.trim();
    }

    /**
     * 分数
     * @return score_num 分数
     */
    public Float getScoreNum() {
        return scoreNum;
    }

    /**
     * 分数
     * @param scoreNum 分数
     */
    public void setScoreNum(Float scoreNum) {
        this.scoreNum = scoreNum;
    }

    /**
     * 教师id
     * @return t_id 教师id
     */
    public String gettId() {
        return tId;
    }

    /**
     * 教师id
     * @param tId 教师id
     */
    public void settId(String tId) {
        this.tId = tId == null ? null : tId.trim();
    }

    /**
     * 课程id
     * @return c_id 课程id
     */
    public String getcId() {
        return cId;
    }

    /**
     * 课程id
     * @param cId 课程id
     */
    public void setcId(String cId) {
        this.cId = cId == null ? null : cId.trim();
    }
}