package cn.eva.service.Impl.student;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaScoreStuMapper;
import cn.eva.dao.EvaStudentMapper;
import cn.eva.dao.EvaStudentVoMapper;
import cn.eva.entity.EvaEvaluate;
import cn.eva.entity.EvaScoreStu;
import cn.eva.entity.EvaStudent;
import cn.eva.entity.EvaStudentVo;
import cn.eva.example.EvaScoreStuExample;
import cn.eva.service.EvaEvaluateMapService;
import cn.eva.service.student.EvaStudentService;
import cn.eva.utils.ScoreStuUtils;

@Service
public class EvaStudentServiceImpl implements EvaStudentService {
    @Autowired
    private EvaEvaluateMapService evaEvaluateMapService;
    @Autowired
    private EvaStudentVoMapper evaStudentVoMapper; // 学生 老师 课程
    @Autowired
    private EvaScoreStuMapper evaScoreStuMapper; // 计分_学生 中间表
    @Autowired
    private EvaStudentMapper evaStudentMapper;
    /**
          * 根据当前学生的uuid查询课程等基本信息
     */
    @Override
	public ResultJosn selectMymeDeatil(String sid) {
		EvaStudent stu=evaStudentMapper.selectByPrimaryKey(sid);
		List<EvaStudentVo> list = evaStudentVoMapper.selectMyDeatileCname(stu.getClassId());
		return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), list);
	}
    /**
     * 根据学生ID查询自己对任课教师的评价明细
     */
    @Override
    public ResultJosn select(EvaScoreStu evaScoreStu) {
        /**
         * 查询单表
         */
        EvaScoreStuExample stuExample = new EvaScoreStuExample();
        stuExample.createCriteria().andSIdEqualTo(evaScoreStu.getsId()).andCIdEqualTo(evaScoreStu.getcId())
                .andTIdEqualTo(evaScoreStu.gettId());
        // 查询数据
        List<EvaScoreStu> list = evaScoreStuMapper.selectByExample(stuExample);

        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), list);
    }

    /**
     * 根据学生id 查询需要评价的老师、课程、班级信息 已经评教分数
     */
    @Override
    public ResultJosn selectSidAndTid(String sid) {
        if (StringUtils.isBlank(sid)) {
            throw new IllegalArgumentException("ssid错误");
        }
        List<EvaStudentVo> list = evaStudentVoMapper.selectTnameCnameKname(sid);

        //获取题目的及权重
        Map<Integer, EvaEvaluate> selectAll = evaEvaluateMapService.selectAll();
        // 计算分数
        for (EvaStudentVo eVo : list) {
            if ((eVo.getcName() == null || "".equals(eVo.getcName()))
                    || (eVo.gettName() == null || "".equals(eVo.gettName()))) {
                return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), null);
            }
            eVo.setScores(ScoreStuUtils.calculateScore(eVo.getScores(), selectAll));
        }
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), list);

    }

    /**
     * 学生登录，打分提交到数据库
     */
    @Override
    public ResultJosn insertByGrade(EvaScoreStu evaScoreStu) {

        // 插入数据
        evaScoreStuMapper.insert(evaScoreStu);

        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), evaScoreStu);
    }

	

}