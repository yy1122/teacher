/*
*
* EvaTeacherMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaTeacher;
import cn.eva.example.EvaTeacherExample;

@Mapper
public interface EvaTeacherMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaTeacherExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaTeacherExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String tId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaTeacher record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaTeacher record);

    /**
     *
     * @mbg.generated
     */
    List<EvaTeacher> selectByExample(EvaTeacherExample example);

    /**
     *
     * @mbg.generated
     */
    EvaTeacher selectByPrimaryKey(String tId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaTeacher record, @Param("example") EvaTeacherExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaTeacher record, @Param("example") EvaTeacherExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaTeacher record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaTeacher record);
}