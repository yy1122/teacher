package cn.eva.dto;

import java.util.List;

public class SysTeacherCourseVo {
    /**
     * 唯一标识
     */
    private String tcId;

    public String getTcId() {
        return tcId;
    }

    public void setTcId(String tcId) {
        this.tcId = tcId;
    }

    public String gettId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }





    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    /**
     * 教师id
     */
    private  String tId;

    public List<String> getcIds() {
        return cIds;
    }

    public void setcIds(List<String> cIds) {
        this.cIds = cIds;
    }

    /**
     *选的单门或者多门课程
     */
    private List<String> cIds;
    /**
     *教师姓名
     */
    private  String tName;

}
