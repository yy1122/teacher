/*
*
* SysMenuMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.SysMenu;
import cn.eva.example.SysMenuExample;

@Mapper
public interface SysMenuMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(SysMenuExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(SysMenuExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Long id);

    /**
     *
     * @mbg.generated
     */
    int insert(SysMenu record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(SysMenu record);

    /**
     *
     * @mbg.generated
     */
    List<SysMenu> selectByExample(SysMenuExample example);

    /**
     *
     * @mbg.generated
     */
    SysMenu selectByPrimaryKey(Long id);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") SysMenu record, @Param("example") SysMenuExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") SysMenu record, @Param("example") SysMenuExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(SysMenu record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(SysMenu record);
}