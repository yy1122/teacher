package cn.eva.entity;

public class EvaScoreTimes {

	/**
	 * 题目
	 */
	private String subject;

	/**
	 * 记录1星分数
	 */
	private String score_1;

	/**
	 * 记录2星分数
	 */
	private String score_2;

	/**
	 * 记录3星分数
	 */
	private String score_3;

	/**
	 * 记录4星分数
	 */
	private String score_4;

	/**
	 * 记录1星分数的次数
	 */
	private int times_1;

	/**
	 * 记录2星分数次数
	 */
	private int times_2;
	/**
	 * 记录3星分数次数
	 */
	private int times_3;
	/**
	 * 记录4星分数次数
	 */
	private int times_4;

	/**
	 * 题目
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * 题目
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * 记录1星分数
	 */
	public String getScore_1() {
		return score_1;
	}

	/**
	 * 记录1星分数
	 */
	public void setScore_1(String score_1) {
		this.score_1 = score_1;
	}

	/**
	 * 记录2星分数
	 */
	public String getScore_2() {
		return score_2;
	}

	/**
	 * 记录2星分数
	 */
	public void setScore_2(String score_2) {
		this.score_2 = score_2;
	}

	/**
	 * 记录3星分数
	 */
	public String getScore_3() {
		return score_3;
	}

	/**
	 * 记录3星分数
	 */
	public void setScore_3(String score_3) {
		this.score_3 = score_3;
	}

	/**
	 * 记录4星分数
	 */
	public String getScore_4() {
		return score_4;
	}

	/**
	 * 记录4星分数
	 */
	public void setScore_4(String score_4) {
		this.score_4 = score_4;
	}

	/**
	 * 记录1星分数次数
	 */
	public int getTimes_1() {
		return times_1;
	}

	/**
	 * 记录1星分数次数
	 */
	public void setTimes_1(int times_1) {
		this.times_1 = times_1;
	}

	/**
	 * 记录2星分数次数
	 */
	public int getTimes_2() {
		return times_2;
	}

	/**
	 * 记录2星分数次数
	 */
	public void setTimes_2(int times_2) {
		this.times_2 = times_2;
	}

	/**
	 * 记录3星分数次数
	 */
	public int getTimes_3() {
		return times_3;
	}

	/**
	 * 记录3星分数次数
	 */
	public void setTimes_3(int times_3) {
		this.times_3 = times_3;
	}

	/**
	 * 记录4星分数次数
	 */
	public int getTimes_4() {
		return times_4;
	}

	/**
	 * 记录4星分数次数
	 */
	public void setTimes_4(int times_4) {
		this.times_4 = times_4;
	}

}
