package cn.eva.dto;

import java.util.List;

import cn.eva.entity.EvaManager;
import cn.eva.entity.SysRole;

public class SysUserSelect extends EvaManager {

    private static final long serialVersionUID = 6082639282203291034L;

    List<SysRole> roles;

    public List<SysRole> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRole> roles) {
        this.roles = roles;
    }

}
