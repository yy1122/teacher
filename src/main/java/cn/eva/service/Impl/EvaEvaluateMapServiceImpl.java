package cn.eva.service.Impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.eva.dao.EvaEvaluateMapper;
import cn.eva.entity.EvaEvaluate;
import cn.eva.service.EvaEvaluateMapService;

@Service
public class EvaEvaluateMapServiceImpl implements EvaEvaluateMapService {
    @Autowired
    private EvaEvaluateMapper evaluateMapper;

    /**
     * 查询所有评价标准，存到hashMap中
     */
    @Override
    public Map<Integer, EvaEvaluate> selectAll() {
        List<EvaEvaluate> eList = evaluateMapper.selectByExample(null);
        Map<Integer, EvaEvaluate> eMap = eList.stream()
                .collect(Collectors.toMap(EvaEvaluate::geteId, a -> a, (k1, k2) -> k1));
        return eMap;
    }
}
