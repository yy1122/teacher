package cn.eva.common.enums;

/**
 * 登录类型枚举
 * Copyright (c) 2017 by 
 * @ClassName: CredentialType.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月6日 下午3:23:14
 */
public enum CredentialType {

    /**
     * 教师
     */
    TEACHER,
    /**
     * 学生
     */
    STUDENT,
    /**
     * 管理员
     */
    ADMIN,
}