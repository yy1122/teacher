package cn.eva.service.job;

import cn.eva.common.LayuiTableResult;
import cn.eva.entity.SysJobBaseInfo;

public interface SysJobBaseInfoService {

    void addJob(SysJobBaseInfo jobBaseInfo);

    void delJob(SysJobBaseInfo jobBaseInfo);

    void updJob(SysJobBaseInfo jobBaseInfo);

    LayuiTableResult seletByPage(SysJobBaseInfo jobBaseInfo, Integer page, Integer pageSize);

    SysJobBaseInfo selectById(Long id);

}
