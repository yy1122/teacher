package cn.eva.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cn.eva.dto.LoginUser;
import cn.eva.service.UserService;

/**
 * 
 * @ClassName:  UserDetailsServiceImpl   
 * @Description: spring security登录处理逻辑
 * @author: yuyong 
 * @date:   2018年9月20日 下午12:30:35   
 *     
 * @Copyright: 2018 www.xxx.com Inc. All rights reserved. 
 * @note: 注意：本内容仅限于xxx公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginUser loginUser = new LoginUser();
        loginUser = userService.getUserInfoByAccount(username);
        return loginUser;
    }

}
