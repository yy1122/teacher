package cn.eva.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaManagerMapper;
import cn.eva.dao.EvaStudentMapper;
import cn.eva.dao.EvaTeacherMapper;
import cn.eva.dto.LoginUser;
import cn.eva.entity.EvaManager;
import cn.eva.entity.EvaStudent;
import cn.eva.entity.EvaTeacher;
import cn.eva.utils.UserUtils;
import io.swagger.annotations.ApiOperation;

/**
 * 登录用户相关的
 * Copyright (c) 2017 by 
 * @ClassName: UserController.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月7日 下午5:14:56
 */
@RestController
@RequestMapping("/mine")
public class UserController {

    @Autowired
    private EvaManagerMapper evaManagerMapper;

    @Autowired
    private EvaStudentMapper evaStudentMapper;

    @Autowired
    private EvaTeacherMapper evaTeacherMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/current")
    @ApiOperation(value = "获取当前登录用户的基本信息")
    public UserDetails get_user_info(@AuthenticationPrincipal UserDetails user) {
        return user;
    }

    /**
     * 更改当前登录用户的密码
     * @Title: updatePass
     * @param @return
     * @return ResultJosn
     * @throws
     */
    @PutMapping("/upPass")
    public ResultJosn updatePass(String password) {
        if (StringUtils.isBlank(password)) {
            throw new IllegalArgumentException("请填写密码");
        }
        //获取登录用户
        LoginUser loginUser = UserUtils.getLoginUser();
        Object user = loginUser.getUser();
        if (user instanceof EvaManager) {
            //后台管理员
            EvaManager record = (EvaManager) user;
            record.setmPass(passwordEncoder.encode(password));
            evaManagerMapper.updateByPrimaryKeySelective(record);
        } else if (user instanceof EvaStudent) {
            //学生
            EvaStudent record = (EvaStudent) user;
            record.setsPass(passwordEncoder.encode(password));
            evaStudentMapper.updateByPrimaryKeySelective(record);
        } else if (user instanceof EvaTeacher) {
            //老师
            EvaTeacher record = (EvaTeacher) user;
            record.settPass(passwordEncoder.encode(password));
            evaTeacherMapper.updateByPrimaryKeySelective(record);
        }
        //TODO：更新登录信息缓存
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "修改成功");
    }

}
