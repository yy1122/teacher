package cn.eva.service.admin.courseGrade;

import java.util.List;

import cn.eva.entity.EvaScoreStu;
import cn.eva.entity.EvaScoreTeh;


public interface courseGradeService {
	
	
	/**
	 * 将学生分数中间表的教师id去重取出 
	 * @return 被评教的教师id
	 */
	List<EvaScoreStu>  selectScoresStutId();
	
	
	/**
	 *  将学生分数中间表的教师id去重分别去查询被评教的教师信息
	 * @param List<EvaScoreStu> stu
	 * @return 被评教的教师信息
	 */
	
	List<EvaScoreStu> selectstuInfo(EvaScoreStu stu);
	
	
	/**
	 * #将教师分数中间表的被评教的教师id去重取出
	 * @return 被评教的教师id
	 */
	List<EvaScoreTeh> selectScoresTeatIdtwo();
	
	
	/**
	 * 将教师分数中间表的被评教的教师id去重分别去查询被评教的教师信息
	 * @param List<EvaScoreTeh> tea
	 * @return 被评教的教师信息
	 */
	List<EvaScoreTeh> selectteaInfo(EvaScoreTeh tea);

/**
 *  统计总分
 * @return true，false
 */
	public boolean totalPoint();
	
	

}
