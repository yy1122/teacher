package cn.eva.service.Impl.teacher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dao.SyscourseGradeMapper;
import cn.eva.dto.EvaScoreStuVo;
import cn.eva.entity.EvaEvaluate;
import cn.eva.entity.EvaScoreStu;
import cn.eva.entity.SyscourseGrade;
import cn.eva.service.EvaEvaluateMapService;
import cn.eva.service.teacher.ExamineEvaService;
import cn.eva.utils.ScoreStuUtils;

@Service
public class ExamineEvaServiceImpl implements ExamineEvaService {

	@Autowired
	private SyscourseGradeMapper syscourseGradeMapper;

	@Autowired
	private EvaEvaluateMapService evaEvaluateMapService;

	@Override
	public ResultJosn teachercourseList(Integer page, Integer pageSize, SyscourseGrade syscourseGrade) {
		// TODO Auto-generated method stub

		SyscourseGrade scg = new SyscourseGrade();
		// 判断输入查询数据是否为空或者""
		if (syscourseGrade.gettId() != null) {
			scg.settId("%" + syscourseGrade.gettId() + "%");
		}
		if (syscourseGrade.getcName() != null) {
			scg.setcName("%" + syscourseGrade.getcName() + "%");

		}
		if (syscourseGrade.getClassNum() != null) {
			scg.setClassNum("%" + syscourseGrade.getClassNum() + "%");
		}

		List<SyscourseGrade> list = syscourseGradeMapper.tehCourseList(scg);

		List<SyscourseGrade> slist = syscourseGradeMapper.tehCourse(syscourseGrade);

		Map<String, SyscourseGrade> map = new HashMap<String, SyscourseGrade>();

		List<SyscourseGrade> sglist = new ArrayList<SyscourseGrade>();

		for (SyscourseGrade s : slist) {
			map.put(s.getcId() + s.getClassNum(), s);
		}

		Map<Integer, EvaEvaluate> emap = evaEvaluateMapService.selectAll();
		List<EvaScoreStu> evaScoreStus = new ArrayList<EvaScoreStu>();

		for (String key : map.keySet()) {
			SyscourseGrade scGrade = new SyscourseGrade();
			for (SyscourseGrade s : list) {
				if (key.equals(s.getcId() + s.getClassNum())) {
					EvaScoreStu ess = new EvaScoreStu();
					ess.settId(s.gettId());
					ess.setcId(s.getcId());
					ess.setScores(s.getScores());
					evaScoreStus.add(ess);

					scGrade.setClassNum(s.getClassNum());
					scGrade.setcName(s.getcName());
				}

			}

			List<EvaScoreStuVo> ev = ScoreStuUtils.parseScoreStu(evaScoreStus);
			List<String> scourse = new ArrayList<String>();

			for (EvaScoreStuVo ess : ev) {
				scourse.add(ess.getScores());
			}

			scGrade.setScores(ScoreStuUtils.caScoreAll(scourse, emap));
			sglist.add(scGrade);

		}

		/*
		 * for(SyscourseGrade s:list){
		 * 
		 * List<ScoreVo> sList = ScoreStuUtils.parseScores(s.getScores());
		 * System.err.println(sList);
		 * 
		 * }
		 */

		// 开启分页查找

		PageHelper.startPage(page, pageSize);
		PageInfo<SyscourseGrade> pageInfo = new PageInfo<>(sglist, pageSize);
		// 设置返回前台的格式和数据，如code=0/success，message，查询的总数total，和数据集合data
		LayuiTableResult resultJosn = new LayuiTableResult();
		resultJosn.setCode(HttpStatus.OK.value());
		resultJosn.setMessage(HttpStatus.OK.getReasonPhrase());
		resultJosn.setCount(pageInfo.getTotal());
		resultJosn.setData(pageInfo.getList());
		return resultJosn;

	}

}
