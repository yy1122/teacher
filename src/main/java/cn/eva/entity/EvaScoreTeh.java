/*
*
* EvaScoreTeh.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-15
*/
package cn.eva.entity;

public class EvaScoreTeh {
    /**
     * 计分_老师中间表主键ID
     */
    private String stId;

    /**
     * 评测老师ID
     */
    private String tIdOne;

    /**
     * 被评老师ID
     */
    private String tIdTwo;

    /**
     * 课程ID
     */
    private String cId;

    /**
     * 答案集合
     */
    private String scoresT;

    /**
     * 计分_老师中间表主键ID
     * @return st_id 计分_老师中间表主键ID
     */
    public String getStId() {
        return stId;
    }

    /**
     * 计分_老师中间表主键ID
     * @param stId 计分_老师中间表主键ID
     */
    public void setStId(String stId) {
        this.stId = stId == null ? null : stId.trim();
    }

    /**
     * 评测老师ID
     * @return t_id_one 评测老师ID
     */
    public String gettIdOne() {
        return tIdOne;
    }

    /**
     * 评测老师ID
     * @param tIdOne 评测老师ID
     */
    public void settIdOne(String tIdOne) {
        this.tIdOne = tIdOne == null ? null : tIdOne.trim();
    }

    /**
     * 被评老师ID
     * @return t_id_two 被评老师ID
     */
    public String gettIdTwo() {
        return tIdTwo;
    }

    /**
     * 被评老师ID
     * @param tIdTwo 被评老师ID
     */
    public void settIdTwo(String tIdTwo) {
        this.tIdTwo = tIdTwo == null ? null : tIdTwo.trim();
    }

    /**
     * 课程ID
     * @return c_id 课程ID
     */
    public String getcId() {
        return cId;
    }

    /**
     * 课程ID
     * @param cId 课程ID
     */
    public void setcId(String cId) {
        this.cId = cId == null ? null : cId.trim();
    }

    /**
     * 答案集合
     * @return scores_t 答案集合
     */
    public String getScoresT() {
        return scoresT;
    }

    /**
     * 答案集合
     * @param scoresT 答案集合
     */
    public void setScoresT(String scoresT) {
        this.scoresT = scoresT == null ? null : scoresT.trim();
    }
}