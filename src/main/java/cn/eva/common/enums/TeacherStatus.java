package cn.eva.common.enums;

/**
 * 教师状态
 * Copyright (c) 2017 by 
 * @ClassName: TeacherStatus.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月6日 下午6:34:19
 */
public enum TeacherStatus {

    Incumbent("1", "在职"), Resignation("0", "离职");

    private String code;

    private String name;

    private TeacherStatus(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
