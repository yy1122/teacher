package cn.eva.service.job;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.AddJobInfo;
import cn.eva.entity.SysJobBaseInfo;

public interface JobService {

    ResultJosn saveJob(SysJobBaseInfo jobBaseInfo) throws Exception;

    ResultJosn pause(SysJobBaseInfo jobBaseInfo) throws Exception;

    ResultJosn resumejob(SysJobBaseInfo jobBaseInfo) throws Exception;

    ResultJosn reschedulejob(SysJobBaseInfo jobBaseInfo) throws Exception;

    ResultJosn deletejob(SysJobBaseInfo jobBaseInfo) throws Exception;

    LayuiTableResult getJobAndTriggerDetails(SysJobBaseInfo jobBaseInfo, Integer pageNum, Integer pageSize);

    SysJobBaseInfo getJobInfoById(Long id);

    //添加统计日期任务
    ResultJosn addUp(AddJobInfo addJobInfo) throws Exception;

    //更改统计日期任务
    ResultJosn updUp(AddJobInfo addJobInfo) throws Exception;

    //删除统计日期分数
    ResultJosn delUp(AddJobInfo addJobInfo) throws Exception;

    //暂停统计任务
    ResultJosn pause(AddJobInfo addJobInfo) throws Exception;

    //恢复统计任务
    ResultJosn resumejob(AddJobInfo addJobInfo) throws Exception;

}