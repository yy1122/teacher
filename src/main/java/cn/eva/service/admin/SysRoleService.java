package cn.eva.service.admin;

import java.util.List;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dto.SysRoleAdd;
import cn.eva.entity.SysRole;

public interface SysRoleService {

    public ResultJosn add(SysRoleAdd role);

    public ResultJosn delete(List<Long> ids);

    public ResultJosn update(SysRoleAdd role);

    public LayuiTableResult select(SysRole role, Integer page, Integer pageSize);

    ResultJosn selectByid(Long id);
}
