package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.eva.entity.EvaTeacherVo;
import cn.eva.example.EvaTeacherExample;

@Mapper
public interface EvaTeacherVoMapper {
	
	List<EvaTeacherVo> selectTdIdAndDName(EvaTeacherExample example);

}
