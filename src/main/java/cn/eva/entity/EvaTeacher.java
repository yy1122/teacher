/*
*
* EvaTeacher.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.entity;

import java.util.Date;

public class EvaTeacher {
    /**
     * 教师唯一标识
     */
    private String tId;

    /**
     * 教师工号
     */
    private String tNo;

    /**
     * 教师姓名
     */
    private String tName;

    /**
     * 密码
     */
    private String tPass;

    /**
     * 性别，1：男，0：女
     */
    private String tSex;

    /**
     * 教师状态（1在职0离职）
     */
    private String tStatus;

    /**
     * 关联系部id
     */
    private String dId;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 教师唯一标识
     * @return t_id 教师唯一标识
     */
    public String gettId() {
        return tId;
    }

    /**
     * 教师唯一标识
     * @param tId 教师唯一标识
     */
    public void settId(String tId) {
        this.tId = tId == null ? null : tId.trim();
    }

    /**
     * 教师工号
     * @return t_no 教师工号
     */
    public String gettNo() {
        return tNo;
    }

    /**
     * 教师工号
     * @param tNo 教师工号
     */
    public void settNo(String tNo) {
        this.tNo = tNo == null ? null : tNo.trim();
    }

    /**
     * 教师姓名
     * @return t_name 教师姓名
     */
    public String gettName() {
        return tName;
    }

    /**
     * 教师姓名
     * @param tName 教师姓名
     */
    public void settName(String tName) {
        this.tName = tName == null ? null : tName.trim();
    }

    /**
     * 密码
     * @return t_pass 密码
     */
    public String gettPass() {
        return tPass;
    }

    /**
     * 密码
     * @param tPass 密码
     */
    public void settPass(String tPass) {
        this.tPass = tPass == null ? null : tPass.trim();
    }

    /**
     * 性别，1：男，0：女
     * @return t_sex 性别，1：男，0：女
     */
    public String gettSex() {
        return tSex;
    }

    /**
     * 性别，1：男，0：女
     * @param tSex 性别，1：男，0：女
     */
    public void settSex(String tSex) {
        this.tSex = tSex == null ? null : tSex.trim();
    }

    /**
     * 教师状态（1在职0离职）
     * @return t_status 教师状态（1在职0离职）
     */
    public String gettStatus() {
        return tStatus;
    }

    /**
     * 教师状态（1在职0离职）
     * @param tStatus 教师状态（1在职0离职）
     */
    public void settStatus(String tStatus) {
        this.tStatus = tStatus == null ? null : tStatus.trim();
    }

    /**
     * 关联系部id
     * @return d_id 关联系部id
     */
    public String getdId() {
        return dId;
    }

    /**
     * 关联系部id
     * @param dId 关联系部id
     */
    public void setdId(String dId) {
        this.dId = dId == null ? null : dId.trim();
    }

    /**
     * 
     * @return create_time 
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 
     * @param createTime 
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 
     * @return update_time 
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 
     * @param updateTime 
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}