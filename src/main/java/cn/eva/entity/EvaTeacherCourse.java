/*
*
* EvaTeacherCourse.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-10
*/
package cn.eva.entity;

public class EvaTeacherCourse {
    /**
     * 主键ID
     */
    private String tcId;

    /**
     * 教师id
     */
    private String tId;

    /**
     * 课程id
     */
    private String cId;

    /**
     * 教师姓名
     */
    private String tName;

    /**
     * 主键ID
     * @return tc_id 主键ID
     */
    public String getTcId() {
        return tcId;
    }

    /**
     * 主键ID
     * @param tcId 主键ID
     */
    public void setTcId(String tcId) {
        this.tcId = tcId == null ? null : tcId.trim();
    }

    /**
     * 教师id
     * @return t_id 教师id
     */
    public String gettId() {
        return tId;
    }

    /**
     * 教师id
     * @param tId 教师id
     */
    public void settId(String tId) {
        this.tId = tId == null ? null : tId.trim();
    }

    /**
     * 课程id
     * @return c_id 课程id
     */
    public String getcId() {
        return cId;
    }

    /**
     * 课程id
     * @param cId 课程id
     */
    public void setcId(String cId) {
        this.cId = cId == null ? null : cId.trim();
    }

    /**
     * 教师姓名
     * @return t_name 教师姓名
     */
    public String gettName() {
        return tName;
    }

    /**
     * 教师姓名
     * @param tName 教师姓名
     */
    public void settName(String tName) {
        this.tName = tName == null ? null : tName.trim();
    }
}