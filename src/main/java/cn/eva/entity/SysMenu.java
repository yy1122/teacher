/*
*
* SysMenu.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.entity;

public class SysMenu {
    /**
     * 菜单id
     */
    private Long id;

    /**
     * 父类id
     */
    private Long parentId;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 菜单图标
     */
    private String ico;

    /**
     * url
     */
    private String href;

    /**
     * 菜单类型，1：菜单是个url，2:是一个按钮
     */
    private String type;

    /**
     * 权限标识，唯一
     */
    private String permission;

    /**
     * 菜单排序
     */
    private Long sort;

    /**
     * 菜单id
     * @return id 菜单id
     */
    public Long getId() {
        return id;
    }

    /**
     * 菜单id
     * @param id 菜单id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 父类id
     * @return parent_id 父类id
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 父类id
     * @param parentId 父类id
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 菜单名称
     * @return name 菜单名称
     */
    public String getName() {
        return name;
    }

    /**
     * 菜单名称
     * @param name 菜单名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 菜单图标
     * @return ico 菜单图标
     */
    public String getIco() {
        return ico;
    }

    /**
     * 菜单图标
     * @param ico 菜单图标
     */
    public void setIco(String ico) {
        this.ico = ico == null ? null : ico.trim();
    }

    /**
     * url
     * @return href url
     */
    public String getHref() {
        return href;
    }

    /**
     * url
     * @param href url
     */
    public void setHref(String href) {
        this.href = href == null ? null : href.trim();
    }

    /**
     * 菜单类型，1：菜单是个url，2:是一个按钮
     * @return type 菜单类型，1：菜单是个url，2:是一个按钮
     */
    public String getType() {
        return type;
    }

    /**
     * 菜单类型，1：菜单是个url，2:是一个按钮
     * @param type 菜单类型，1：菜单是个url，2:是一个按钮
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 权限标识，唯一
     * @return permission 权限标识，唯一
     */
    public String getPermission() {
        return permission;
    }

    /**
     * 权限标识，唯一
     * @param permission 权限标识，唯一
     */
    public void setPermission(String permission) {
        this.permission = permission == null ? null : permission.trim();
    }

    /**
     * 菜单排序
     * @return sort 菜单排序
     */
    public Long getSort() {
        return sort;
    }

    /**
     * 菜单排序
     * @param sort 菜单排序
     */
    public void setSort(Long sort) {
        this.sort = sort;
    }
}