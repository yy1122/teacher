package cn.eva.service.Impl.admin.courseGade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaScoreStuMapper;
import cn.eva.dao.EvaScoreTehMapper;
import cn.eva.dto.ScoreVo;
import cn.eva.entity.EvaScoreStu;
import cn.eva.entity.EvaScoreTeh;
import cn.eva.entity.EvaScoreTimes;
import cn.eva.example.EvaScoreStuExample;
import cn.eva.example.EvaScoreTehExample;
import cn.eva.example.EvaScoreStuExample.Criteria;
import cn.eva.service.admin.courseGrade.scoresDetailedService;
import cn.eva.utils.ScoreStuUtils;
import cn.eva.utils.numToChinese;

@Service
public class scoresDetailedServiceImpl implements scoresDetailedService {

	@Autowired
	private EvaScoreStuMapper evaScoreStuMapper;

	@Autowired
	private EvaScoreTehMapper evaScoreTehMapper;

	/**
	 * 将题和分数的list传进去
	 * 
	 * @return 题，分数，分数次数的一个list
	 */
	public static List<EvaScoreTimes> scoreUtils(List<ScoreVo> list, List<EvaScoreTimes> estList) {

		List<EvaScoreTimes> estTimes = new ArrayList<EvaScoreTimes>();

		int i = 0; // 记录当前循环
		for (ScoreVo s : list) {
			EvaScoreTimes est = new EvaScoreTimes();
			i++;
			String str = numToChinese.toChinese(i + "");// 当前题的题号

			// System.err.println(str + ":" + s.getValue());

			est.setSubject(str);
			String sv = s.getValue(); // 当前题的分数

			if (estList.size() != 0) { // 判断是否是第一个进来的学生或老师
				for (EvaScoreTimes sTimes : estList) { // 如果不是就遍历题，分数，分数次数的list

					if (sTimes.getSubject().equals(str)) {
						est = sTimes;
						switch (sv) { // 判断当前题目的分数
						case "1":

							if (sv.equals(sTimes.getScore_1())) { // 判断当前分数是否出现过
								est.setTimes_1(sTimes.getTimes_1() + 1); // 如果出现过，就在它的次数上加一
							} else {// 如果没有出现过，就给它赋值
								est.setScore_1(sv);
								est.setTimes_1(1);
							}
							break;
						case "2":

							if (sv.equals(sTimes.getScore_2())) { // 判断当前分数是否出现过
								est.setTimes_2(sTimes.getTimes_2() + 1);// 如果出现过，就在它的次数上加一
							} else {// 如果没有出现过，就给它赋值
								est.setScore_2(sv);
								est.setTimes_2(1);
							}
							break;
						case "3":

							if (sv.equals(sTimes.getScore_3())) { // 判断当前分数是否出现过
								est.setTimes_3(sTimes.getTimes_3() + 1);// 如果出现过，就在它的次数上加一

							} else {// 如果没有出现过，就给它赋值
								est.setScore_3(sv);
								est.setTimes_3(1);
							}
							break;
						case "4":

							if (sv.equals(sTimes.getScore_4())) { // 判断当前分数是否出现过
								est.setTimes_4(sTimes.getTimes_4() + 1);// 如果出现过，就在它的次数上加一
							} else {// 如果没有出现过，就给它赋值
								est.setScore_4(sv);
								est.setTimes_4(1);
							}
							break;

						default:
							break;
						}

					}

				}

			} else { // 如果是第一个进来的学生或老师

				switch (sv) {// 就根据分数直接赋值
				case "1":
					est.setScore_1(sv);
					est.setTimes_1(1);
					break;
				case "2":
					est.setScore_2(sv);
					est.setTimes_2(1);
					break;
				case "3":
					est.setScore_3(sv);
					est.setTimes_3(1);
					break;
				case "4":
					est.setScore_4(sv);
					est.setTimes_4(1);
					break;

				default:
					break;
				}

			}

			estTimes.add(est); // 将当前的题目信息写入list中
		}

		return estTimes;
	}

	/**
	 * 查询给该老师评教的学生打的分数
	 */
	@Override
	public ResultJosn selectStuDetail(EvaScoreStu evaScoreStu) {

		EvaScoreStuExample example = new EvaScoreStuExample();
		Criteria criteria = example.createCriteria();
		criteria.andCIdEqualTo(evaScoreStu.getcId());
		criteria.andTIdEqualTo(evaScoreStu.gettId());
		List<EvaScoreStu> evaScoreStuList = evaScoreStuMapper.selectByExample(example);

		List<EvaScoreTimes> estList = new ArrayList<EvaScoreTimes>();

		if (evaScoreStuList.size() != 0) {

			for (EvaScoreStu e : evaScoreStuList) {
				List<ScoreVo> list = ScoreStuUtils.parseScores(e.getScores());//将题号和分数取出
				estList = scoreUtils(list, estList);
			}
		}

		return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), estList);
	}

	/**
	 * 查询给该老师评教的教师打的分数
	 */
	@Override
	public ResultJosn selectTehDetail(EvaScoreTeh evaScoreTeh) {
		System.out.println(evaScoreTeh.gettIdTwo());
		EvaScoreTehExample example = new EvaScoreTehExample();
		cn.eva.example.EvaScoreTehExample.Criteria criteria = example.createCriteria();
		criteria.andCIdEqualTo(evaScoreTeh.getcId());
		criteria.andTIdTwoEqualTo(evaScoreTeh.gettIdTwo());
		List<EvaScoreTeh> evaScoreTehList = evaScoreTehMapper.selectByExample(example);

		List<EvaScoreTimes> estList = new ArrayList<EvaScoreTimes>();

		if (evaScoreTehList.size() != 0) {

			for (EvaScoreTeh e : evaScoreTehList) {
				if(!e.gettIdOne().equals(e.gettIdTwo())){
					List<ScoreVo> list = ScoreStuUtils.parseScores(e.getScoresT()); //将题号和分数取出
					estList = scoreUtils(list, estList);
				}
				
				
			}
		}

		return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), estList);
	}

}
