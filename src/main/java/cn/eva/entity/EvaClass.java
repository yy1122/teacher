/*
*
* EvaClass.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.entity;

import java.util.Date;

public class EvaClass {
    /**
     * 班级唯一标识
     */
    private String classId;

    /**
     * 班级编号，例：1590011
     */
    private String classNum;

    /**
     * 班级名称
     */
    private String className;

    /**
     * 班级类型，1:本科，2:专科，3:高职，4:成教
     */
    private String classType;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否删除，0：未删除，1：已删除
     */
    private String flag;

    /**
     * 班级唯一标识
     * @return class_id 班级唯一标识
     */
    public String getClassId() {
        return classId;
    }

    /**
     * 班级唯一标识
     * @param classId 班级唯一标识
     */
    public void setClassId(String classId) {
        this.classId = classId == null ? null : classId.trim();
    }

    /**
     * 班级编号，例：1590011
     * @return class_num 班级编号，例：1590011
     */
    public String getClassNum() {
        return classNum;
    }

    /**
     * 班级编号，例：1590011
     * @param classNum 班级编号，例：1590011
     */
    public void setClassNum(String classNum) {
        this.classNum = classNum == null ? null : classNum.trim();
    }

    /**
     * 班级名称
     * @return class_name 班级名称
     */
    public String getClassName() {
        return className;
    }

    /**
     * 班级名称
     * @param className 班级名称
     */
    public void setClassName(String className) {
        this.className = className == null ? null : className.trim();
    }

    /**
     * 班级类型，1:本科，2:专科，3:高职，4:成教
     * @return class_type 班级类型，1:本科，2:专科，3:高职，4:成教
     */
    public String getClassType() {
        return classType;
    }

    /**
     * 班级类型，1:本科，2:专科，3:高职，4:成教
     * @param classType 班级类型，1:本科，2:专科，3:高职，4:成教
     */
    public void setClassType(String classType) {
        this.classType = classType == null ? null : classType.trim();
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 是否删除，0：未删除，1：已删除
     * @return flag 是否删除，0：未删除，1：已删除
     */
    public String getFlag() {
        return flag;
    }

    /**
     * 是否删除，0：未删除，1：已删除
     * @param flag 是否删除，0：未删除，1：已删除
     */
    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }
}