package cn.eva.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaCourse;
import cn.eva.service.admin.SysCourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "后台课程")
@RequestMapping("/sysCourse")
@RestController
public class SysCourseController {
    @Autowired
    SysCourseService courseService;

    @PostMapping
    @ApiOperation(value = "添加课程", notes = "添加课程")
    @PreAuthorize("hasAnyAuthority('sys:course:add')")
    public ResultJosn add(@RequestBody EvaCourse course) {
        ResultJosn resultJosn = null;
        resultJosn = courseService.add(course);
        return resultJosn;
    }

    @DeleteMapping
    @ApiOperation(value = "单、批量删除课程")
    @PreAuthorize("hasAuthority('sys:course:del')")
    public ResultJosn delete(@RequestBody List<String> deleteids) {
        ResultJosn resultJosn = null;
        resultJosn = courseService.delete(deleteids);
        return resultJosn;
    }

    @PutMapping
    @ApiOperation(value = "更新课程信息")
    @PreAuthorize("hasAuthority('sys:course:update')")
    public ResultJosn update(@RequestBody EvaCourse course) {
        ResultJosn resultJosn = null;
        resultJosn = courseService.update(course);
        return resultJosn;
    }

    @GetMapping
    @ApiOperation(value = "课程列表条件模糊查询")
    @PreAuthorize("hasAnyAuthority('sys:course:query','sys:course:update','sys:course:del')")
    public LayuiTableResult selectByExample(EvaCourse course, @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize) {

        LayuiTableResult resultJosn = courseService.select(course, page, pageSize);
        return resultJosn;
    }

    @GetMapping(value = "/{cId}")
    @ApiOperation(value = "根据cId查找课程详细信息")
    @PreAuthorize("hasAnyAuthority('sys:course:query','sys:course:update','sys:course:del')")
    public ResultJosn selectByUid(@PathVariable String cId) {
        ResultJosn resultJosn = courseService.selectByid(cId);
        return resultJosn;
    }

}
