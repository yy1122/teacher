package cn.eva.service.student;

import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaScoreStu;

public interface EvaStudentService {

    public ResultJosn select(EvaScoreStu evaScoreStu);

    public ResultJosn selectSidAndTid(String sid);

    public ResultJosn insertByGrade(EvaScoreStu evaScoreStu);
    
    public ResultJosn selectMymeDeatil(String sid);
}
