package cn.eva.service.Impl.admin;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaEvaluateMapper;
import cn.eva.dto.EvaluateZtreeVo;
import cn.eva.entity.EvaEvaluate;
import cn.eva.example.EvaEvaluateExample;
import cn.eva.service.admin.SysEvaluateService;

@Service
public class SysEvaluateServiceImpl implements SysEvaluateService {

    @Autowired
    private EvaEvaluateMapper evaEvaluateMapper;

    /**
     * 树形展示所有的评价标准（parentId为0）
     * <p>Title: listAllZtree</p> 
     * <p>Description: </p> 
     * @return 
     * @see cn.eva.service.admin.SysEvaluateService#listAllZtree()
     */
    @Override
    public List<EvaluateZtreeVo> listAllZtree() {
        List<EvaluateZtreeVo> list = this.getChilds(0);
        return list;
    }

    @Override
    public List<EvaEvaluate> listAllZtreeSimple() {
        EvaEvaluateExample example = new EvaEvaluateExample();
        example.setOrderByClause("e_id ASC");
        List<EvaEvaluate> list = evaEvaluateMapper.selectByExample(example);
        return list;
    }

    /**
     * 树形展示学生的评价标准（parentId为1）
     * <p>Title: listStudentZtree</p> 
     * <p>Description: </p> 
     * @return 
     * @see cn.eva.service.admin.SysEvaluateService#listStudentZtree()
     */
    @Override
    public List<EvaluateZtreeVo> listStudentZtree() {
        List<EvaluateZtreeVo> list = this.getChilds(1);
        return list;
    }

    @Override
    public List<EvaluateZtreeVo> listTeacherZtree() {
        List<EvaluateZtreeVo> list = this.getChilds(2);
        return list;
    }

    @Override
    public List<EvaluateZtreeVo> listMyselfevaZtree() {
        List<EvaluateZtreeVo> list = this.getChilds(5);
        return list;
    }

    /**
     * 根据parentId递归查询
     * @Title: getChilds
     * @param @param parentId
     * @param @return 评价标准的树形集合
     * @return List<EvaluateZtreeVo>
     * @throws
     */
    private List<EvaluateZtreeVo> getChilds(Integer parentId) {
        List<EvaluateZtreeVo> evaluateZtreeVos = new ArrayList<>();
        //子节点
        EvaEvaluateExample example = new EvaEvaluateExample();
        example.createCriteria().andEParentIdEqualTo(parentId);
        List<EvaEvaluate> parentList = evaEvaluateMapper.selectByExample(example);
        //遍历一级子节点
        for (EvaEvaluate evaEvaluate : parentList) {
            //将一级加入evaluateZtreeVos
            EvaluateZtreeVo evaluateZtreeVo = new EvaluateZtreeVo();
            BeanUtils.copyProperties(evaEvaluate, evaluateZtreeVo);
            evaluateZtreeVos.add(evaluateZtreeVo);
        }
        //遍历一级子节点的儿子
        for (EvaluateZtreeVo evaluateZtreeVo : evaluateZtreeVos) {
            //取出一级子节点的id作为parentId
            Integer id = evaluateZtreeVo.geteId();
            List<EvaluateZtreeVo> childs = getChilds(id);
            if (CollectionUtils.isEmpty(childs) && evaluateZtreeVo.geteParentId() != 0) {
                evaluateZtreeVo.setIsParent(false);
            }
            evaluateZtreeVo.setChildren(childs);
        }
        return evaluateZtreeVos;
    }

    @Override
    public ResultJosn add(EvaEvaluate evaEvaluate) {
        evaEvaluateMapper.insert(evaEvaluate);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "操作成功");
    }

    @Override
    public ResultJosn delete(Integer eId) {
        EvaEvaluateExample example = new EvaEvaluateExample();
        example.createCriteria().andEParentIdEqualTo(eId);
        evaEvaluateMapper.deleteByExample(example);
        evaEvaluateMapper.deleteByPrimaryKey(eId);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "操作成功");
    }

    @Override
    public ResultJosn update(EvaEvaluate evaEvaluate) {
        evaEvaluate.seteParentId(null);
        evaEvaluateMapper.updateByPrimaryKeySelective(evaEvaluate);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "操作成功");
    }


}
