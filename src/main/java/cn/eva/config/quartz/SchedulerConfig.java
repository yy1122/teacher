package cn.eva.config.quartz;

import java.io.IOException;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration
public class SchedulerConfig {

    public static final String KEY = "applicationContextSchedulerContextKey";

    @Bean(name = "SchedulerFactory")
    public SchedulerFactoryBean schedulerFactoryBean(DataSource dataSource) throws IOException {
        SchedulerFactoryBean quartzScheduler = new SchedulerFactoryBean();

        try {
            quartzScheduler.setQuartzProperties(
                    PropertiesLoaderUtils.loadProperties(new ClassPathResource("quartz.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        quartzScheduler.setDataSource(dataSource);
        quartzScheduler.setOverwriteExistingJobs(true);
        quartzScheduler.setApplicationContextSchedulerContextKey(KEY);
        quartzScheduler.setStartupDelay(10);

        return quartzScheduler;
    }

}
