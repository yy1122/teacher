package cn.eva.entity;

/**
 * 教师课程扩展类
 * 
 * @author 李彬
 *
 */
public class EvaTeacherCourseVo extends EvaTeacherCourse {
	/**
	 * 教师工号
	 */
	private String tNo;
	/**
	 * 部门编号
	 */
	private String dId;

	/**
	 * 部门名称
	 */
	private String dName;
	/**
	 * 课程名称，长度40
	 */
	private String cName;
	/**
	 * 答案集合
	 */
	private String scoresT;

	/**
	 * 部门编号
	 */
	public String getdId() {
		return dId;
	}

	/**
	 * 部门编号
	 */
	public void setdId(String dId) {
		this.dId = dId;
	}

	/**
	 * 答案集合
	 */
	public String getScoresT() {
		return scoresT;
	}

	/**
	 * 答案集合
	 */
	public void setScoresT(String scoresT) {
		this.scoresT = scoresT;
	}

	/**
	 * 教师工号
	 */
	public String gettNo() {
		return tNo;
	}

	/**
	 * 教师工号
	 */
	public void settNo(String tNo) {
		this.tNo = tNo;
	}

	/**
	 * 部门名称
	 */
	public String getdName() {
		return dName;
	}

	/**
	 * 部门名称
	 */
	public void setdName(String dName) {
		this.dName = dName;
	}

	/**
	 * 课程名称，长度40
	 */
	public String getcName() {
		return cName;
	}

	/**
	 * 课程名称，长度40
	 */
	public void setcName(String cName) {
		this.cName = cName;
	}
}
