package cn.eva.entity;


/**
 * 
 * @author ANOTHER ONE
 *
 */
public class SyscourseGrade {
	/**
	 * 课程id
	 */
	private String cId;

	/**
	 * 教师唯一标识
	 */
	private String tId;
	
	/**
	 * 教师编号
	 */
	private String tNo;
	
	/**
	 * 部门id
	 */
	private String dId;

	/**
	 * 部门名
	 */
	private String dName;
	
	/**
	 * 教师编号
	 * 如王小哎 2
	 * @return 教师编号
	 */
	public String gettNo() {
		return tNo;
	}

	/**
	 * 教师编号
	 * 如王小哎 2
	 * @return 教师编号
	 */
	public void settNo(String tNo) {
		this.tNo = tNo;
	}

	/**
	 * 教师姓名
	 */
	private String tName;

	/**
	 * 课程名称，长度40
	 */
	private String cName;
	/**
	 * 班级编号，例：1590011
	 */
	private String classNum;
	/**
	 * 总分分数
	 */
	private Float scoreNum;
	/**
	 * 学生自己分数统计
	 */
	private String scores;

	/**
	 * 教师唯一标识
	 */
	public String gettId() {
		return tId;
	}

	/**
	 * 教师唯一标识
	 */
	public void settId(String tId) {
		this.tId = tId;
	}

	/**
	 * 教师姓名
	 */
	public String gettName() {
		return tName;
	}

	/**
	 * 教师姓名
	 */
	public void settName(String tName) {
		this.tName = tName;
	}

	/**
	 * 部门id
	 */
	public String getdId() {
		return dId;
	}
	/**
	 * 部门id
	 */
	public void setdId(String dId) {
		this.dId = dId;
	}
	/**
	 * 部门名
	 */
	public String getdName() {
		return dName;
	}
	/**
	 * 部门名
	 */
	public void setdName(String dName) {
		this.dName = dName;
	}

	/**
	 * 课程名称，长度40
	 */
	public String getcName() {
		return cName;
	}

	/**
	 * 课程名称，长度40
	 */
	public void setcName(String cName) {
		this.cName = cName;
	}

	/**
	 * 班级编号，例：1590011
	 */
	public String getClassNum() {
		return classNum;
	}

	/**
	 * 班级编号，例：1590011
	 */
	public void setClassNum(String classNum) {
		this.classNum = classNum;
	}

	/**
	 * 分数
	 */
	public Float getScoreNum() {
		return scoreNum;
	}

	/**
	 * 分数
	 */
	public void setScoreNum(Float scoreNum) {
		this.scoreNum = scoreNum;
	}

	/**
	 * 学生自己分数统计
	 */
	public String getScores() {
		return scores;
	}

	/**
	 * 学生自己分数统计
	 */
	public void setScores(String scores) {
		this.scores = scores;
	}

	/**
	 * 课程id
	 */
	public String getcId() {
		return cId;
	}

	/**
	 * 课程id
	 */
	public void setcId(String cId) {
		this.cId = cId;
	}
}
