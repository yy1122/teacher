package cn.eva.controller.teacher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.ResultJosn;
import cn.eva.entity.SyscourseGrade;
import cn.eva.service.admin.courseGrade.rankingListService;
import cn.eva.service.teacher.ExamineEvaService;
import cn.eva.utils.UserUtils;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ANOTHER ONE
 * @Date: 2018.09.06 14:47 教师控制器 查看学生对老师的评价
 */
@RestController
@RequestMapping("/Examine")
public class ExamineEvaController {

	@Autowired
	private ExamineEvaService examineEvaService;



	@GetMapping("/ExamineTehCourse")
	 @ApiOperation(value = "课程分数") 
	public ResultJosn tehCourse(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize,SyscourseGrade syscourseGrade) {
		syscourseGrade.settId(UserUtils.getLoginUserUUID()); //获取登录UUId
		System.out.println(UserUtils.getLoginUserUUID());
		ResultJosn resultJosn = examineEvaService.teachercourseList(page, pageSize,syscourseGrade);
		return resultJosn;
	}
		
}
