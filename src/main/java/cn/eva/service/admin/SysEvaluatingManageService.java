package cn.eva.service.admin;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaWhatTime;

public interface SysEvaluatingManageService {

    public ResultJosn add(EvaWhatTime evaWhatTime);

    public ResultJosn delete(String wtid);

    public ResultJosn update(EvaWhatTime evaWhatTime);

    public ResultJosn updateState(EvaWhatTime evaWhatTime);

    public LayuiTableResult select(EvaWhatTime evaWhatTime, Integer page, Integer pageSize);

    public ResultJosn selectBytId(String wtid);

    public void pauseAll(EvaWhatTime evaWhatTime);
}
