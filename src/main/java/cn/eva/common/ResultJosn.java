package cn.eva.common;

/**
 * 统一json返回
 * Copyright (c) 2017 by 
 * @ClassName: PublicResultJosn.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月6日 上午11:56:59
 */
public class ResultJosn {

    private Integer code;

    private String message;

    private Object data;

    public ResultJosn() {
        super();
    }

    public ResultJosn(Integer code, String message, Object data) {
        super();
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}