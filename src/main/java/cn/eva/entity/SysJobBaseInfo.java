/*
*
* SysJobBaseInfo.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-18
*/
package cn.eva.entity;

import java.io.Serializable;
import java.util.Date;

public class SysJobBaseInfo implements Serializable {

    private static final long serialVersionUID = -4762700554200839083L;

    /**
     * 
     */
    private Long id;

    /**
     * job名称
     */
    private String jobName;

    /**
     * job描述
     */
    private String jobDesc;

    /**
     * job的类路径
     */
    private String jobClassName;

    /**
     * job所属组
     */
    private String jobGroupName;

    /**
     * cron表达式（执行周期）
     */
    private String cron;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * job名称
     * @return job_name job名称
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * job名称
     * @param jobName job名称
     */
    public void setJobName(String jobName) {
        this.jobName = jobName == null ? null : jobName.trim();
    }

    /**
     * job描述
     * @return job_desc job描述
     */
    public String getJobDesc() {
        return jobDesc;
    }

    /**
     * job描述
     * @param jobDesc job描述
     */
    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc == null ? null : jobDesc.trim();
    }

    /**
     * job的类路径
     * @return job_class_name job的类路径
     */
    public String getJobClassName() {
        return jobClassName;
    }

    /**
     * job的类路径
     * @param jobClassName job的类路径
     */
    public void setJobClassName(String jobClassName) {
        this.jobClassName = jobClassName == null ? null : jobClassName.trim();
    }

    /**
     * job所属组
     * @return job_group_name job所属组
     */
    public String getJobGroupName() {
        return jobGroupName;
    }

    /**
     * job所属组
     * @param jobGroupName job所属组
     */
    public void setJobGroupName(String jobGroupName) {
        this.jobGroupName = jobGroupName == null ? null : jobGroupName.trim();
    }

    /**
     * cron表达式（执行周期）
     * @return cron cron表达式（执行周期）
     */
    public String getCron() {
        return cron;
    }

    /**
     * cron表达式（执行周期）
     * @param cron cron表达式（执行周期）
     */
    public void setCron(String cron) {
        this.cron = cron == null ? null : cron.trim();
    }

    /**
     * 状态
     * @return status 状态
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态
     * @param status 状态
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}