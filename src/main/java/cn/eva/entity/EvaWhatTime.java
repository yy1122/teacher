/*
*
* EvaWhatTime.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-20
*/
package cn.eva.entity;

import java.util.Date;

public class EvaWhatTime {
    /**
     * 评价时间管理id
     */
    private String wtId;

    /**
     * 评价批次编号
     */
    private String wtNumber;

    /**
     * 开始时间
     */
    private Date wtBegin;

    /**
     * 结束时间
     */
    private Date wtEnd;

    /**
     * 该评价状态；0:未开启，1:开启
     */
    private Boolean wtState;

    /**
     * 描述
     */
    private String wtDescribe;

    /**
     * 评价时间管理id
     * @return wt_id 评价时间管理id
     */
    public String getWtId() {
        return wtId;
    }

    /**
     * 评价时间管理id
     * @param wtId 评价时间管理id
     */
    public void setWtId(String wtId) {
        this.wtId = wtId == null ? null : wtId.trim();
    }

    /**
     * 评价批次编号
     * @return wt_number 评价批次编号
     */
    public String getWtNumber() {
        return wtNumber;
    }

    /**
     * 评价批次编号
     * @param wtNumber 评价批次编号
     */
    public void setWtNumber(String wtNumber) {
        this.wtNumber = wtNumber == null ? null : wtNumber.trim();
    }

    /**
     * 开始时间
     * @return wt_begin 开始时间
     */
    public Date getWtBegin() {
        return wtBegin;
    }

    /**
     * 开始时间
     * @param wtBegin 开始时间
     */
    public void setWtBegin(Date wtBegin) {
        this.wtBegin = wtBegin;
    }

    /**
     * 结束时间
     * @return wt_end 结束时间
     */
    public Date getWtEnd() {
        return wtEnd;
    }

    /**
     * 结束时间
     * @param wtEnd 结束时间
     */
    public void setWtEnd(Date wtEnd) {
        this.wtEnd = wtEnd;
    }

    /**
     * 该评价状态；0:未开启，1:开启
     * @return wt_state 该评价状态；0:未开启，1:开启
     */
    public Boolean getWtState() {
        return wtState;
    }

    /**
     * 该评价状态；0:未开启，1:开启
     * @param wtState 该评价状态；0:未开启，1:开启
     */
    public void setWtState(Boolean wtState) {
        this.wtState = wtState;
    }

    /**
     * 描述
     * @return wt_describe 描述
     */
    public String getWtDescribe() {
        return wtDescribe;
    }

    /**
     * 描述
     * @param wtDescribe 描述
     */
    public void setWtDescribe(String wtDescribe) {
        this.wtDescribe = wtDescribe == null ? null : wtDescribe.trim();
    }
}