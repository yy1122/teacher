package cn.eva.service.admin;

import cn.eva.common.ResultJosn;
import cn.eva.entity.SysMenu;

public interface SysMenuService {

    public ResultJosn add(SysMenu menu);

    public ResultJosn delete(Long id);

    public ResultJosn update(SysMenu menu);

    public ResultJosn select(SysMenu menu);

}