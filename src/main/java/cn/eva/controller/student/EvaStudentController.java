package cn.eva.controller.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaScoreStu;
import cn.eva.plugin.spring.CheckTeachingTime;
import cn.eva.service.student.EvaStudentService;
import cn.eva.utils.UUIDUtils;
import cn.eva.utils.UserUtils;
import io.swagger.annotations.ApiOperation;

/**
 * 学生登录模块 :学生控制器 学生信息管理
 * 
 * @className:EvaStudentController * @Description: TODO
 * 
 * @author 李彬
 * @version 1.0
 * @Date:2018.09.07 11:31
 *
 */

@RequestMapping("/sstulogin")
@RestController
public class EvaStudentController {

    @Autowired
    private EvaStudentService evaStudentService;
    
    @GetMapping("/myalldetial")
    @ApiOperation(value = "根据cid,tid,sid(UU获取)查找自己对任课教师的详情")
//    @PreAuthorize("hasAuthority('sys:student:eva')")
    public ResultJosn selectBySidDeatil() {
    	String sid=UserUtils.getLoginUserUUID();
    	ResultJosn resultJosn = evaStudentService.selectMymeDeatil(sid);
    	return resultJosn;
    }
    @PostMapping("/mydetial")
    @ApiOperation(value = "根据cid,tid,sid(UU获取)查找自己对任课教师的评分详情")
    @CheckTeachingTime
    @PreAuthorize("hasAuthority('sys:student:eva')")
    public ResultJosn selectBySid(@RequestBody EvaScoreStu eScoreStu) {
        // UserUtils.getLoginUserUUID() 获取登录UUId
        eScoreStu.setsId(UserUtils.getLoginUserUUID());
        ResultJosn resultJosn = evaStudentService.select(eScoreStu);
        return resultJosn;
    }
    
    @GetMapping("/allname")
    @ApiOperation(value = "根据ssid(UUID格式)查找自己对应任课教师")
    @CheckTeachingTime
    @PreAuthorize("hasAuthority('sys:student:eva')")
    public ResultJosn selectBySidFindAllName() {
        // UserUtils.getLoginUserUUID() 获取登录UUId
        ResultJosn resultJosn = evaStudentService.selectSidAndTid(UserUtils.getLoginUserUUID());
        return resultJosn;
    }

    @PostMapping
    @ApiOperation(value = "提交打分数据")
    @CheckTeachingTime
    @PreAuthorize("hasAuthority('sys:student:eva')")
    public ResultJosn insertByGrade(@RequestBody EvaScoreStu evaScoreStu) {
        evaScoreStu.setSsId(UUIDUtils.getUUID());
        evaScoreStu.setsId(UserUtils.getLoginUserUUID()); // 改为登录sid，获取登录缓存的sid
        evaScoreStu.settId(evaScoreStu.gettId());
        ResultJosn resultJosn = evaStudentService.insertByGrade(evaScoreStu);
        return resultJosn;
    }

}
