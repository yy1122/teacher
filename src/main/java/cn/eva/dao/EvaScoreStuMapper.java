/*
*
* EvaScoreStuMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-11
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaScoreStu;
import cn.eva.example.EvaScoreStuExample;

@Mapper
public interface EvaScoreStuMapper {
	/**
	 *
	 * @mbg.generated
	 */
	long countByExample(EvaScoreStuExample example);

	/**
	 *
	 * @mbg.generated
	 */
	int deleteByExample(EvaScoreStuExample example);

	/**
	 *
	 * @mbg.generated
	 */
	int deleteByPrimaryKey(String ssId);

	/**
	 *
	 * @mbg.generated
	 */
	int insert(EvaScoreStu record);

	/**
	 *
	 * @mbg.generated
	 */
	int insertSelective(EvaScoreStu record);

	/**
	 *
	 * @mbg.generated
	 */
	List<EvaScoreStu> selectByExample(EvaScoreStuExample example);

	/**
	 *
	 * @mbg.generated
	 */
	EvaScoreStu selectByPrimaryKey(String ssId);

	/**
	 *
	 * @mbg.generated
	 */
	int updateByExampleSelective(@Param("record") EvaScoreStu record, @Param("example") EvaScoreStuExample example);

	/**
	 *
	 * @mbg.generated
	 */
	int updateByExample(@Param("record") EvaScoreStu record, @Param("example") EvaScoreStuExample example);

	/**
	 *
	 * @mbg.generated
	 */
	int updateByPrimaryKeySelective(EvaScoreStu record);

	/**
	 *
	 * @mbg.generated
	 */
	int updateByPrimaryKey(EvaScoreStu record);
}