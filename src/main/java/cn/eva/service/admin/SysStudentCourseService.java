package cn.eva.service.admin;

import cn.eva.common.ResultJosn;
import cn.eva.dto.SysClassCourseVo;
import cn.eva.entity.EvaClassCourse;

public interface SysStudentCourseService {

    /**
     * 保存学生选课信息
     * @param sysClassCourseVo
     * @return
     */
    public ResultJosn add(SysClassCourseVo sysClassCourseVo);

    /**
     * 管理员给学生选课 先点击班级得到class_id,再根据class_id得到所有课程，进行复选框勾选显示，方便后续插入
     * @param classId
     * @return
     */
    public ResultJosn selectFilterCourse(String classId);

    /**
     * 根据所选的班级选课
     * @param classId
     * @return
     */
    public ResultJosn selectByClassId(String classId);

}
