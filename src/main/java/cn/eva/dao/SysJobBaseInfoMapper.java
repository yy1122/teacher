/*
*
* SysJobBaseInfoMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-18
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.SysJobBaseInfo;
import cn.eva.entity.SysJobBaseInfoExample;

@Mapper
public interface SysJobBaseInfoMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(SysJobBaseInfoExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(SysJobBaseInfoExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Long id);

    /**
     *
     * @mbg.generated
     */
    int insert(SysJobBaseInfo record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(SysJobBaseInfo record);

    /**
     *
     * @mbg.generated
     */
    List<SysJobBaseInfo> selectByExample(SysJobBaseInfoExample example);

    /**
     *
     * @mbg.generated
     */
    SysJobBaseInfo selectByPrimaryKey(Long id);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") SysJobBaseInfo record,
            @Param("example") SysJobBaseInfoExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") SysJobBaseInfo record, @Param("example") SysJobBaseInfoExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(SysJobBaseInfo record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(SysJobBaseInfo record);
}