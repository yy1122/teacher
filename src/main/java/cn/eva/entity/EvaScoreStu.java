/*
*
* EvaScoreStu.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-11
*/
package cn.eva.entity;

public class EvaScoreStu {
    /**
     * 计分_学生中间表主键ID
     */
    private String ssId;

    /**
     * 教师id
     */
    private String tId;

    /**
     * 学生id
     */
    private String sId;

    /**
     * 课程id
     */
    private String cId;

    /**
     * 分数统计 格式 key:value,
     */
    private String scores;

    /**
     * 计分_学生中间表主键ID
     * @return ss_id 计分_学生中间表主键ID
     */
    public String getSsId() {
        return ssId;
    }

    /**
     * 计分_学生中间表主键ID
     * @param ssId 计分_学生中间表主键ID
     */
    public void setSsId(String ssId) {
        this.ssId = ssId == null ? null : ssId.trim();
    }

    /**
     * 教师id
     * @return t_id 教师id
     */
    public String gettId() {
        return tId;
    }

    /**
     * 教师id
     * @param tId 教师id
     */
    public void settId(String tId) {
        this.tId = tId == null ? null : tId.trim();
    }

    /**
     * 学生id
     * @return s_id 学生id
     */
    public String getsId() {
        return sId;
    }

    /**
     * 学生id
     * @param sId 学生id
     */
    public void setsId(String sId) {
        this.sId = sId == null ? null : sId.trim();
    }

    /**
     * 课程id
     * @return c_id 课程id
     */
    public String getcId() {
        return cId;
    }

    /**
     * 课程id
     * @param cId 课程id
     */
    public void setcId(String cId) {
        this.cId = cId == null ? null : cId.trim();
    }

    /**
     * 分数统计 格式 key:value,
     * @return scores 分数统计 格式 key:value,
     */
    public String getScores() {
        return scores;
    }

    /**
     * 分数统计 格式 key:value,
     * @param scores 分数统计 格式 key:value,
     */
    public void setScores(String scores) {
        this.scores = scores == null ? null : scores.trim();
    }
}