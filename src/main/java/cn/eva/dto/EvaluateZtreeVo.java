package cn.eva.dto;

import java.util.List;

import cn.eva.entity.EvaEvaluate;

/**
 * ztree树形(评价标准表)
 * Copyright (c) 2017 by 
 * @ClassName: ZtreeVo.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月9日 上午10:09:47
 */
public class EvaluateZtreeVo extends EvaEvaluate {

    private Boolean isParent = true;

    List<EvaluateZtreeVo> children;

    public List<EvaluateZtreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<EvaluateZtreeVo> children) {
        this.children = children;
    }

    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean isParent) {
        this.isParent = isParent;
    }

}
