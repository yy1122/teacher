/*
*
* EvaManager.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.entity;

import java.io.Serializable;
import java.util.Date;

public class EvaManager implements Serializable {
    /** 
    * @Fields serialVersionUID : TODO 
    */
    private static final long serialVersionUID = 6815979236374706546L;

    /**
     * 管理员唯一标识
     */
    private String mId;

    /**
     * 账号
     */
    private String mAccount;

    /**
     * 密码
     */
    private String mPass;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 管理员唯一标识
     * @return m_id 管理员唯一标识
     */
    public String getmId() {
        return mId;
    }

    /**
     * 管理员唯一标识
     * @param mId 管理员唯一标识
     */
    public void setmId(String mId) {
        this.mId = mId == null ? null : mId.trim();
    }

    /**
     * 账号
     * @return m_account 账号
     */
    public String getmAccount() {
        return mAccount;
    }

    /**
     * 账号
     * @param mAccount 账号
     */
    public void setmAccount(String mAccount) {
        this.mAccount = mAccount == null ? null : mAccount.trim();
    }

    /**
     * 密码
     * @return m_pass 密码
     */
    public String getmPass() {
        return mPass;
    }

    /**
     * 密码
     * @param mPass 密码
     */
    public void setmPass(String mPass) {
        this.mPass = mPass == null ? null : mPass.trim();
    }

    /**
     * 
     * @return create_time 
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 
     * @param createTime 
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 
     * @return update_time 
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 
     * @param updateTime 
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}