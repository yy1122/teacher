package cn.eva.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.ResultJosn;
import cn.eva.dto.SysTeacherCourseVo;
import cn.eva.service.admin.SysTeacherCourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "老师选课")
@RequestMapping("/sysTeacherCourse")
@RestController
/**
 * 老师选择班级，课程
 * 李绍兴 by 2018.8.10
 */
public class SysTeacherCourseController {
    @Autowired
    SysTeacherCourseService sysTeacherCourseService;

    @GetMapping(value = "/{tId}")
    @ApiOperation(value = "根据tId查找课程cId有哪些，渲染教师选课的复选框")
    @PreAuthorize("hasAuthority('sys:teacher:course:add')")
    public ResultJosn selectBytId(@PathVariable String tId) {
        ResultJosn resultJosn = sysTeacherCourseService.selectBytid(tId);
        return resultJosn;
    }

    @GetMapping(value = "/selectFilter/{tId}")
    @ApiOperation(value = "选择教师，检索课程列表，不分页")
    @PreAuthorize("hasAuthority('sys:teacher:course:add')")
    public ResultJosn selectByExample(@PathVariable String tId) {
        ResultJosn resultJosn = sysTeacherCourseService.selectFilterCourse(tId);
        return resultJosn;
    }

    @PostMapping
    @ApiOperation(value = "添加教师选课程的信息，在实现方法中，先删除对应老师所选课程")
    @PreAuthorize("hasAuthority('sys:teacher:course:add')")
    public ResultJosn add(@RequestBody SysTeacherCourseVo sysTeacherCourseVo) {
        ResultJosn resultJosn = null;
        resultJosn = sysTeacherCourseService.add(sysTeacherCourseVo);
        return resultJosn;
    }
}
