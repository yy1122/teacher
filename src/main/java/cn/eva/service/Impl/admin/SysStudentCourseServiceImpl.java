package cn.eva.service.Impl.admin;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cn.eva.common.LayuiTableResult;
import cn.eva.dto.SysClassCourseVo;
import cn.eva.entity.EvaCourse;
import cn.eva.entity.EvaTeacherCourse;
import cn.eva.example.EvaStudentExample;
import cn.eva.example.EvaTeacherCourseExample;
import cn.eva.service.admin.SysCourseService;
import cn.eva.utils.CoursesUtils;
import cn.eva.utils.UUIDUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaClassCourseMapper;
import cn.eva.dao.EvaCourseMapper;
import cn.eva.entity.EvaClassCourse;
import cn.eva.example.EvaClassCourseExample;
import cn.eva.service.admin.SysStudentCourseService;

@Service
public class SysStudentCourseServiceImpl implements SysStudentCourseService {
    @Autowired
    private EvaClassCourseMapper classCourseMapper;
    @Autowired
    SysCourseService sysCourseService;
    @Autowired
    private EvaCourseMapper evaCourseMapper;
    /**
     * 保存管理员学生选课信息信息，选择班级和课程
     * @param sysClassCourseVo
     * @return
     */
    @Override
    @Transactional
    public ResultJosn add(SysClassCourseVo sysClassCourseVo) {
        if (StringUtils.isBlank(sysClassCourseVo.getClassId())) {
            throw new IllegalArgumentException("班级classId错误");
        }
        //先删除该班级classId所选的课程信息
        delete(sysClassCourseVo.getClassId());
        //获取所有课程
        List<EvaCourse> allCouese=evaCourseMapper.selectByExample(null);
        //将课程信息存入hasmp中
        Map<String, EvaCourse> courses = CoursesUtils.getNameAccountMap(allCouese);
        
        String classId = sysClassCourseVo.getClassId();
        String cName = sysClassCourseVo.getcName();
        List<String> cIds = sysClassCourseVo.getcIds();
//        List<String> cNames = sysClassCourseVo.getcNames();
        EvaClassCourse cc=new EvaClassCourse();
        EvaClassCourseExample example = new EvaClassCourseExample();
        example.createCriteria().andCIdIn(cIds);
        for(int i=0;i<cIds.size();i++){
            cc.setClassId(classId);
            cc.setcId(cIds.get(i));
            cc.setcName(courses.get(cIds.get(i)).getcName());
            classCourseMapper.insertSelective(cc);
        }
        return new ResultJosn(HttpStatus.OK.value(), "学生班级选课成功", null);


    }



    /**
     * 根据cName查找是否重复
     * @param classId
     * @param cName
     * @return
     */
    public EvaClassCourse checkCourceName(String classId, String cName) {
        EvaClassCourseExample example = new EvaClassCourseExample();
        EvaClassCourseExample.Criteria criteria = example.createCriteria();
        criteria.andClassIdEqualTo(classId);
        criteria.andCNameEqualTo(cName);
        List<EvaClassCourse> list = classCourseMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return (EvaClassCourse) list.get(0);

    }
    @Override
    @Transactional
    public ResultJosn selectFilterCourse(String classId) {
        if (StringUtils.isBlank(classId)) {
            throw new IllegalArgumentException(" classId错误");
        }
        //查询出中间表的已选课程，班级 classId，课程cId
        EvaClassCourseExample example = new EvaClassCourseExample();
        /*构建条件，不是当前班级所选的课程*/
        example.createCriteria().andClassIdNotEqualTo(classId);

        List<EvaClassCourse> listChooseCourse = classCourseMapper.selectByExample(example);
        List<String> collect = listChooseCourse.parallelStream().map(EvaClassCourse::getcId)
                .collect(Collectors.toList());

        //查询所有课程
        LayuiTableResult allCourse = sysCourseService.select(new EvaCourse(), 1, 9999);
        allCourse.getData();
        List<EvaCourse> listAllCourse = (List<EvaCourse>) allCourse.getData();

        /*迭代移除其他班级已选课程*/
        /*构建迭代器*/
        Iterator<EvaCourse> iterator = listAllCourse.iterator();
        while (iterator.hasNext()) {
            EvaCourse evaCourse = iterator.next();
            //去掉中间表其他班级选的课程
            if (collect.contains(evaCourse.getcId())) {
                iterator.remove();
            }

        }
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), listAllCourse);
    }

    @Override
    public ResultJosn selectByClassId(String classId) {
        if(StringUtils.isBlank(classId)){
            throw new IllegalArgumentException("classId错误");
        }
        EvaClassCourseExample example = new EvaClassCourseExample();
        example.createCriteria().andClassIdEqualTo(classId);
        List<EvaClassCourse> list = classCourseMapper.selectByExample(example);
        return new ResultJosn(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list);
    }

    /**
     * 管理员删除该老师的所有课程，为了后续重新插入课程
     * @param classId
     * @return
     */
    @Transactional
    public ResultJosn delete(String classId) {
        if (classId.equals("") || classId == null) {
            throw new IllegalArgumentException("删除需要classId的参数错误为空");
        }
        EvaClassCourseExample example = new EvaClassCourseExample();
        example.createCriteria().andClassIdEqualTo(classId);
        classCourseMapper.deleteByExample(example);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "操作成功");

    }
}
