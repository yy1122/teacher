package cn.eva.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.dao.EvaClassMapper;
import cn.eva.dto.LoginUser;
import cn.eva.entity.EvaClass;
import cn.eva.utils.UserUtils;

/**
 * 
 * Copyright (c) 2017 by 
 * @ClassName: TestController.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月6日 下午12:00:24
 */
@RestController
public class TestController {

    @Autowired
    private EvaClassMapper evaClassMapper;

    @GetMapping("/hello")
    public Object test(HttpSession session) {
        LoginUser loginUser = (LoginUser) session.getAttribute("loginUser");
        if (null != loginUser) {
            LoginUser user = UserUtils.getLoginUser();
            System.err.println(user.toString());
            System.err.println(UserUtils.getLoginUserAccout());
            System.out.println(loginUser.toString());
        }
        System.out.println("sessionid:" + session.getId());
        List<EvaClass> list = evaClassMapper.selectByExample(null);
        return list;
    }

}
