/*
*
* EvaEvaluateExample.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-29
*/
package cn.eva.example;

import java.util.ArrayList;
import java.util.List;

public class EvaEvaluateExample {
    /**
     * eva_evaluate
     */
    protected String orderByClause;

    /**
     * eva_evaluate
     */
    protected boolean distinct;

    /**
     * eva_evaluate
     */
    protected List<Criteria> oredCriteria;

    /**
     *
     * @mbg.generated
     */
    public EvaEvaluateExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     *
     * @mbg.generated
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     *
     * @mbg.generated
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     *
     * @mbg.generated
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     *
     * @mbg.generated
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     *
     * @mbg.generated
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     *
     * @mbg.generated
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     *
     * @mbg.generated
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * eva_evaluate null
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEIdIsNull() {
            addCriterion("e_id is null");
            return (Criteria) this;
        }

        public Criteria andEIdIsNotNull() {
            addCriterion("e_id is not null");
            return (Criteria) this;
        }

        public Criteria andEIdEqualTo(Integer value) {
            addCriterion("e_id =", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotEqualTo(Integer value) {
            addCriterion("e_id <>", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdGreaterThan(Integer value) {
            addCriterion("e_id >", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("e_id >=", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdLessThan(Integer value) {
            addCriterion("e_id <", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdLessThanOrEqualTo(Integer value) {
            addCriterion("e_id <=", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdIn(List<Integer> values) {
            addCriterion("e_id in", values, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotIn(List<Integer> values) {
            addCriterion("e_id not in", values, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdBetween(Integer value1, Integer value2) {
            addCriterion("e_id between", value1, value2, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotBetween(Integer value1, Integer value2) {
            addCriterion("e_id not between", value1, value2, "eId");
            return (Criteria) this;
        }

        public Criteria andETitleIsNull() {
            addCriterion("e_title is null");
            return (Criteria) this;
        }

        public Criteria andETitleIsNotNull() {
            addCriterion("e_title is not null");
            return (Criteria) this;
        }

        public Criteria andETitleEqualTo(String value) {
            addCriterion("e_title =", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleNotEqualTo(String value) {
            addCriterion("e_title <>", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleGreaterThan(String value) {
            addCriterion("e_title >", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleGreaterThanOrEqualTo(String value) {
            addCriterion("e_title >=", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleLessThan(String value) {
            addCriterion("e_title <", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleLessThanOrEqualTo(String value) {
            addCriterion("e_title <=", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleLike(String value) {
            addCriterion("e_title like", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleNotLike(String value) {
            addCriterion("e_title not like", value, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleIn(List<String> values) {
            addCriterion("e_title in", values, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleNotIn(List<String> values) {
            addCriterion("e_title not in", values, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleBetween(String value1, String value2) {
            addCriterion("e_title between", value1, value2, "eTitle");
            return (Criteria) this;
        }

        public Criteria andETitleNotBetween(String value1, String value2) {
            addCriterion("e_title not between", value1, value2, "eTitle");
            return (Criteria) this;
        }

        public Criteria andEParentIdIsNull() {
            addCriterion("e_parent_id is null");
            return (Criteria) this;
        }

        public Criteria andEParentIdIsNotNull() {
            addCriterion("e_parent_id is not null");
            return (Criteria) this;
        }

        public Criteria andEParentIdEqualTo(Integer value) {
            addCriterion("e_parent_id =", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdNotEqualTo(Integer value) {
            addCriterion("e_parent_id <>", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdGreaterThan(Integer value) {
            addCriterion("e_parent_id >", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("e_parent_id >=", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdLessThan(Integer value) {
            addCriterion("e_parent_id <", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("e_parent_id <=", value, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdIn(List<Integer> values) {
            addCriterion("e_parent_id in", values, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdNotIn(List<Integer> values) {
            addCriterion("e_parent_id not in", values, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdBetween(Integer value1, Integer value2) {
            addCriterion("e_parent_id between", value1, value2, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("e_parent_id not between", value1, value2, "eParentId");
            return (Criteria) this;
        }

        public Criteria andEIsEndIsNull() {
            addCriterion("e_is_end is null");
            return (Criteria) this;
        }

        public Criteria andEIsEndIsNotNull() {
            addCriterion("e_is_end is not null");
            return (Criteria) this;
        }

        public Criteria andEIsEndEqualTo(Double value) {
            addCriterion("e_is_end =", value, "eIsEnd");
            return (Criteria) this;
        }

        public Criteria andEIsEndNotEqualTo(Double value) {
            addCriterion("e_is_end <>", value, "eIsEnd");
            return (Criteria) this;
        }

        public Criteria andEIsEndGreaterThan(Double value) {
            addCriterion("e_is_end >", value, "eIsEnd");
            return (Criteria) this;
        }

        public Criteria andEIsEndGreaterThanOrEqualTo(Double value) {
            addCriterion("e_is_end >=", value, "eIsEnd");
            return (Criteria) this;
        }

        public Criteria andEIsEndLessThan(Double value) {
            addCriterion("e_is_end <", value, "eIsEnd");
            return (Criteria) this;
        }

        public Criteria andEIsEndLessThanOrEqualTo(Double value) {
            addCriterion("e_is_end <=", value, "eIsEnd");
            return (Criteria) this;
        }

        public Criteria andEIsEndIn(List<Double> values) {
            addCriterion("e_is_end in", values, "eIsEnd");
            return (Criteria) this;
        }

        public Criteria andEIsEndNotIn(List<Double> values) {
            addCriterion("e_is_end not in", values, "eIsEnd");
            return (Criteria) this;
        }

        public Criteria andEIsEndBetween(Double value1, Double value2) {
            addCriterion("e_is_end between", value1, value2, "eIsEnd");
            return (Criteria) this;
        }

        public Criteria andEIsEndNotBetween(Double value1, Double value2) {
            addCriterion("e_is_end not between", value1, value2, "eIsEnd");
            return (Criteria) this;
        }
    }

    /**
     *  * eva_evaluate
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * eva_evaluate null
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}