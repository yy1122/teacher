package cn.eva.utils.BasicHttpResponse.impl;

import cn.eva.common.enums.BasicHttpStatus;
import cn.eva.utils.BasicHttpResponse.BasicHttpResponse;

public class BasicHttpResponseEntity implements BasicHttpResponse {
    private String code;
    private String message;
    private Object data;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the data
     */
    public Object getData() {
        return data;
    }

    public BasicHttpResponse success() {
        this.setResultCode(BasicHttpStatus.SUCCESS);
        return this;
    }

    public BasicHttpResponse success(String code, String message) {
        this.code = code;
        this.message = message;
        return this;
    }

    public BasicHttpResponse success(BasicHttpStatus status) {
        this.setResultCode(status);
        return this;
    }

    public BasicHttpResponse success(Object data) {
        this.setResultCode(BasicHttpStatus.SUCCESS);
        this.data = data;
        return this;
    }

    public BasicHttpResponse failure(BasicHttpStatus status) {
        this.setResultCode(status);
        return this;
    }

    public BasicHttpResponse failure(String code, String message) {
        this.code = code;
        this.message = message;
        return this;
    }

    public BasicHttpResponse failure(BasicHttpStatus status, Object data) {
        this.setResultCode(status);
        this.data = data;
        return this;
    }

    public BasicHttpResponse failure(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
        return this;
    }


    public void setResultCode(BasicHttpStatus status) {
        this.code = String.valueOf(status.getCode());
        this.message = status.getMessage();
    }

}
