/*
*
* EvaManagerMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.dto.SysUserSelect;
import cn.eva.entity.EvaManager;
import cn.eva.example.EvaManagerExample;

@Mapper
public interface EvaManagerMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaManagerExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaManagerExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String mId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaManager record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaManager record);

    /**
     *
     * @mbg.generated
     */
    List<EvaManager> selectByExample(EvaManagerExample example);

    /**
     *
     * @mbg.generated
     */
    EvaManager selectByPrimaryKey(String mId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaManager record, @Param("example") EvaManagerExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaManager record, @Param("example") EvaManagerExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaManager record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaManager record);

    SysUserSelect selectByUid(String uid);
}