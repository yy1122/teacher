/*
*
* EvaScoreTehMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-15
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaScoreTeh;
import cn.eva.example.EvaScoreTehExample;

@Mapper
public interface EvaScoreTehMapper {
	/**
	 *
	 * @mbg.generated
	 */
	long countByExample(EvaScoreTehExample example);

	/**
	 *
	 * @mbg.generated
	 */
	int deleteByExample(EvaScoreTehExample example);

	/**
	 *
	 * @mbg.generated
	 */
	int deleteByPrimaryKey(String stId);

	/**
	 *
	 * @mbg.generated
	 */
	int insert(EvaScoreTeh record);

	/**
	 *
	 * @mbg.generated
	 */
	int insertSelective(EvaScoreTeh record);

	/**
	 *
	 * @mbg.generated
	 */
	List<EvaScoreTeh> selectByExample(EvaScoreTehExample example);

	/**
	 *
	 * @mbg.generated
	 */
	EvaScoreTeh selectByPrimaryKey(String stId);

	/**
	 *
	 * @mbg.generated
	 */
	int updateByExampleSelective(@Param("record") EvaScoreTeh record, @Param("example") EvaScoreTehExample example);

	/**
	 *
	 * @mbg.generated
	 */
	int updateByExample(@Param("record") EvaScoreTeh record, @Param("example") EvaScoreTehExample example);

	/**
	 *
	 * @mbg.generated
	 */
	int updateByPrimaryKeySelective(EvaScoreTeh record);

	/**
	 *
	 * @mbg.generated
	 */
	int updateByPrimaryKey(EvaScoreTeh record);
}