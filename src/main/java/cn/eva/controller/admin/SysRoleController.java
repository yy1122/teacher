package cn.eva.controller.admin;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dto.LoginUser;
import cn.eva.dto.SysRoleAdd;
import cn.eva.entity.SysRole;
import cn.eva.service.UserService;
import cn.eva.service.admin.SysRoleService;
import cn.eva.utils.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 角色管理
 * Copyright (c) 2017 by 
 * @ClassName: SysRoleController.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月6日 下午8:26:58
 */
@Api(tags = "角色管理")
@RestController
@RequestMapping("/role")
public class SysRoleController {

    @Autowired
    private UserService userService;

    @Autowired
    private SysRoleService sysRoleService;

    @PostMapping
    @ApiOperation(value = "添加角色")
    @PreAuthorize("hasAuthority('sys:role:add')")
    public ResultJosn add(@RequestBody SysRoleAdd role_add) {
        ResultJosn resultJosn = sysRoleService.add(role_add);
        return resultJosn;
    }

    @DeleteMapping
    @ApiOperation(value = "单、批量删除")
    @PreAuthorize("hasAuthority('sys:role:del')")
    public ResultJosn delete(@RequestBody List<Long> ids) {
        ResultJosn resultJosn = sysRoleService.delete(ids);
        return resultJosn;
    }

    @PutMapping
    @ApiOperation(value = "修改角色信息")
    @PreAuthorize("hasAuthority('sys:role:update')")
    public ResultJosn update(@RequestBody SysRoleAdd role_add, HttpSession session) {
        ResultJosn resultJosn = sysRoleService.update(role_add);
        LoginUser loginUser = userService
                .getUserInfoByAccount(UserUtils.getLoginUserAccout() + "|" + UserUtils.getLoginUserType());
        session.setAttribute("loginUser", loginUser);
        return resultJosn;
    }

    @GetMapping
    @ApiOperation(value = "模糊查询")
    @PreAuthorize("hasAnyAuthority('sys:role:query','sys:role:update')")
    public LayuiTableResult select(SysRole role, @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        LayuiTableResult result = sysRoleService.select(role, page, pageSize);
        return result;
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAnyAuthority('sys:role:query','sys:role:update')")
    @ApiOperation(value = "根据id查找角色、包括角色的权限")
    public ResultJosn selectById(@PathVariable Long id) {
        ResultJosn resultJosn = sysRoleService.selectByid(id);
        return resultJosn;
    }

}
