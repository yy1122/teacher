package cn.eva.service.admin;

import java.util.List;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dto.SysManagerAdd;
import cn.eva.entity.EvaManager;

public interface SysManagerService {

    ResultJosn add(SysManagerAdd user);

    ResultJosn delete(List<String> uids);

    ResultJosn updateByExample(SysManagerAdd user);

    LayuiTableResult selectByExample(EvaManager user, Integer page, Integer pageSize);

    ResultJosn selectByUid(String uid);
}
