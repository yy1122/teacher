/*
*
* EvaTeacherCourseMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-10
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaTeacherCourse;
import cn.eva.example.EvaTeacherCourseExample;

@Mapper
public interface EvaTeacherCourseMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaTeacherCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaTeacherCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String tcId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaTeacherCourse record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaTeacherCourse record);

    /**
     *
     * @mbg.generated
     */
    List<EvaTeacherCourse> selectByExample(EvaTeacherCourseExample example);

    /**
     *
     * @mbg.generated
     */
    EvaTeacherCourse selectByPrimaryKey(String tcId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaTeacherCourse record,
            @Param("example") EvaTeacherCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaTeacherCourse record, @Param("example") EvaTeacherCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaTeacherCourse record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaTeacherCourse record);
}