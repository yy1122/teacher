package cn.eva.controller.teacher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaScoreTehVo;
import cn.eva.entity.EvaTeacherCourseVo;
import cn.eva.entity.SyscourseGrade;
import cn.eva.plugin.spring.CheckTeachingTime;
import cn.eva.service.admin.courseGrade.rankingListService;
import cn.eva.service.teacher.EvaTeacherService;
import cn.eva.utils.UUIDUtils;
import cn.eva.utils.UserUtils;
import io.swagger.annotations.ApiOperation;

/**
 * Copyright (c) 2018 by
 * 
 * @ClassName: EvaTeacherController.java
 * @Description: TODO
 * @author 李彬
 * @Date: 2018.09.15 10:40 教师测评管理 老师对老师的综合测评
 * @version: V1.0
 */
@RestController
@RequestMapping("/teacherLogin")
public class EvaTeacherController {
    @Autowired
    private EvaTeacherService evaTeacherService;

    @Autowired
    private rankingListService rankinglistService;

    @GetMapping(value = "/myStudents/{classId}")
    @ApiOperation(value = "查询我上课的班级学生")
//	    @PreAuthorize("hasAuthority('sys:teacher:eva')")
    public ResultJosn selectByMyStudentsExample(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize, @PathVariable String classId) {
        // 获取当前登录UUID
        System.err.println(page + ": " + pageSize);
        ResultJosn resultJosn = evaTeacherService.selectsMyStudents(page, pageSize, classId);
        return resultJosn;
    }

    @GetMapping(value = "/withMeCourses")
    @ApiOperation(value = "查询和当前登录老师课程班级信息")
//    @PreAuthorize("hasAuthority('sys:teacher:eva')")
    public ResultJosn selectByMyCoursesExample() {
        // 获取当前登录UUID
        ResultJosn resultJosn = evaTeacherService.selectWithMeCourses(UserUtils.getLoginUserUUID());
        return resultJosn;
    }

    @GetMapping(value = "/withMe")
    @ApiOperation(value = "查询和当前登录用户相同部门的老师信息、评价列表条件模糊查询")
    @CheckTeachingTime
    @PreAuthorize("hasAuthority('sys:teacher:eva')")
    public ResultJosn selectByExample(EvaTeacherCourseVo eTehCVo) {
        // 获取当前登录UUID,注入eTehCVO
        eTehCVo.settId(UserUtils.getLoginUserUUID());
        ResultJosn resultJosn = evaTeacherService.selectAllTcAndalikedpMH(eTehCVo);
        return resultJosn;
    }

    @GetMapping(value = "/myself")
    @ApiOperation(value = "查询自己信息、评价列表条件模糊查询")
    @CheckTeachingTime
    @PreAuthorize("hasAuthority('sys:teacher:eva')")
    public ResultJosn selectByMyselfExample(EvaTeacherCourseVo eTehCVo) {
        // 获取当前登录UUID,注入eTehCVO
        eTehCVo.settId(UserUtils.getLoginUserUUID());
        ResultJosn resultJosn = evaTeacherService.selectMyselfTcAndalikedpMH(eTehCVo);
        return resultJosn;
    }

    @PostMapping("/mydetial")
    @ApiOperation(value = "根据cid,tid_two,tid_one(UU获取)查找自己对任课教师的评分详情")
    @CheckTeachingTime
    @PreAuthorize("hasAuthority('sys:teacher:eva')")
    public ResultJosn selectBySid(@RequestBody EvaScoreTehVo eScoreTehVo) {
        // UserUtils.getLoginUserUUID() 获取登录UUId
        eScoreTehVo.settIdOne(UserUtils.getLoginUserUUID());
        eScoreTehVo.settIdTwo(eScoreTehVo.gettId());
        ResultJosn resultJosn = evaTeacherService.selectOneDetail(eScoreTehVo);
        return resultJosn;
    }

    @PostMapping
    @ApiOperation(value = "提交打分数据")
    @CheckTeachingTime
    @PreAuthorize("hasAuthority('sys:teacher:eva')")
    public ResultJosn insertByGrade(@RequestBody EvaScoreTehVo eScoreTehVo) {
        eScoreTehVo.setStId(UUIDUtils.getUUID()); // 为表主键添加UUId
        eScoreTehVo.settIdOne(UserUtils.getLoginUserUUID()); // 获取登录缓存的sid
        eScoreTehVo.settIdTwo(eScoreTehVo.gettId());
        ResultJosn resultJosn = evaTeacherService.insertByGrade(eScoreTehVo);
        return resultJosn;
    }

    @GetMapping("/teacher_course_List")
    /* @ApiOperation(value = "查询分数排名") */
    public ResultJosn selectBySidFindAllName(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize, SyscourseGrade syscourseGrade) {
        // UserUtils.getLoginUserUUID() 获取登录UUId
        syscourseGrade.settId(UserUtils.getLoginUserUUID());
        ResultJosn resultJosn = rankinglistService.selectInfo(page, pageSize, syscourseGrade);
        return resultJosn;
    }

}
