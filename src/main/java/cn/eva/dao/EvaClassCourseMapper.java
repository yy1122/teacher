/*
*
* EvaClassCourseMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-10
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaClassCourse;
import cn.eva.example.EvaClassCourseExample;

@Mapper
public interface EvaClassCourseMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaClassCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaClassCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer ccId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaClassCourse record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaClassCourse record);

    /**
     *
     * @mbg.generated
     */
    List<EvaClassCourse> selectByExample(EvaClassCourseExample example);

    /**
     *
     * @mbg.generated
     */
    EvaClassCourse selectByPrimaryKey(Integer ccId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaClassCourse record,
            @Param("example") EvaClassCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaClassCourse record, @Param("example") EvaClassCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaClassCourse record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaClassCourse record);
}