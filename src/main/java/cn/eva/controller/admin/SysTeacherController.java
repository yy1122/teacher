package cn.eva.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaTeacher;
import cn.eva.entity.EvaTeacherVo;
import cn.eva.service.admin.SysTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "后台教师")
@RequestMapping("/sysTeacher")
@RestController
public class SysTeacherController {

    @Autowired
    SysTeacherService teacherService;

    @PostMapping
    @ApiOperation(value = "添加教师", notes = "添加教师")
    @PreAuthorize("hasAnyAuthority('sys:teacher:add')")
    public ResultJosn add(@RequestBody EvaTeacher teacher) {
        ResultJosn resultJosn = null;
        resultJosn = teacherService.add(teacher);
        return resultJosn;
    }

    @DeleteMapping
    @ApiOperation(value = "单、批量删除教师")
    @PreAuthorize("hasAuthority('sys:teacher:del')")
    public ResultJosn delete(@RequestBody List<String> deleteids) {
        ResultJosn resultJosn = null;
        resultJosn = teacherService.delete(deleteids);
        return resultJosn;
    }

    @PutMapping
    @ApiOperation(value = "更新教师信息")
    @PreAuthorize("hasAuthority('sys:teacher:update')")
    public ResultJosn update(@RequestBody EvaTeacher teacher) {
        ResultJosn resultJosn = null;
        resultJosn = teacherService.update(teacher);
        return resultJosn;
    }

    @GetMapping
    @ApiOperation(value = "教师列表条件模糊查询")
    @PreAuthorize("hasAnyAuthority('sys:teacher:query','sys:teacher:update','sys:teacher:del')")
    public LayuiTableResult selectTdIdAndDName(EvaTeacherVo evaTeacherVo,
            @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer pageSize) {
        LayuiTableResult resultJosn = teacherService.select(evaTeacherVo, page, pageSize);
        return resultJosn;
    }

    @GetMapping(value = "/{tId}")
    @ApiOperation(value = "根据tId查找教师详细信息")
    @PreAuthorize("hasAnyAuthority('sys:teacher:query','sys:teacher:update','sys:teacher:del')")
    public ResultJosn selectByUid(@PathVariable String tId) {
        ResultJosn resultJosn = teacherService.selectBytId(tId);
        return resultJosn;
    }

}
