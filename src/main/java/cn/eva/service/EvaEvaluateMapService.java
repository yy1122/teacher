package cn.eva.service;

import java.util.Map;

import cn.eva.entity.EvaEvaluate;

public interface EvaEvaluateMapService {

    public Map<Integer, EvaEvaluate> selectAll();

}
