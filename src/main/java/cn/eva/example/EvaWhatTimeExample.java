/*
*
* EvaWhatTimeExample.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-20
*/
package cn.eva.example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EvaWhatTimeExample {
    /**
     * eva_whattime
     */
    protected String orderByClause;

    /**
     * eva_whattime
     */
    protected boolean distinct;

    /**
     * eva_whattime
     */
    protected List<Criteria> oredCriteria;

    /**
     *
     * @mbg.generated
     */
    public EvaWhatTimeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     *
     * @mbg.generated
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     *
     * @mbg.generated
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     *
     * @mbg.generated
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     *
     * @mbg.generated
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     *
     * @mbg.generated
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     *
     * @mbg.generated
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     *
     * @mbg.generated
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * eva_whattime null
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andWtIdIsNull() {
            addCriterion("wt_id is null");
            return (Criteria) this;
        }

        public Criteria andWtIdIsNotNull() {
            addCriterion("wt_id is not null");
            return (Criteria) this;
        }

        public Criteria andWtIdEqualTo(String value) {
            addCriterion("wt_id =", value, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdNotEqualTo(String value) {
            addCriterion("wt_id <>", value, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdGreaterThan(String value) {
            addCriterion("wt_id >", value, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdGreaterThanOrEqualTo(String value) {
            addCriterion("wt_id >=", value, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdLessThan(String value) {
            addCriterion("wt_id <", value, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdLessThanOrEqualTo(String value) {
            addCriterion("wt_id <=", value, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdLike(String value) {
            addCriterion("wt_id like", value, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdNotLike(String value) {
            addCriterion("wt_id not like", value, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdIn(List<String> values) {
            addCriterion("wt_id in", values, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdNotIn(List<String> values) {
            addCriterion("wt_id not in", values, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdBetween(String value1, String value2) {
            addCriterion("wt_id between", value1, value2, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtIdNotBetween(String value1, String value2) {
            addCriterion("wt_id not between", value1, value2, "wtId");
            return (Criteria) this;
        }

        public Criteria andWtNumberIsNull() {
            addCriterion("wt_number is null");
            return (Criteria) this;
        }

        public Criteria andWtNumberIsNotNull() {
            addCriterion("wt_number is not null");
            return (Criteria) this;
        }

        public Criteria andWtNumberEqualTo(String value) {
            addCriterion("wt_number =", value, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberNotEqualTo(String value) {
            addCriterion("wt_number <>", value, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberGreaterThan(String value) {
            addCriterion("wt_number >", value, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberGreaterThanOrEqualTo(String value) {
            addCriterion("wt_number >=", value, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberLessThan(String value) {
            addCriterion("wt_number <", value, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberLessThanOrEqualTo(String value) {
            addCriterion("wt_number <=", value, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberLike(String value) {
            addCriterion("wt_number like", value, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberNotLike(String value) {
            addCriterion("wt_number not like", value, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberIn(List<String> values) {
            addCriterion("wt_number in", values, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberNotIn(List<String> values) {
            addCriterion("wt_number not in", values, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberBetween(String value1, String value2) {
            addCriterion("wt_number between", value1, value2, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtNumberNotBetween(String value1, String value2) {
            addCriterion("wt_number not between", value1, value2, "wtNumber");
            return (Criteria) this;
        }

        public Criteria andWtBeginIsNull() {
            addCriterion("wt_begin is null");
            return (Criteria) this;
        }

        public Criteria andWtBeginIsNotNull() {
            addCriterion("wt_begin is not null");
            return (Criteria) this;
        }

        public Criteria andWtBeginEqualTo(Date value) {
            addCriterion("wt_begin =", value, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtBeginNotEqualTo(Date value) {
            addCriterion("wt_begin <>", value, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtBeginGreaterThan(Date value) {
            addCriterion("wt_begin >", value, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtBeginGreaterThanOrEqualTo(Date value) {
            addCriterion("wt_begin >=", value, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtBeginLessThan(Date value) {
            addCriterion("wt_begin <", value, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtBeginLessThanOrEqualTo(Date value) {
            addCriterion("wt_begin <=", value, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtBeginIn(List<Date> values) {
            addCriterion("wt_begin in", values, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtBeginNotIn(List<Date> values) {
            addCriterion("wt_begin not in", values, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtBeginBetween(Date value1, Date value2) {
            addCriterion("wt_begin between", value1, value2, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtBeginNotBetween(Date value1, Date value2) {
            addCriterion("wt_begin not between", value1, value2, "wtBegin");
            return (Criteria) this;
        }

        public Criteria andWtEndIsNull() {
            addCriterion("wt_end is null");
            return (Criteria) this;
        }

        public Criteria andWtEndIsNotNull() {
            addCriterion("wt_end is not null");
            return (Criteria) this;
        }

        public Criteria andWtEndEqualTo(Date value) {
            addCriterion("wt_end =", value, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtEndNotEqualTo(Date value) {
            addCriterion("wt_end <>", value, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtEndGreaterThan(Date value) {
            addCriterion("wt_end >", value, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtEndGreaterThanOrEqualTo(Date value) {
            addCriterion("wt_end >=", value, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtEndLessThan(Date value) {
            addCriterion("wt_end <", value, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtEndLessThanOrEqualTo(Date value) {
            addCriterion("wt_end <=", value, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtEndIn(List<Date> values) {
            addCriterion("wt_end in", values, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtEndNotIn(List<Date> values) {
            addCriterion("wt_end not in", values, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtEndBetween(Date value1, Date value2) {
            addCriterion("wt_end between", value1, value2, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtEndNotBetween(Date value1, Date value2) {
            addCriterion("wt_end not between", value1, value2, "wtEnd");
            return (Criteria) this;
        }

        public Criteria andWtStateIsNull() {
            addCriterion("wt_state is null");
            return (Criteria) this;
        }

        public Criteria andWtStateIsNotNull() {
            addCriterion("wt_state is not null");
            return (Criteria) this;
        }

        public Criteria andWtStateEqualTo(Boolean value) {
            addCriterion("wt_state =", value, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtStateNotEqualTo(Boolean value) {
            addCriterion("wt_state <>", value, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtStateGreaterThan(Boolean value) {
            addCriterion("wt_state >", value, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtStateGreaterThanOrEqualTo(Boolean value) {
            addCriterion("wt_state >=", value, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtStateLessThan(Boolean value) {
            addCriterion("wt_state <", value, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtStateLessThanOrEqualTo(Boolean value) {
            addCriterion("wt_state <=", value, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtStateIn(List<Boolean> values) {
            addCriterion("wt_state in", values, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtStateNotIn(List<Boolean> values) {
            addCriterion("wt_state not in", values, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtStateBetween(Boolean value1, Boolean value2) {
            addCriterion("wt_state between", value1, value2, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtStateNotBetween(Boolean value1, Boolean value2) {
            addCriterion("wt_state not between", value1, value2, "wtState");
            return (Criteria) this;
        }

        public Criteria andWtDescribeIsNull() {
            addCriterion("wt_describe is null");
            return (Criteria) this;
        }

        public Criteria andWtDescribeIsNotNull() {
            addCriterion("wt_describe is not null");
            return (Criteria) this;
        }

        public Criteria andWtDescribeEqualTo(String value) {
            addCriterion("wt_describe =", value, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeNotEqualTo(String value) {
            addCriterion("wt_describe <>", value, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeGreaterThan(String value) {
            addCriterion("wt_describe >", value, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeGreaterThanOrEqualTo(String value) {
            addCriterion("wt_describe >=", value, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeLessThan(String value) {
            addCriterion("wt_describe <", value, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeLessThanOrEqualTo(String value) {
            addCriterion("wt_describe <=", value, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeLike(String value) {
            addCriterion("wt_describe like", value, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeNotLike(String value) {
            addCriterion("wt_describe not like", value, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeIn(List<String> values) {
            addCriterion("wt_describe in", values, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeNotIn(List<String> values) {
            addCriterion("wt_describe not in", values, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeBetween(String value1, String value2) {
            addCriterion("wt_describe between", value1, value2, "wtDescribe");
            return (Criteria) this;
        }

        public Criteria andWtDescribeNotBetween(String value1, String value2) {
            addCriterion("wt_describe not between", value1, value2, "wtDescribe");
            return (Criteria) this;
        }
    }

    /**
     *  * eva_whattime
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * eva_whattime null
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}