package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.eva.entity.EvaScoreStu;
import cn.eva.entity.EvaScoreTeh;
import cn.eva.entity.SyscourseGrade;

@Mapper
public interface SyscourseGradeMapper {
	
	List<SyscourseGrade> selectrankingList(SyscourseGrade syscourseGrade);
	
	List<EvaScoreTeh>  selecttIdtwoByest();
	
	List<EvaScoreTeh> selecttScoresInfo(EvaScoreTeh tea);
	
	List<EvaScoreStu>  selecttIdByesc();
	
	List<EvaScoreStu> selectstuScoresInfo(EvaScoreStu stu);
	
	List<SyscourseGrade> tehCourseList(SyscourseGrade scg);
	
	
	List<SyscourseGrade> tehCourse(SyscourseGrade scg);
	
	
}
