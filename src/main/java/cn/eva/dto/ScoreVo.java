package cn.eva.dto;

public class ScoreVo {

    /**
     * 题号
     */
    private String tId;

    /**
     * 得分
     */
    private String value;

    public String gettId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
