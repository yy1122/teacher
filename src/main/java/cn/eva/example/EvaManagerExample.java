/*
*
* EvaManagerExample.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EvaManagerExample {
    /**
     * eva_manager
     */
    protected String orderByClause;

    /**
     * eva_manager
     */
    protected boolean distinct;

    /**
     * eva_manager
     */
    protected List<Criteria> oredCriteria;

    /**
     *
     * @mbg.generated
     */
    public EvaManagerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     *
     * @mbg.generated
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     *
     * @mbg.generated
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     *
     * @mbg.generated
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     *
     * @mbg.generated
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     *
     * @mbg.generated
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     *
     * @mbg.generated
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     *
     * @mbg.generated
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * eva_manager null
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMIdIsNull() {
            addCriterion("m_id is null");
            return (Criteria) this;
        }

        public Criteria andMIdIsNotNull() {
            addCriterion("m_id is not null");
            return (Criteria) this;
        }

        public Criteria andMIdEqualTo(String value) {
            addCriterion("m_id =", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotEqualTo(String value) {
            addCriterion("m_id <>", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdGreaterThan(String value) {
            addCriterion("m_id >", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdGreaterThanOrEqualTo(String value) {
            addCriterion("m_id >=", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdLessThan(String value) {
            addCriterion("m_id <", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdLessThanOrEqualTo(String value) {
            addCriterion("m_id <=", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdLike(String value) {
            addCriterion("m_id like", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotLike(String value) {
            addCriterion("m_id not like", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdIn(List<String> values) {
            addCriterion("m_id in", values, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotIn(List<String> values) {
            addCriterion("m_id not in", values, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdBetween(String value1, String value2) {
            addCriterion("m_id between", value1, value2, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotBetween(String value1, String value2) {
            addCriterion("m_id not between", value1, value2, "mId");
            return (Criteria) this;
        }

        public Criteria andMAccountIsNull() {
            addCriterion("m_account is null");
            return (Criteria) this;
        }

        public Criteria andMAccountIsNotNull() {
            addCriterion("m_account is not null");
            return (Criteria) this;
        }

        public Criteria andMAccountEqualTo(String value) {
            addCriterion("m_account =", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountNotEqualTo(String value) {
            addCriterion("m_account <>", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountGreaterThan(String value) {
            addCriterion("m_account >", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountGreaterThanOrEqualTo(String value) {
            addCriterion("m_account >=", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountLessThan(String value) {
            addCriterion("m_account <", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountLessThanOrEqualTo(String value) {
            addCriterion("m_account <=", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountLike(String value) {
            addCriterion("m_account like", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountNotLike(String value) {
            addCriterion("m_account not like", value, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountIn(List<String> values) {
            addCriterion("m_account in", values, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountNotIn(List<String> values) {
            addCriterion("m_account not in", values, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountBetween(String value1, String value2) {
            addCriterion("m_account between", value1, value2, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMAccountNotBetween(String value1, String value2) {
            addCriterion("m_account not between", value1, value2, "mAccount");
            return (Criteria) this;
        }

        public Criteria andMPassIsNull() {
            addCriterion("m_pass is null");
            return (Criteria) this;
        }

        public Criteria andMPassIsNotNull() {
            addCriterion("m_pass is not null");
            return (Criteria) this;
        }

        public Criteria andMPassEqualTo(String value) {
            addCriterion("m_pass =", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassNotEqualTo(String value) {
            addCriterion("m_pass <>", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassGreaterThan(String value) {
            addCriterion("m_pass >", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassGreaterThanOrEqualTo(String value) {
            addCriterion("m_pass >=", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassLessThan(String value) {
            addCriterion("m_pass <", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassLessThanOrEqualTo(String value) {
            addCriterion("m_pass <=", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassLike(String value) {
            addCriterion("m_pass like", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassNotLike(String value) {
            addCriterion("m_pass not like", value, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassIn(List<String> values) {
            addCriterion("m_pass in", values, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassNotIn(List<String> values) {
            addCriterion("m_pass not in", values, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassBetween(String value1, String value2) {
            addCriterion("m_pass between", value1, value2, "mPass");
            return (Criteria) this;
        }

        public Criteria andMPassNotBetween(String value1, String value2) {
            addCriterion("m_pass not between", value1, value2, "mPass");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    /**
     *  * eva_manager
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * eva_manager null
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}