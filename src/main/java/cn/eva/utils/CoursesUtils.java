package cn.eva.utils;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import cn.eva.entity.EvaCourse;

/**
 * 课程工具类，存入hasmap
 * @author Li-Ob
 *
 */
public class CoursesUtils {
	public static Map<String, EvaCourse> getNameAccountMap(List<EvaCourse> accounts) {
	    return accounts.stream().collect(Collectors.toMap(EvaCourse::getcId, Function.identity(), (key1, key2) -> key2));
	}
}
