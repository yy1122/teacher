package cn.eva.controller.admin;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaStudent;
import cn.eva.service.admin.SysStudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "学生")
@RequestMapping("/sysStudent")
@RestController
public class SysStudentController {

    @Autowired
    SysStudentService studentService;

    @PostMapping
    @ApiOperation(value = "添加学生", notes = "添加学生")
    @PreAuthorize("hasAuthority('sys:student:add')")
    public ResultJosn add(@RequestBody EvaStudent student) {
        ResultJosn resultJosn = null;
        resultJosn = studentService.add(student);
        return resultJosn;
    }

    @DeleteMapping
    @ApiOperation(value = "单、批量删除学生")
    @PreAuthorize("hasAuthority('sys:student:del')")
    public ResultJosn delete(@RequestBody List<String> deleteids) {
        ResultJosn resultJosn = null;
        resultJosn = studentService.delete(deleteids);
        return resultJosn;
    }

    @PutMapping
    @ApiOperation(value = "更新学生信息")
    @PreAuthorize("hasAuthority('sys:student:update')")
    public ResultJosn update(@RequestBody EvaStudent student) {
        ResultJosn resultJosn = null;
        resultJosn = studentService.update(student);
        return resultJosn;
    }

    @GetMapping
    @ApiOperation(value = "学生列表条件模糊查询")
    @PreAuthorize("hasAnyAuthority('sys:student:query','sys:student:update','sys:student:del')")
    public LayuiTableResult selectByExample(EvaStudent student, @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        LayuiTableResult resultJosn = studentService.select(student, page, pageSize);
        return resultJosn;
    }

    @GetMapping(value = "/{sId}")
    @ApiOperation(value = "根据sId查找学生详细信息")
    @PreAuthorize("hasAnyAuthority('sys:student:query','sys:student:update','sys:student:del')")
    public ResultJosn selectByUid(@PathVariable String sId) {
        ResultJosn resultJosn = studentService.selectBysId(sId);
        return resultJosn;
    }

    @PostMapping("/impExcel")
    @ApiOperation(value = "批量导入学生excel")
    @PreAuthorize("hasAuthority('sys:student:add')")
    public ResultJosn importExcel(@RequestParam("file") MultipartFile file) throws Exception {
        if (null == file) {
            throw new IllegalArgumentException("文件为空");
        }
        return studentService.importExcel(file);
    }

    @GetMapping("/exportExcel")
    @ApiOperation(value = "批量导入学生excel")
    @PreAuthorize("hasAuthority('sys:student:export')")
    public ResponseEntity<byte[]> exportExcel(EvaStudent evaStudent, @RequestParam(defaultValue = "xls") String type)
            throws Exception {
        ByteArrayOutputStream outputStream = studentService.exportExcel(evaStudent, type);
        String fileName = new String(
                ((StringUtils.isBlank(evaStudent.getClassId()) ? "" : evaStudent.getClassId() + "班") + "学生基本信息表."
                        + type).getBytes("UTF-8"),
                "iso-8859-1");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", fileName);
        return new ResponseEntity<byte[]>(outputStream.toByteArray(), headers, HttpStatus.OK);
    }

}
