package cn.eva.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.ResultJosn;
import cn.eva.dto.EvaluateZtreeVo;
import cn.eva.entity.EvaEvaluate;
import cn.eva.service.admin.SysEvaluateService;
import io.swagger.annotations.ApiOperation;

/**
 * 评教指标表
 * Copyright (c) 2017 by 
 * @ClassName: SysEvaluate.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月9日 上午9:53:25
 */
@RestController
@RequestMapping("/evaluate")
public class SysEvaluateController {

    @Autowired
    private SysEvaluateService sysEvaluateService;

    @GetMapping
    @ApiOperation(value = "树形展示所有的评价标准", notes = "树形展示所有的评价标准")
    List<EvaluateZtreeVo> listAllZtree() {
        return sysEvaluateService.listAllZtree();
    }

    @GetMapping("/simple")
    @ApiOperation(value = "simple展示所有的评价标准", notes = "simple展示所有的评价标准")
    List<EvaEvaluate> listAllZtreeSimple() {
        return sysEvaluateService.listAllZtreeSimple();
    }
    

    @GetMapping("/student")
    @ApiOperation(value = "树形展示学生评价标准", notes = "树形展示学生评价标准")
    List<EvaluateZtreeVo> listStudentZtree() {
        return sysEvaluateService.listStudentZtree();
    }

    @GetMapping("/teacher")
    @ApiOperation(value = "树形展示教师评价标准", notes = "树形展示教师评价标准")
    List<EvaluateZtreeVo> listTeacherZtree() {
        return sysEvaluateService.listTeacherZtree();
    }

    @GetMapping("/myselfeva")
    @ApiOperation(value = "树形展示自我评价标准", notes = "树形展示自我评价标准")
    List<EvaluateZtreeVo> listMyselfevaZtree() {
        return sysEvaluateService.listMyselfevaZtree();
    }

    @PostMapping
    @ApiOperation(value = "增加评价标准", notes = "增加评价标准")
    public ResultJosn add(@RequestBody EvaEvaluate evaEvaluate) {
        return sysEvaluateService.add(evaEvaluate);
    }

    @DeleteMapping("/{eId}")
    @ApiOperation(value = "删除评价标准", notes = "删除评价标准")
    public ResultJosn delete(@PathVariable(value = "eId") Integer eId) {
        return sysEvaluateService.delete(eId);
    }

    @PutMapping
    @ApiOperation(value = "修改评价标准", notes = "修改评价标准")
    public ResultJosn update(@RequestBody EvaEvaluate evaEvaluate) {
        return sysEvaluateService.update(evaEvaluate);
    }

}
