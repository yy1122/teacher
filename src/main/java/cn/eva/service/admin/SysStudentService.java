package cn.eva.service.admin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaStudent;

/**
 * @Title 学生管理
 * @author wzh
 *
 */
public interface SysStudentService {

    /**
     * 添加学生
     */
    public ResultJosn add(EvaStudent student);

    /**
     * 根据 sId 删除学生
     */
    public ResultJosn delete(List<String> sId);

    /**
     * 修改学生信息
     */
    public ResultJosn update(EvaStudent student);

    /**
     * 查询所有学生 传入分页数据
     * @param evaTeacher
     * @param page
     * @param pageSize
     * @return
     */
    public LayuiTableResult select(EvaStudent student, Integer page, Integer pageSize);

    /**
     * 根据 sId 查询单个学生
     */
    public ResultJosn selectBysId(String sId);

    /**
     * excel批量导入学生信息
     * @Title: importExcel
     * @param @param file
     * @param @return
     * @return ResultJosn
     * @throws
     */
    public ResultJosn importExcel(@RequestParam("file") MultipartFile file) throws Exception;

    /**
     * @Title: exportExcel   
     * @Description: excel批量到出学生信息表
     * @param: @param evaStudent
     * @param: @param type      
     * @return: void      
     * @throws
     */
    public ByteArrayOutputStream exportExcel(EvaStudent evaStudent, String type) throws IOException;

}
