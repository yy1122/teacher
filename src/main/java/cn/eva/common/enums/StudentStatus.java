package cn.eva.common.enums;

/**
 * 学生类型
 * Copyright (c) 2017 by 
 * @ClassName: TeacherStatus.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月6日 下午6:34:19
 */
public enum StudentStatus {

    //学生类型（1在校2退学3休学）
    IN("1", "在校"), OUT("2", "退学"), Suspension("3", "休学");

    private String code;

    private String name;

    private StudentStatus(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
