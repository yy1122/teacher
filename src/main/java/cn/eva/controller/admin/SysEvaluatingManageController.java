package cn.eva.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaWhatTime;
import cn.eva.job.impl.StatisticalFractionJob;
import cn.eva.service.admin.SysEvaluatingManageService;
import cn.eva.utils.UUIDUtils;
import io.swagger.annotations.ApiOperation;

/**
 * 评测时间、任务管理
 * @author Li-Ob
 *
 */
@RestController
@RequestMapping("/evaManager")
public class SysEvaluatingManageController {
    @Autowired
    private SysEvaluatingManageService sysEvaManage;

    @SuppressWarnings("unlikely-arg-type")
    @PostMapping
    @ApiOperation(value = "添加一次评测任务")
    @PreAuthorize("hasAuthority('sys:evaluate:add')")
    public ResultJosn save(@RequestBody EvaWhatTime evaWhatTime) {
        /**
         * 判断wtState的值
         */
        if ("".equals(evaWhatTime.getWtState()) || evaWhatTime.getWtState() == null) {
            evaWhatTime.setWtState(false);
        }
        if ("1".equals(evaWhatTime.getWtState())) {
            evaWhatTime.setWtState(true);
        }
        /**
         * 判断时间是否为空
         */
        if (evaWhatTime.getWtBegin() == null || evaWhatTime.getWtEnd() == null) {
            throw new IllegalArgumentException("时间错误");
        }
        System.err.println(evaWhatTime.getWtBegin());
        evaWhatTime.setWtId(UUIDUtils.getUUID() + StatisticalFractionJob.class.getName());
        ResultJosn result = sysEvaManage.add(evaWhatTime);
        return result;
    }

    @DeleteMapping("/{wtId}")
    @ApiOperation(value = "删除一次评测任务")
    @PreAuthorize("hasAuthority('sys:evaluate:del')")
    public ResultJosn deleteEvaManger(@PathVariable String wtId) {
        ResultJosn result = sysEvaManage.delete(wtId);
        return result;
    }

    @PutMapping
    @ApiOperation(value = "修改评测任务")
    @PreAuthorize("hasAnyAuthority('sys:evaluate:update')")
    public ResultJosn updateEvaManger(@RequestBody EvaWhatTime evaWhatTime) {
        ResultJosn result = sysEvaManage.update(evaWhatTime);
        return result;
    }

    @PutMapping("/myState")
    @ApiOperation(value = "修改评测任务状态")
    @PreAuthorize("hasAnyAuthority('sys:evaluate:update')")
    public ResultJosn updateEvaMangerState(@RequestBody EvaWhatTime evaWhatTime) {
        ResultJosn result = sysEvaManage.updateState(evaWhatTime);
        return result;
    }

    @GetMapping
    @ApiOperation(value = "查询评测任务")
    @PreAuthorize("hasAnyAuthority('sys:evaluate:query','sys:evaluate:update','sys:evaluate:del')")
    public LayuiTableResult selectEvaManger(EvaWhatTime evaWhatTime, @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        LayuiTableResult resultJosn = sysEvaManage.select(evaWhatTime, page, pageSize);
        return resultJosn;
    }

    @GetMapping(value = "/{wtId}")
    @ApiOperation(value = "根据wtid查询单个评测任务详情")
    @PreAuthorize("hasAnyAuthority('sys:evaluate:query','sys:evaluate:update','sys:evaluate:del')")
    public ResultJosn selectBytId(@PathVariable String wtId) {
        ResultJosn resultJosn = sysEvaManage.selectBytId(wtId);
        return resultJosn;
    }
}
