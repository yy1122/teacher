package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.eva.entity.EvaStudentVo;

@Mapper
public interface EvaStudentVoMapper {
	//查询TameCnameKname
	List<EvaStudentVo> selectTnameCnameKname(String sid);
	
	//根据班级查询课程信息
	List<EvaStudentVo> selectMyDeatileCname(String ccid);
}
