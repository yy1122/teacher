/*
*
* EvaScoreMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-18
*/
package cn.eva.dao;

import cn.eva.entity.EvaScore;
import cn.eva.example.EvaScoreExample;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface EvaScoreMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaScoreExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaScoreExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String scoreId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaScore record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaScore record);

    /**
     *
     * @mbg.generated
     */
    List<EvaScore> selectByExample(EvaScoreExample example);

    /**
     *
     * @mbg.generated
     */
    EvaScore selectByPrimaryKey(String scoreId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaScore record, @Param("example") EvaScoreExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaScore record, @Param("example") EvaScoreExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaScore record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaScore record);
}