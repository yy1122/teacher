package cn.eva.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import com.alibaba.fastjson.JSON;

import cn.eva.dto.EvaScoreStuVo;
import cn.eva.dto.ScoreVo;
import cn.eva.entity.EvaEvaluate;
import cn.eva.entity.EvaScoreStu;

/**
 * 评分解析工具 Copyright (c) 2017 by
 * 
 * @ClassName: ScoreStuUtils.java
 * @Description: TODO
 * 
 * @author: 余勇 李彬
 * @version: V1.0
 * @Date: 2018年9月11日 上午9:56:16
 */
public class ScoreStuUtils {
    /**
     * 保留两位小数
     */
    private static String twoDecimals(double num) {
        return new java.text.DecimalFormat("#.00").format(num);
    }

    /**
     * 解析评分中间表 @Title: parseScoreStu @param @return @return Object @throws
     */
    public static List<EvaScoreStuVo> parseScoreStu(List<EvaScoreStu> evaScoreStus) {
        List<EvaScoreStuVo> result = new ArrayList<>();
        for (EvaScoreStu evaScoreStu : evaScoreStus) {
            EvaScoreStuVo evaScoreStuVo = new EvaScoreStuVo();
            BeanUtils.copyProperties(evaScoreStu, evaScoreStuVo);
            // 解析分数
            List<ScoreVo> scoreVos = JSON.parseArray(evaScoreStuVo.getScores(), ScoreVo.class);
            evaScoreStuVo.setScoreVos(scoreVos);
            result.add(evaScoreStuVo);
        }
        return result;
    }

    /**
     * 解析分数score @Title: parseScores @param @param scores @param @return @return
     * List<ScoreVo> @throws
     */
    public static List<ScoreVo> parseScores(String scores) {
        List<ScoreVo> list = new ArrayList<>();
        if (StringUtils.isNotBlank(scores)) {
            list = JSON.parseArray(scores, ScoreVo.class);
        }
        return list;
    }

    /**
     * 统计单个分数，把单个结果(分数)返回
     * 
     * @param JSON数据字符
     * @return String sum
     */
    public static String calculateScore(String dataPart, Map<Integer, EvaEvaluate> map) {
        List<ScoreVo> reList = ScoreStuUtils.parseScores(dataPart);
        if (dataPart == null) {
            return "";
        } else {
            double sum = 0;
            for (ScoreVo sVo : reList) {
                double xiaofen = 0;
                String data = sVo.getValue();
                if ("4".equals(data)) {
                    xiaofen = 1.0;
                } else if ("3".equals(data)) {
                    xiaofen = ((double) 8 / 10);
                } else if ("2".equals(data)) {
                    xiaofen = ((double) 6 / 10);
                } else {
                    xiaofen = ((double) 4 / 10);
                }
                xiaofen = xiaofen * 100 * oneScore(sVo.gettId(), map);
                sum += xiaofen;
            }
            sum = (sum * 100) / 100;
            return twoDecimals(sum);
        }
    }

    /**
     * 统计多个分数，把分数算成平均返回
     * 
     * @param JOSN数据字符串
     * @return String 总分
     */
    public static String caScoreAll(List<String> list, Map<Integer, EvaEvaluate> emap) {
        double avgAll = 0;
        for (int i = 0; i < list.size(); i++) {
            avgAll += Double.parseDouble(calculateScore(list.get(i), emap));
        }
        return twoDecimals(avgAll / list.size());
    }

    /**
     * 统计该题的权重
     * 
     * @param id
     * @param map
     * @return
     */
    public static double oneScore(String id, Map<Integer, EvaEvaluate> map) {
        EvaEvaluate evaluate = map.get(Integer.parseInt(id));
        double quzhong = 1;
        if (evaluate != null) {
            if (evaluate.geteParentId() != 0) {
                quzhong = evaluate.geteIsEnd();
                quzhong = quzhong * oneScore(Integer.toString(evaluate.geteParentId()), map);
            }
            return quzhong;
        }
        return 0;
    }
}
