package cn.eva.service.admin;

import java.util.List;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaDepartment;

/**
 * 部门接口类
 * @author wzh
 *
 */
public interface SysDepartmentService {
	
	/**
	 * 添加部门
	 */
	public ResultJosn add(EvaDepartment evaDepartment);

	/**
	 * 根据 dId 删除部门
	 */
	public ResultJosn delete(List<String> dId);
	
	/**
	 * 修改部门信息
	 */
	public ResultJosn update(EvaDepartment evaDepartment);
	
	/**
	 * 查询所有部门，传入分页数据
	 */
	public LayuiTableResult select(EvaDepartment evaDepartment,Integer page,Integer pageSize);
	
	/**
	 * 查询单个部门
	 */
	public ResultJosn selectBydId(String dId);
	
	
	
}
