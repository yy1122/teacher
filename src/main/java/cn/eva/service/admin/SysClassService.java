package cn.eva.service.admin;

import java.util.List;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaClass;

/**
 * @date 2018-9-11
 * @author wzh
 *  班级接口类
 */
public interface SysClassService {
	
	/**
	 * 添加班级
	 */
	public ResultJosn add(EvaClass evaClass);
	
	/**
	 * 根据 classId 删除班级
	 */
	public ResultJosn delete(List<String> classId);
	
	/**
	 * 修改班级信息
	 */
	public ResultJosn update(EvaClass evaClass);
	
	/**
	 * 查询所有班级，传入分页数据
	 * @param page,pageSize
	 */
	public LayuiTableResult select(EvaClass evaClass,Integer page,Integer pageSize);
	
	/**
	 * 根据classId查询单个班级
	 */
	public ResultJosn selectByclassId(String classId);

}
