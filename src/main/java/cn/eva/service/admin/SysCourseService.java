package cn.eva.service.admin;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaCourse;
import java.util.List;

/**
 * @author lishaoxing 2018.9.7
 * 课程表
 */
public interface SysCourseService {

    /**
     * 添加课程
     * @param  course
     * @return
     */
    public ResultJosn add(EvaCourse  course);

    /**
     * 根据id删除课程
     * @param cId
     * @return
     */
    public ResultJosn delete(List<String> cId);

    /**
     * 修改课程信息
     * @param course
     * @return
     */
    public ResultJosn update(EvaCourse course);

    /**
     * 查询所有课程 传入分页数据
     * @param evaCourse
     * @param page
     * @param pageSize
     * @return
     */
    public LayuiTableResult select(EvaCourse evaCourse, Integer page, Integer pageSize);

    /**
     * 根据id查询单个课程
     * @param cId
     * @return
     */
    ResultJosn selectByid(String cId);
}
