/*
*
* EvaWhatTimeMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-20
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaWhatTime;
import cn.eva.example.EvaWhatTimeExample;

@Mapper
public interface EvaWhatTimeMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaWhatTimeExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaWhatTimeExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String wtId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaWhatTime record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaWhatTime record);

    /**
     *
     * @mbg.generated
     */
    List<EvaWhatTime> selectByExample(EvaWhatTimeExample example);

    /**
     *
     * @mbg.generated
     */
    EvaWhatTime selectByPrimaryKey(String wtId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaWhatTime record, @Param("example") EvaWhatTimeExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaWhatTime record, @Param("example") EvaWhatTimeExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaWhatTime record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaWhatTime record);
}