package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.eva.entity.JobAndTrigger;

@Mapper
public interface JobAndTriggerMapper {

    public List<JobAndTrigger> getJobAndTriggerDetails();
}
