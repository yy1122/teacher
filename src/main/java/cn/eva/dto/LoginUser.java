package cn.eva.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.eva.entity.SysMenu;
import cn.eva.entity.SysRole;

/**
 * 登录用户类
 * Copyright (c) 2017 by 
 * @ClassName: LoginUser.java
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月6日 下午2:24:06
 */
public class LoginUser implements Serializable, UserDetails {

    /** 
    * @Fields serialVersionUID : TODO 
    */
    private static final long serialVersionUID = 1L;

    private String uuid;

    private String username;

    @JsonIgnore
    private String password;

    private Boolean enabled;

    private String type;

    private Object user;

    //角色
    private List<SysRole> roles;
    //权限（可操作的菜单）
    private List<SysMenu> menus;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<SysRole> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRole> roles) {
        this.roles = roles;
    }

    public List<SysMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<SysMenu> menus) {
        this.menus = menus;
    }

    public Object getUser() {
        return user;
    }

    public void setUser(Object user) {
        this.user = user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> collection = new HashSet<>();
        // 设置角色
        if (!CollectionUtils.isEmpty(roles)) {
            for (SysRole r : roles) {
                if (r.getrName().startsWith("ROLE_")) {
                    collection.add(new SimpleGrantedAuthority(r.getrName()));
                } else {
                    collection.add(new SimpleGrantedAuthority("ROLE_" + r.getrName()));
                }
            }
        }
        // 设置权限标识
        if (!CollectionUtils.isEmpty(menus)) {
            for (SysMenu m : menus) {
                if (StringUtils.isNotBlank(m.getPermission())) {
                    collection.add(new SimpleGrantedAuthority(m.getPermission()));
                }
            }
        }
        return collection;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return getEnabled();
    }

}
