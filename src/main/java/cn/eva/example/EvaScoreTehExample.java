/*
*
* EvaScoreTehExample.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-15
*/
package cn.eva.example;

import java.util.ArrayList;
import java.util.List;

public class EvaScoreTehExample {
    /**
     * eva_score_teh
     */
    protected String orderByClause;

    /**
     * eva_score_teh
     */
    protected boolean distinct;

    /**
     * eva_score_teh
     */
    protected List<Criteria> oredCriteria;

    /**
     *
     * @mbg.generated
     */
    public EvaScoreTehExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     *
     * @mbg.generated
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     *
     * @mbg.generated
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     *
     * @mbg.generated
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     *
     * @mbg.generated
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     *
     * @mbg.generated
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     *
     * @mbg.generated
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     *
     * @mbg.generated
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     *
     * @mbg.generated
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * eva_score_teh null
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andStIdIsNull() {
            addCriterion("st_id is null");
            return (Criteria) this;
        }

        public Criteria andStIdIsNotNull() {
            addCriterion("st_id is not null");
            return (Criteria) this;
        }

        public Criteria andStIdEqualTo(String value) {
            addCriterion("st_id =", value, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdNotEqualTo(String value) {
            addCriterion("st_id <>", value, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdGreaterThan(String value) {
            addCriterion("st_id >", value, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdGreaterThanOrEqualTo(String value) {
            addCriterion("st_id >=", value, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdLessThan(String value) {
            addCriterion("st_id <", value, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdLessThanOrEqualTo(String value) {
            addCriterion("st_id <=", value, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdLike(String value) {
            addCriterion("st_id like", value, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdNotLike(String value) {
            addCriterion("st_id not like", value, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdIn(List<String> values) {
            addCriterion("st_id in", values, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdNotIn(List<String> values) {
            addCriterion("st_id not in", values, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdBetween(String value1, String value2) {
            addCriterion("st_id between", value1, value2, "stId");
            return (Criteria) this;
        }

        public Criteria andStIdNotBetween(String value1, String value2) {
            addCriterion("st_id not between", value1, value2, "stId");
            return (Criteria) this;
        }

        public Criteria andTIdOneIsNull() {
            addCriterion("t_id_one is null");
            return (Criteria) this;
        }

        public Criteria andTIdOneIsNotNull() {
            addCriterion("t_id_one is not null");
            return (Criteria) this;
        }

        public Criteria andTIdOneEqualTo(String value) {
            addCriterion("t_id_one =", value, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneNotEqualTo(String value) {
            addCriterion("t_id_one <>", value, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneGreaterThan(String value) {
            addCriterion("t_id_one >", value, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneGreaterThanOrEqualTo(String value) {
            addCriterion("t_id_one >=", value, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneLessThan(String value) {
            addCriterion("t_id_one <", value, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneLessThanOrEqualTo(String value) {
            addCriterion("t_id_one <=", value, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneLike(String value) {
            addCriterion("t_id_one like", value, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneNotLike(String value) {
            addCriterion("t_id_one not like", value, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneIn(List<String> values) {
            addCriterion("t_id_one in", values, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneNotIn(List<String> values) {
            addCriterion("t_id_one not in", values, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneBetween(String value1, String value2) {
            addCriterion("t_id_one between", value1, value2, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdOneNotBetween(String value1, String value2) {
            addCriterion("t_id_one not between", value1, value2, "tIdOne");
            return (Criteria) this;
        }

        public Criteria andTIdTwoIsNull() {
            addCriterion("t_id_two is null");
            return (Criteria) this;
        }

        public Criteria andTIdTwoIsNotNull() {
            addCriterion("t_id_two is not null");
            return (Criteria) this;
        }

        public Criteria andTIdTwoEqualTo(String value) {
            addCriterion("t_id_two =", value, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoNotEqualTo(String value) {
            addCriterion("t_id_two <>", value, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoGreaterThan(String value) {
            addCriterion("t_id_two >", value, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoGreaterThanOrEqualTo(String value) {
            addCriterion("t_id_two >=", value, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoLessThan(String value) {
            addCriterion("t_id_two <", value, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoLessThanOrEqualTo(String value) {
            addCriterion("t_id_two <=", value, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoLike(String value) {
            addCriterion("t_id_two like", value, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoNotLike(String value) {
            addCriterion("t_id_two not like", value, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoIn(List<String> values) {
            addCriterion("t_id_two in", values, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoNotIn(List<String> values) {
            addCriterion("t_id_two not in", values, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoBetween(String value1, String value2) {
            addCriterion("t_id_two between", value1, value2, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andTIdTwoNotBetween(String value1, String value2) {
            addCriterion("t_id_two not between", value1, value2, "tIdTwo");
            return (Criteria) this;
        }

        public Criteria andCIdIsNull() {
            addCriterion("c_id is null");
            return (Criteria) this;
        }

        public Criteria andCIdIsNotNull() {
            addCriterion("c_id is not null");
            return (Criteria) this;
        }

        public Criteria andCIdEqualTo(String value) {
            addCriterion("c_id =", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdNotEqualTo(String value) {
            addCriterion("c_id <>", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdGreaterThan(String value) {
            addCriterion("c_id >", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdGreaterThanOrEqualTo(String value) {
            addCriterion("c_id >=", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdLessThan(String value) {
            addCriterion("c_id <", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdLessThanOrEqualTo(String value) {
            addCriterion("c_id <=", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdLike(String value) {
            addCriterion("c_id like", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdNotLike(String value) {
            addCriterion("c_id not like", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdIn(List<String> values) {
            addCriterion("c_id in", values, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdNotIn(List<String> values) {
            addCriterion("c_id not in", values, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdBetween(String value1, String value2) {
            addCriterion("c_id between", value1, value2, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdNotBetween(String value1, String value2) {
            addCriterion("c_id not between", value1, value2, "cId");
            return (Criteria) this;
        }

        public Criteria andScoresTIsNull() {
            addCriterion("scores_t is null");
            return (Criteria) this;
        }

        public Criteria andScoresTIsNotNull() {
            addCriterion("scores_t is not null");
            return (Criteria) this;
        }

        public Criteria andScoresTEqualTo(String value) {
            addCriterion("scores_t =", value, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTNotEqualTo(String value) {
            addCriterion("scores_t <>", value, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTGreaterThan(String value) {
            addCriterion("scores_t >", value, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTGreaterThanOrEqualTo(String value) {
            addCriterion("scores_t >=", value, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTLessThan(String value) {
            addCriterion("scores_t <", value, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTLessThanOrEqualTo(String value) {
            addCriterion("scores_t <=", value, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTLike(String value) {
            addCriterion("scores_t like", value, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTNotLike(String value) {
            addCriterion("scores_t not like", value, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTIn(List<String> values) {
            addCriterion("scores_t in", values, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTNotIn(List<String> values) {
            addCriterion("scores_t not in", values, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTBetween(String value1, String value2) {
            addCriterion("scores_t between", value1, value2, "scoresT");
            return (Criteria) this;
        }

        public Criteria andScoresTNotBetween(String value1, String value2) {
            addCriterion("scores_t not between", value1, value2, "scoresT");
            return (Criteria) this;
        }
    }

    /**
     *  * eva_score_teh
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * eva_score_teh null
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}