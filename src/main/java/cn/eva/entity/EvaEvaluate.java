/*
*
* EvaEvaluate.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-29
*/
package cn.eva.entity;

public class EvaEvaluate {
    /**
     * 自增id标识
     */
    private Integer eId;

    /**
     * 评教标题
     */
    private String eTitle;

    /**
     * 评教父id
     */
    private Integer eParentId;

    /**
     * 权重
     */
    private Double eIsEnd;

    /**
     * 自增id标识
     * @return e_id 自增id标识
     */
    public Integer geteId() {
        return eId;
    }

    /**
     * 自增id标识
     * @param eId 自增id标识
     */
    public void seteId(Integer eId) {
        this.eId = eId;
    }

    /**
     * 评教标题
     * @return e_title 评教标题
     */
    public String geteTitle() {
        return eTitle;
    }

    /**
     * 评教标题
     * @param eTitle 评教标题
     */
    public void seteTitle(String eTitle) {
        this.eTitle = eTitle == null ? null : eTitle.trim();
    }

    /**
     * 评教父id
     * @return e_parent_id 评教父id
     */
    public Integer geteParentId() {
        return eParentId;
    }

    /**
     * 评教父id
     * @param eParentId 评教父id
     */
    public void seteParentId(Integer eParentId) {
        this.eParentId = eParentId;
    }

    /**
     * 权重
     * @return e_is_end 权重
     */
    public Double geteIsEnd() {
        return eIsEnd;
    }

    /**
     * 权重
     * @param eIsEnd 权重
     */
    public void seteIsEnd(Double eIsEnd) {
        this.eIsEnd = eIsEnd;
    }
}