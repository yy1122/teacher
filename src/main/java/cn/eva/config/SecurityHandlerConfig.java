package cn.eva.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import cn.eva.common.ResultJosn;
import cn.eva.dto.LoginUser;
import cn.eva.utils.ResponseUtil;

/**
 * spring security处理器
 * Copyright (c) 2017 by 
 * @ClassName: SecurityHandlerConfig.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月7日 上午10:06:36
 */
@Configuration
public class SecurityHandlerConfig {

    /**
     * 登陆成功，返回Token
     * 
     * @return
     */
    @Bean
    public AuthenticationSuccessHandler loginSuccessHandler() {
        return new AuthenticationSuccessHandler() {

            @Override
            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                    Authentication authentication) throws IOException, ServletException {
                LoginUser loginUser = (LoginUser) authentication.getPrincipal();
                HttpSession session = request.getSession();
                session.setAttribute("loginUser", loginUser);
                ResultJosn info = new ResultJosn(HttpStatus.OK.value(), "登录成功", "登录成功");
                ResponseUtil.responseJson(response, HttpStatus.OK.value(), info);
            }
        };
    }

    /**
     * 登陆失败
     * 
     * @return
     */
    @Bean
    public AuthenticationFailureHandler loginFailureHandler() {
        return new AuthenticationFailureHandler() {

            @Override
            public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                    AuthenticationException exception) throws IOException, ServletException {
                String msg = null;
                if (exception instanceof BadCredentialsException) {
                    msg = "密码错误";
                } else {
                    msg = exception.getMessage();
                }
                ResultJosn info = new ResultJosn(HttpStatus.BAD_REQUEST.value(), msg, msg);
                ResponseUtil.responseJson(response, HttpStatus.BAD_REQUEST.value(), info);
            }
        };

    }

    /**
     * 未登录，返回401
     * 
     * @return
     */
    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new AuthenticationEntryPoint() {

            @Override
            public void commence(HttpServletRequest request, HttpServletResponse response,
                    AuthenticationException authException) throws IOException, ServletException {
                ResultJosn info = new ResultJosn(HttpStatus.UNAUTHORIZED.value(), "请先登录", "请先登录");
                ResponseUtil.responseJson(response, HttpStatus.UNAUTHORIZED.value(), info);
            }
        };
    }

    /**
     * 退出处理
     * 
     * @return
     */
    @Bean
    public LogoutSuccessHandler logoutSussHandler() {
        return new LogoutSuccessHandler() {

            @Override
            public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
                    Authentication authentication) throws IOException, ServletException {
                ResultJosn info = new ResultJosn(HttpStatus.OK.value(), "退出成功", "退出成功");
                //清除session
                HttpSession session = request.getSession();
                session.setAttribute("loginUser", null);
                ResponseUtil.responseJson(response, HttpStatus.OK.value(), info);
            }
        };

    }

}
