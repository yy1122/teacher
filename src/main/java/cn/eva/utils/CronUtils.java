package cn.eva.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.CronExpression;

/**
 * 
 * @ClassName:  CronUtils   
 * @Description: cron工具
 * @author: yuyong 
 * @date:   2018年9月19日 上午8:39:52   
 *     
 * @Copyright: 2018 www.xxx.com Inc. All rights reserved. 
 * @note: 注意：本内容仅限于xxx公司内部传阅，禁止外泄以及用于其他的商业目
 */
public class CronUtils {

    /*** 
     *  
     * @param date 
     * @param dateFormat : e.g:yyyy-MM-dd HH:mm:ss 
     * @return 
     */
    public static String formatDateByPattern(Date date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String formatTimeStr = null;
        if (date != null) {
            formatTimeStr = sdf.format(date);
        }
        return formatTimeStr;
    }

    /**
     * 
     * @Title: getCron   
     * @Description: 日期转为cron表达式  
     * @param: @param date
     * @param: @return      
     * @return: String      
     * @throws
     */
    public static String getCron(Date date) {
        String dateFormat = "ss mm HH dd MM ? yyyy";
        return formatDateByPattern(date, dateFormat);
    }

    /**
     * 
     * @Title: checkCron   
     * @Description: cron表达式检查
     * @param: @param cron
     * @param: @return      
     * @return: boolean true:正确，false:不正确
     * @throws
     */
    public boolean checkCron(String cron) {
        return CronExpression.isValidExpression(cron);
    }

}
