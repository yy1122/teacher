package cn.eva.job.impl;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import cn.eva.common.MiniConstant;
import cn.eva.config.quartz.SchedulerConfig;
import cn.eva.entity.EvaWhatTime;
import cn.eva.entity.SysJobBaseInfo;
import cn.eva.job.BaseJob;
import cn.eva.service.admin.SysEvaluatingManageService;
import cn.eva.service.admin.courseGrade.courseGradeService;

/**
 * 
 * @ClassName:  StatisticalFractionJob   
 * @Description: 分数统计job
 * @author: yuyong 
 * @date:   2018年9月18日 上午10:41:45   
 *     
 * @Copyright: 2018 www.xxx.com Inc. All rights reserved. 
 * @note: 注意：本内容仅限于xxx公司内部传阅，禁止外泄以及用于其他的商业目
 */
public class StatisticalFractionJob implements BaseJob {

	private static Logger _log = LoggerFactory.getLogger(StatisticalFractionJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO:真正统计业务
		_log.info("统计任务开始执行时间: " + new Date());
		//下次执行时间
		if (null == context.getNextFireTime()) {
			SysEvaluatingManageService evaluatingManageService = null;
			courseGradeService cGradeService = null;
			try {
				ApplicationContext applicationContext = (ApplicationContext) context.getScheduler().getContext()
						.get(SchedulerConfig.KEY);
				evaluatingManageService = applicationContext.getBean(SysEvaluatingManageService.class);
				cGradeService = applicationContext.getBean(courseGradeService.class);
			} catch (SchedulerException e) {
				e.printStackTrace();
			} finally {
				//更改状态
				SysJobBaseInfo jobBaseInfo = (SysJobBaseInfo) (context.getJobDetail().getJobDataMap()
						.get(MiniConstant.JOB_DATA_KEY));
				String jobGroupName = jobBaseInfo.getJobGroupName();
				EvaWhatTime evaWhatTime = new EvaWhatTime();
				evaWhatTime.setWtNumber(jobGroupName);
				evaluatingManageService.pauseAll(evaWhatTime);
				//统计
				cGradeService.totalPoint();
			}
		}
		_log.info("统计任务结束时间: " + new Date());
	}

}
