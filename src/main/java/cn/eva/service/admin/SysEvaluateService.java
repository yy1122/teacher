package cn.eva.service.admin;

import java.util.List;

import cn.eva.common.ResultJosn;
import cn.eva.dto.EvaluateZtreeVo;
import cn.eva.entity.EvaEvaluate;

public interface SysEvaluateService {

    /**
     * 树形展示所有的评价标准
     * @Title: listAllZtree
     * @param @return
     * @return List<ZtreeVo>
     * @throws
     */
    List<EvaluateZtreeVo> listAllZtree();

    /**
     * 简单展示所有的评价标准
     * @Title: listAllZtreeSimple
     * @param @return
     * @return List<EvaEvaluate>
     * @throws
     */
    List<EvaEvaluate> listAllZtreeSimple();

    /**
     * 树形展示学生评价标准
     * @Title: listStudentZtree
     * @param @return
     * @return List<EvaluateZtreeVo>
     * @throws
     */
    List<EvaluateZtreeVo> listStudentZtree();

    /**
     * 树形展示教师评价标准
     * @Title: listTeacherZtree
     * @param @return
     * @return List<EvaluateZtreeVo>
     * @throws
     */
    List<EvaluateZtreeVo> listTeacherZtree();

    /**
     * 树形展示自我评价标准
     * @Title: listTeacherZtree
     * @param @return
     * @return List<EvaluateZtreeVo>
     * @throws
     */
    List<EvaluateZtreeVo> listMyselfevaZtree();

    /**
     * 增加评价标准
     * @Title: add
     * @param @param evaEvaluate
     * @param @return
     * @return ResultJosn
     * @throws
     */
    public ResultJosn add(EvaEvaluate evaEvaluate);

    /**
     * 根据eId删除评价标准
     * @Title: delete
     * @param @param eId
     * @param @return
     * @return ResultJosn
     * @throws
     */
    public ResultJosn delete(Integer eId);

    /**
     * 根据eId修改评价标准
     * @Title: update
     * @param @param evaEvaluate
     * @param @return
     * @return ResultJosn
     * @throws
     */
    public ResultJosn update(EvaEvaluate evaEvaluate);

}
