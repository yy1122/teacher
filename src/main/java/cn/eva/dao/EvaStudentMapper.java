/*
*
* EvaStudentMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaStudent;
import cn.eva.example.EvaStudentExample;

@Mapper
public interface EvaStudentMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaStudentExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaStudentExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String sId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaStudent record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaStudent record);

    /**
     *
     * @mbg.generated
     */
    List<EvaStudent> selectByExample(EvaStudentExample example);

    /**
     *
     * @mbg.generated
     */
    EvaStudent selectByPrimaryKey(String sId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaStudent record, @Param("example") EvaStudentExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaStudent record, @Param("example") EvaStudentExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaStudent record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaStudent record);

    /**
     * 批量插入
     * @Title: insertBatch
     * @param @param list
     * @param @return
     * @return int
     * @throws
     */
    int insertBatch(List<EvaStudent> list);

    /**
     * 批量更新
     * @Title: updateBatch
     * @param @param updateLists
     * @param @return
     * @return int
     * @throws
     */
    int updateBatch(List<EvaStudent> list);
}