package cn.eva.utils;

import java.util.UUID;

/**
 * UUID工具
 * Copyright (c) 2017 by 
 * @ClassName: UUIDUtils.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月6日 下午10:08:09
 */
public class UUIDUtils {

    /**
     * 获取UUID，已经去除-
     * @Title: getUUID
     * @param @return
     * @return String
     * @throws
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}
