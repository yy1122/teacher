package cn.eva.service.Impl.admin.courseGade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dao.SyscourseGradeMapper;
import cn.eva.entity.SyscourseGrade;
import cn.eva.service.admin.courseGrade.rankingListService;

@Service
public class rankingListServiceImpl implements rankingListService {

	@Autowired
	private SyscourseGradeMapper syscourseGradeMapper;

	/**
	 * 查询评分排名
	 * 
	 */
	@Override
	public ResultJosn selectInfo(Integer page, Integer pageSize, SyscourseGrade syscourseGrade) {
		// TODO Auto-generated method stub

		SyscourseGrade scg = new SyscourseGrade();

		// 判断输入查询数据是否为空或者""
		if (syscourseGrade.gettNo() != null) {
			scg.settNo("%" + syscourseGrade.gettNo() + "%");
		}
		if (syscourseGrade.gettName() != null) {
			scg.settName("%" + syscourseGrade.gettName() + "%");

		}
		if (syscourseGrade.getcName() != null) {
			scg.setcName("%" + syscourseGrade.getcName() + "%");
		}

		List<SyscourseGrade> list = syscourseGradeMapper.selectrankingList(scg);
		// 开启分页查找

		PageHelper.startPage(page, pageSize);
		PageInfo<SyscourseGrade> pageInfo = new PageInfo<>(list, pageSize);
		// 设置返回前台的格式和数据，如code=0/success，message，查询的总数total，和数据集合data
		LayuiTableResult resultJosn = new LayuiTableResult();
		resultJosn.setCode(HttpStatus.OK.value());
		resultJosn.setMessage(HttpStatus.OK.getReasonPhrase());
		resultJosn.setCount(pageInfo.getTotal());
		resultJosn.setData(pageInfo.getList());
		return resultJosn;
	}

}
