package cn.eva.service.Impl.admin.courseGade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.eva.dao.EvaEvaluateMapper;
import cn.eva.dao.EvaScoreMapper;
import cn.eva.dao.SyscourseGradeMapper;
import cn.eva.entity.EvaEvaluate;
import cn.eva.entity.EvaScore;
import cn.eva.entity.EvaScoreStu;
import cn.eva.entity.EvaScoreTeh;
import cn.eva.example.EvaScoreExample;
import cn.eva.example.EvaScoreExample.Criteria;
import cn.eva.service.EvaEvaluateMapService;
import cn.eva.service.admin.courseGrade.courseGradeService;
import cn.eva.utils.ScoreStuUtils;
import cn.eva.utils.UUIDUtils;

@Service
public class courseGradeServiceImpl implements courseGradeService{
	
	@Autowired
	private SyscourseGradeMapper syscourseGradeMapper;


	@Autowired
	private courseGradeService courseGradeService;

	@Autowired
	private EvaScoreMapper evaScoreMapper;
	
	@Autowired
	private  EvaEvaluateMapService evaEvaluateMapService;
	
	@Autowired
	private EvaEvaluateMapper evaEvaluateMapper;
	

	@Override
	public List<EvaScoreStu> selectScoresStutId() {
		// TODO Auto-generated method stub
		List<EvaScoreStu> evaScoreStutIdList = syscourseGradeMapper.selecttIdByesc();
		return evaScoreStutIdList;
	}

	@Override
	public List<EvaScoreStu> selectstuInfo(EvaScoreStu stu) {
		
		List<EvaScoreStu> evaScoreStuList = syscourseGradeMapper.selectstuScoresInfo(stu);
		// TODO Auto-generated method stub
		return evaScoreStuList;
	}

	@Override
	public List<EvaScoreTeh> selectScoresTeatIdtwo() {
		// TODO Auto-generated method stub
		List<EvaScoreTeh> evaScoreTeatIdtwoList = syscourseGradeMapper.selecttIdtwoByest();
		return evaScoreTeatIdtwoList;
	}

	@Override
	public List<EvaScoreTeh> selectteaInfo(EvaScoreTeh tea) {
		// TODO Auto-generated method stub
		List<EvaScoreTeh> evaScoreTeaList = syscourseGradeMapper.selecttScoresInfo(tea);
		
		return evaScoreTeaList;
	}

	@Override
	public boolean totalPoint() {
		boolean b = false;
		
		
		List<EvaScore> score = new ArrayList<EvaScore>();
		/**
		 * 用来记录总分
		 */
		float i = 0;
		List<EvaScoreStu> stutIdList = courseGradeService.selectScoresStutId();

		Map<String, Float> map = new HashMap<String, Float>();
		List<String> stuList = new ArrayList<String>();
		
		Map<Integer,EvaEvaluate> emap = evaEvaluateMapService.selectAll();
		
		
		//获取学生权重
		double stuWeight = evaEvaluateMapper.selectByPrimaryKey(1).geteIsEnd();
		
		//获取教研室权重
		double tehWeight = evaEvaluateMapper.selectByPrimaryKey(2).geteIsEnd();
		
		//系部方面评教
		//double partWeight = evaEvaluateMapper.selectByPrimaryKey(3).geteIsEnd();
		
		
		//同行评教
		//double peerWeight = evaEvaluateMapper.selectByPrimaryKey(4).geteIsEnd();
		
		//教师自评
		double selfEvaluation = evaEvaluateMapper.selectByPrimaryKey(5).geteIsEnd();
		
		/**
		 * 计算学生的平均分数
		 * 将教师id和课程id装入map中作为key，分数作为value
		 */
		for (EvaScoreStu stu : stutIdList) {
			// stuInfoList.addAll(courseGradeService.selectstuInfo(stu));
			List<EvaScoreStu> stuInfoList = courseGradeService.selectstuInfo(stu);

			EvaScoreStu st = new EvaScoreStu();
			for (EvaScoreStu s : stuInfoList) {
				stuList.add(s.getScores());
				st.setcId(s.getcId());
				st.settId(s.gettId());
			}

			try {
				float stuScore = Float.parseFloat(ScoreStuUtils.caScoreAll(stuList,emap));
				map.put(st.gettId() + st.getcId(), stuScore);
			} catch (Exception e) {
				System.err.println("学生分数统计出错");
			}

		}

		List<EvaScoreTeh> teatIdtwoList = courseGradeService.selectScoresTeatIdtwo();
		List<EvaScoreTeh> teaInfoList = null;
		
		//用来存放分数
		List<String> teaList = new ArrayList<String>();
		

		/**
		 * 
		 */
		for (EvaScoreTeh tea : teatIdtwoList) {
			teaInfoList = courseGradeService.selectteaInfo(tea);

			EvaScoreTeh te = new EvaScoreTeh();
			//自评
			EvaScoreTeh teh = new EvaScoreTeh();
			for (EvaScoreTeh t : teaInfoList) {
				if(!t.gettIdTwo().equals(t.gettIdOne())){
				teaList.add(t.getScoresT());
				te.setcId(t.getcId());
				te.settIdTwo(t.gettIdTwo());
				te.settIdOne(t.gettIdOne());
				}else{
					teh.setScoresT(t.getScoresT());
					teh.setcId(t.getcId());
					teh.settIdTwo(t.gettIdTwo());
					teh.settIdOne(t.gettIdOne());
				}
			}
			float teaScore = 0;
			float tehScore = 0;
			try {
				teaScore = Float.parseFloat(ScoreStuUtils.caScoreAll(teaList,emap));//将String转为float
				if(teh.getScoresT() != null){
					tehScore = Float.parseFloat(ScoreStuUtils.calculateScore(teh.getScoresT(),emap));
				}
	
				
			} catch (Exception e) {
				System.err.println("教师分数统计出错");
			}
			// 遍历map中的键
			for (String key : map.keySet()) {

				if (key.equals(te.gettIdTwo() + te.getcId())) {//判断是否是同一个老师和同一门课程
					// System.out.println("Key = " + key);
					EvaScore scores = new EvaScore();
					
					//计算总分
					i = (float) ((map.get(key)*stuWeight)  //获取学生权重
							+ (teaScore*tehWeight)  //获取教研室权重
							+(tehScore*selfEvaluation)); //教师自评
					
					
					scores.setcId(te.getcId());
					scores.settId(te.gettIdTwo());
					scores.setScoreId(UUIDUtils.getUUID());
					scores.setScoreNum(i);
					score.add(scores);
				}
			}
			// 遍历map中的值
			/*
			 * for (Float value : map.values()) { System.out.println("Value = "
			 * + value); }
			 */

		}
		
		

		//判断分数是否插入
		for (EvaScore s : score) {
			//添加where条件
			EvaScoreExample example = new EvaScoreExample(); 
			Criteria criteria = example.createCriteria();
			criteria.andCIdEqualTo(s.getcId());
			criteria.andTIdEqualTo(s.gettId());
			List<EvaScore> evaScore= evaScoreMapper.selectByExample(example);
			if (evaScore.size() == 0) {
				evaScoreMapper.insert(s);
				b=true;
			}else{
				b=false;
			}
		}
		return b;

	}
	
	
}
