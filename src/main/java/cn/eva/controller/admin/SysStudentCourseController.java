package cn.eva.controller.admin;

import cn.eva.dto.SysClassCourseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaClass;
import cn.eva.entity.EvaClassCourse;
import cn.eva.entity.EvaCourse;
import cn.eva.service.admin.SysClassService;
import cn.eva.service.admin.SysCourseService;
import cn.eva.service.admin.SysStudentCourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "管理员给学生选课")
@RequestMapping("/sysStudentCourse")
@RestController
/**
 * 老师选择班级，课程
 * 李绍兴 by 2018.8.10
 */
public class SysStudentCourseController {
    @Autowired
    SysStudentCourseService sysStudentCourseService;
    @Autowired
    SysCourseService courseService;
    @Autowired
    SysClassService classService;

    @PostMapping
    @ApiOperation(value = "添加学生选课程的信息，在实现方法中，先删除所选课程，在重新勾选")
    @PreAuthorize("hasAuthority('sys:student:course:add')")
    public ResultJosn add(@RequestBody SysClassCourseVo sysClassCourseVo) {
        ResultJosn resultJosn = null;
        resultJosn = sysStudentCourseService.add(sysClassCourseVo);
        return resultJosn;
    }

   /* @GetMapping
    @ApiOperation(value = "查询所有课程列表，假分页")
    @PreAuthorize("hasAnyAuthority('sys:student:course:add')")*/
    /*
    public LayuiTableResult selectByExample(EvaCourse course, @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "9999") Integer pageSize) {

        LayuiTableResult resultJosn = courseService.select(course, page, pageSize);
        return resultJosn;
    }*/
   @GetMapping(value = "/{classId}")
   @ApiOperation(value = "根据classId查找课程cId有哪些，渲染学生班级选课的复选框")
   @PreAuthorize("hasAuthority('sys:student:course:add')")
   public ResultJosn selectByClassId(@PathVariable String classId) {
       ResultJosn resultJosn = sysStudentCourseService.selectByClassId(classId);
       return resultJosn;
   }

    @GetMapping(value = "/allClass")
    @ApiOperation(value = "查询所有班级列表,假分页")
    @PreAuthorize("hasAnyAuthority('sys:student:course:add')")
    public LayuiTableResult selectByExample(EvaClass evaClass, @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "9999") Integer pageSize) {
        /*检索删除和不删除的班级 0代表要不删除的*/
        evaClass.setFlag("0");
        LayuiTableResult resultJosn = classService.select(evaClass, page, pageSize);
        return resultJosn;
    }



    @GetMapping(value = "/selectFilter/{classId}")
    @ApiOperation(value = "选择班级，检索课程列表，不分页")
    @PreAuthorize("hasAuthority('sys:student:course:add')")
    public ResultJosn selectByExample(@PathVariable String classId) {
        ResultJosn resultJosn = sysStudentCourseService.selectFilterCourse(classId);
        return resultJosn;
    }


}
