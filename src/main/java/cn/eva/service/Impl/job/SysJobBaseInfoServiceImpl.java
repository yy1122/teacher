package cn.eva.service.Impl.job;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.enums.JobStatus;
import cn.eva.dao.SysJobBaseInfoMapper;
import cn.eva.entity.SysJobBaseInfo;
import cn.eva.entity.SysJobBaseInfoExample;
import cn.eva.entity.SysJobBaseInfoExample.Criteria;
import cn.eva.service.job.SysJobBaseInfoService;

@Service
public class SysJobBaseInfoServiceImpl implements SysJobBaseInfoService {

	@Autowired
	private SysJobBaseInfoMapper sBaseInfoMapper;

	@Override
	@Transactional
	public void addJob(SysJobBaseInfo jobBaseInfo) {
		if (this.checkJobExits(jobBaseInfo)) {
			//更新状态
			jobBaseInfo.setStatus(JobStatus.RUN.getCode());
			this.updJob(jobBaseInfo);
		} else {
			jobBaseInfo.setCreateTime(new Date());
			jobBaseInfo.setUpdateTime(new Date());
			//状态
			jobBaseInfo.setStatus(JobStatus.RUN.getCode());
			sBaseInfoMapper.insertSelective(jobBaseInfo);
		}
	}

	@Override
	@Transactional
	public void delJob(SysJobBaseInfo jobBaseInfo) {
		if (null == jobBaseInfo.getId()) {
			this.delJobByExample(jobBaseInfo);
		} else {
			sBaseInfoMapper.deleteByPrimaryKey(jobBaseInfo.getId());
		}
	}

	@Transactional
	public void delJobByExample(SysJobBaseInfo jobBaseInfo) {
		SysJobBaseInfoExample example = new SysJobBaseInfoExample();
		example.createCriteria().andJobClassNameEqualTo(jobBaseInfo.getJobClassName())
				.andJobGroupNameEqualTo(jobBaseInfo.getJobGroupName());
		sBaseInfoMapper.deleteByExample(example);
	}

	@Override
	@Transactional
	public void updJob(SysJobBaseInfo jobBaseInfo) {
		if (null == jobBaseInfo.getId()) {
			//(批次)条件模糊修改
			SysJobBaseInfoExample example = new SysJobBaseInfoExample();
			example.createCriteria().andJobClassNameEqualTo(jobBaseInfo.getJobClassName())
					.andJobGroupNameEqualTo(jobBaseInfo.getJobGroupName());
			sBaseInfoMapper.updateByExampleSelective(jobBaseInfo, example);
		} else {
			//根据主键修改
			sBaseInfoMapper.updateByPrimaryKeySelective(jobBaseInfo);
		}
	}

	@Override
	public LayuiTableResult seletByPage(SysJobBaseInfo jobBaseInfo, Integer page, Integer pageSize) {
		LayuiTableResult resultJosn = new LayuiTableResult();
		SysJobBaseInfoExample example = new SysJobBaseInfoExample();
		Criteria criteria = example.createCriteria();
		if (StringUtils.isNotBlank(jobBaseInfo.getJobName())) {
			criteria.andJobNameLike("%" + jobBaseInfo.getJobName() + "%");
		}
		PageHelper.startPage(page, pageSize);
		List<SysJobBaseInfo> list = sBaseInfoMapper.selectByExample(example);
		PageInfo<SysJobBaseInfo> pageInfo = new PageInfo<SysJobBaseInfo>(list);
		resultJosn.setCode(HttpStatus.OK.value());
		resultJosn.setMessage(HttpStatus.OK.getReasonPhrase());
		resultJosn.setCount(pageInfo.getTotal());
		resultJosn.setData(pageInfo.getList());
		return resultJosn;
	}

	@Override
	public SysJobBaseInfo selectById(Long id) {
		return sBaseInfoMapper.selectByPrimaryKey(id);
	}

	/**
	 * 
	 * @Title: checkJobExits   
	 * @Description: 检查job是否存在
	 * @param: @param jobBaseInfo
	 * @param: @return      
	 * @return: Boolean
	 * 			false:不存在
	 * 			true:存在
	 * @throws
	 */
	private Boolean checkJobExits(SysJobBaseInfo jobBaseInfo) {
		SysJobBaseInfoExample example = new SysJobBaseInfoExample();
		example.createCriteria().andJobClassNameEqualTo(jobBaseInfo.getJobClassName())
				.andJobGroupNameEqualTo(jobBaseInfo.getJobGroupName());
		List<SysJobBaseInfo> list = sBaseInfoMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(list)) {
			return false;
		}
		return true;
	}

}
