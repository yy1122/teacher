package cn.eva.common.enums;

public enum BasicHttpStatus {
    /* 成功状态码 */
    SUCCESS(0, "OK"),

    /* 参数错误：10001-19999 */
    PARAM_IS_INVALID(10001, "参数无效"),
    PARAM_IS_BLANK(10002, "参数为空"),
    PARAM_TYPE_BIND_ERROR(10003, "参数类型错误"),
    PARAM_NOT_COMPLETE(10004, "参数缺失"),
    PARAM_PRICE_ERROR(10005, "价格有误或者数量有误"),
    PARAM_DATE_ERROR(10006, "日期有误"),
    PARAM_PHONE_ERROR(10007, "手机号或者座机号有误"),

    /* 用户错误：20001-29999*/
    USER_NOT_LOGGED_IN(20001, "用户未登录"),
    USER_LOGIN_ERROR(20002, "账号不存在或密码错误"),
    USER_ACCOUNT_FORBIDDEN(20003, "账号已被禁用"),
    USER_NOT_EXIST(20004, "用户不存在"),
    USER_HAS_EXISTED(20005, "用户已存在"),

    /* 业务错误：30000-39999 */



    /* 系统错误：40001-49999 */
    SYSTEM_INNER_ERROR(40001, "系统繁忙，请稍后重试"),
    SYSTEM_SERVICE_RANGE_NOT_EXIST(40002,"服务范围不存在"),

    /* 数据错误：50001-599999 */
    RESULE_DATA_NONE(50001, "未查询到数据"),
    DATA_IS_WRONG(50002, "数据有误"),
    DATA_ALREADY_EXISTED(50003, "数据已存在"),
    DATA_STORAGE_ERROR(50004, "数据存储异常"),
    DATA_DELETE_ERROR(50006, "数据删除异常"),
    DATA_UPDATE_ERROR(50007, "数据修改异常"),
    DATA_SIGN_DELETE_ERROR(50008, "数据标记删除异常"),

    /* 接口错误：60001-69999 */
    INTERFACE_INNER_INVOKE_ERROR(60001, "内部系统接口调用异常"),
    INTERFACE_OUTTER_INVOKE_ERROR(60002, "外部系统接口调用异常"),
    INTERFACE_FORBID_VISIT(60003, "该接口禁止访问"),
    INTERFACE_ADDRESS_INVALID(60004, "接口地址无效"),
    INTERFACE_REQUEST_TIMEOUT(60005, "接口请求超时"),
    INTERFACE_EXCEED_LOAD(60006, "接口负载过高"),
    INTERFACE_ACCESS_TOKEN_ERROR(60007, "接口访问token异常"),
    INTERFACE_ACCESS_TOKEN_EXPIRE(60008, "接口访问token过期"),
    INTERFACE_ACCESS_FAILED(60009, " 接口请求失败"),


    /* 权限错误：70001-79999 */
    PERMISSION_NO_ACCESS(70001, "无访问权限");

    private Integer code;
    private String message;

    BasicHttpStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String getMessage(String name) {
        for (BasicHttpStatus item : BasicHttpStatus.values()) {
            if (item.name().equals(name)) {
                return item.message;
            }
        }
        return name;
    }

    public static Integer getCode(String name) {
        for (BasicHttpStatus item : BasicHttpStatus.values()) {
            if (item.name().equals(name)) {
                return item.code;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name();
    }

}
