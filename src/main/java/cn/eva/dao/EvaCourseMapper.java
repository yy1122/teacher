/*
*
* EvaCourseMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-07
*/
package cn.eva.dao;

import cn.eva.entity.EvaCourse;
import cn.eva.example.EvaCourseExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface EvaCourseMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String cId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaCourse record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaCourse record);

    /**
     *
     * @mbg.generated
     */
    List<EvaCourse> selectByExample(EvaCourseExample example);

    /**
     *
     * @mbg.generated
     */
    EvaCourse selectByPrimaryKey(String cId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaCourse record, @Param("example") EvaCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaCourse record, @Param("example") EvaCourseExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaCourse record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaCourse record);
}