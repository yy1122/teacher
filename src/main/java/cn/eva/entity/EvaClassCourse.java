/*
*
* EvaClassCourse.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-10
*/
package cn.eva.entity;

public class EvaClassCourse {
    /**
     * 班级课程关联表；自动增长
     */
    private Integer ccId;

    /**
     * 班级唯一标识
     */
    private String classId;

    /**
     * 课程标识
     */
    private String cId;

    /**
     * 课程名
     */
    private String cName;

    /**
     * 班级课程关联表；自动增长
     * @return cc_id 班级课程关联表；自动增长
     */
    public Integer getCcId() {
        return ccId;
    }

    /**
     * 班级课程关联表；自动增长
     * @param ccId 班级课程关联表；自动增长
     */
    public void setCcId(Integer ccId) {
        this.ccId = ccId;
    }

    /**
     * 班级唯一标识
     * @return class_id 班级唯一标识
     */
    public String getClassId() {
        return classId;
    }

    /**
     * 班级唯一标识
     * @param classId 班级唯一标识
     */
    public void setClassId(String classId) {
        this.classId = classId == null ? null : classId.trim();
    }

    /**
     * 课程标识
     * @return c_id 课程标识
     */
    public String getcId() {
        return cId;
    }

    /**
     * 课程标识
     * @param cId 课程标识
     */
    public void setcId(String cId) {
        this.cId = cId == null ? null : cId.trim();
    }

    /**
     * 课程名
     * @return c_name 课程名
     */
    public String getcName() {
        return cName;
    }

    /**
     * 课程名
     * @param cName 课程名
     */
    public void setcName(String cName) {
        this.cName = cName == null ? null : cName.trim();
    }
}