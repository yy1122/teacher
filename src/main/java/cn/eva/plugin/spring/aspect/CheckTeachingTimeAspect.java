package cn.eva.plugin.spring.aspect;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaWhatTimeMapper;
import cn.eva.entity.EvaWhatTime;
import cn.eva.example.EvaWhatTimeExample;
import cn.eva.plugin.spring.CheckTeachingTime;
import cn.eva.utils.ResponseUtil;

/**
 * 
 * @ClassName: CheckTeachingTimeAspect
 * @Description: 检查评教时间
 * @author: yuyong
 * @date: 2018年9月19日 下午12:17:40
 * 
 * @Copyright: 2018 www.xxx.com Inc. All rights reserved.
 * @note: 注意：本内容仅限于xxx公司内部传阅，禁止外泄以及用于其他的商业目
 */

@Aspect
@Component
public class CheckTeachingTimeAspect {

	@Autowired
	private EvaWhatTimeMapper evaWhatTimeMapper; // 评价时间

	@Pointcut(value = "(execution(* *.*(..)) && @annotation(cn.eva.plugin.spring.CheckTeachingTime))")
	private void pointcut() {
	}

	@Before(value = "pointcut()")
	private void process(JoinPoint joinPoint) {
		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getResponse();
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		CheckTeachingTime checkTeachingTime = method.getAnnotation(CheckTeachingTime.class);
		if (null != checkTeachingTime) {
			long time = new Date().getTime();
			EvaWhatTimeExample whatTimeExample = new EvaWhatTimeExample();
			whatTimeExample.createCriteria().andWtStateEqualTo(true);
			List<EvaWhatTime> listWT = evaWhatTimeMapper.selectByExample(whatTimeExample);
			if (CollectionUtils.isEmpty(listWT)) {
				ResultJosn info = new ResultJosn(HttpStatus.BAD_REQUEST.value(), "评教暂未开放", new ArrayList<>());
				ResponseUtil.responseJson(response, HttpStatus.OK.value(), info);
			} else {
				EvaWhatTime sTime = listWT.get(0);
				long begin = sTime.getWtBegin().getTime();
				long end = sTime.getWtEnd().getTime();
				if (time < begin) {
					ResultJosn info = new ResultJosn(HttpStatus.BAD_REQUEST.value(), "评教暂未开始", new ArrayList<>());
					ResponseUtil.responseJson(response, HttpStatus.OK.value(), info);
				}
				if (time > end) {
					ResultJosn info = new ResultJosn(HttpStatus.BAD_REQUEST.value(), "评教已经结束", new ArrayList<>());
					ResponseUtil.responseJson(response, HttpStatus.OK.value(), info);
				}
			}
		}
	}
}
