/*
*
* EvaClassMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaClass;
import cn.eva.example.EvaClassExample;

@Mapper
public interface EvaClassMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaClassExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaClassExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String classId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaClass record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaClass record);

    /**
     *
     * @mbg.generated
     */
    List<EvaClass> selectByExample(EvaClassExample example);

    /**
     *
     * @mbg.generated
     */
    EvaClass selectByPrimaryKey(String classId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaClass record, @Param("example") EvaClassExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaClass record, @Param("example") EvaClassExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaClass record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaClass record);
}