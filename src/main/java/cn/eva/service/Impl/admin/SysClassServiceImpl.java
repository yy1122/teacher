package cn.eva.service.Impl.admin;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaClassMapper;
import cn.eva.entity.EvaClass;
import cn.eva.example.EvaClassExample;
import cn.eva.example.EvaClassExample.Criteria;
import cn.eva.service.admin.SysClassService;

/**
 * 班级接口实现类
 * @author wzh
 *
 */
@Service
public class SysClassServiceImpl implements SysClassService {

    @Autowired
    private EvaClassMapper classMapper;

    /**
     * 新增班级
     */
    @Override
    @Transactional
    public ResultJosn add(EvaClass evaClass) {
        if (null != this.checkClassNum(evaClass.getClassNum())) {
            throw new IllegalArgumentException(evaClass.getClassNum() + "已存在！");
        }
        evaClass.setClassId(evaClass.getClassNum());
        evaClass.setCreateTime(new Date());
        evaClass.setUpdateTime(new Date());
        classMapper.insertSelective(evaClass);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), null);
    }

    /**
     * 单个、批量删除
     */
    @Override
    @Transactional
    public ResultJosn delete(List<String> classId) {
        EvaClassExample classExample = new EvaClassExample();
        classExample.createCriteria().andClassIdIn(classId);
        classMapper.deleteByExample(classExample);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "删除成功");
    }

    /**
     * 修改班级信息
     */
    @Override
    @Transactional
    public ResultJosn update(EvaClass evaClass) {
        if (StringUtils.isBlank(evaClass.getClassId())) {
            throw new IllegalArgumentException("classId 错误！");
        }
        EvaClass check = this.checkClassNum(evaClass.getClassNum());
        if (null != check && !(check.getClassId()).equals(evaClass.getClassId())) {
            throw new IllegalArgumentException("班级编号已存在！");
        }
        classMapper.updateByPrimaryKeySelective(evaClass);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "修改成功");
    }

    /**
     * 根据 班级编号、编辑名称 进行模糊查询
     */
    @Override
    public LayuiTableResult select(EvaClass evaClass, Integer page, Integer pageSize) {
        LayuiTableResult resultJosn = new LayuiTableResult();
        // 构造条件
        EvaClassExample example = new EvaClassExample();
        example.setOrderByClause("create_time DESC");
        Criteria criteria = example.createCriteria();
        // 根据 输入的班级编号 classNum 进行模糊查找
        if (StringUtils.isNotBlank(evaClass.getClassNum())) {
            criteria.andClassNumLike("%" + evaClass.getClassNum() + "%");
        }
        // 根据输入的 班级名称 className 进行模糊查找
        if (StringUtils.isNotBlank(evaClass.getClassName())) {
            criteria.andClassNameLike("%" + evaClass.getClassName() + "%");
        }
        if ("0".equals(evaClass.getFlag())) {
            criteria.andFlagEqualTo("0");
        }
        // 开启分页查找
        PageHelper.startPage(page, pageSize);
        List<EvaClass> list = classMapper.selectByExample(example);
        PageInfo<EvaClass> pageInfo = new PageInfo<>(list, pageSize);
        resultJosn.setCode(HttpStatus.OK.value());
        resultJosn.setMessage(HttpStatus.OK.getReasonPhrase());
        resultJosn.setCount(pageInfo.getTotal());
        resultJosn.setData(pageInfo.getList());
        return resultJosn;
    }

    @Override
    public ResultJosn selectByclassId(String classId) {
        if (StringUtils.isBlank(classId)) {
            throw new IllegalArgumentException("classId 错误！");
        }
        EvaClass result = classMapper.selectByPrimaryKey(classId);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), result);
    }

    /**
     * 根据 班级编号查找是否重复
     * @param classNum
     */
    public EvaClass checkClassNum(String classNum) {
        EvaClassExample example = new EvaClassExample();
        example.createCriteria().andClassNumEqualTo(classNum);
        List<EvaClass> list = classMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return (EvaClass) list.get(0);
    }

}
