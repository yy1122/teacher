/*
*
* EvaDepartment.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-12
*/
package cn.eva.entity;

public class EvaDepartment {
    /**
     * 部门编号
     */
    private String dId;

    /**
     * 部门名称
     */
    private String dName;

    /**
     * 部门说明
     */
    private String dDescription;

    /**
     * 部门编号
     * @return d_id 部门编号
     */
    public String getdId() {
        return dId;
    }

    /**
     * 部门编号
     * @param dId 部门编号
     */
    public void setdId(String dId) {
        this.dId = dId == null ? null : dId.trim();
    }

    /**
     * 部门名称
     * @return d_name 部门名称
     */
    public String getdName() {
        return dName;
    }

    /**
     * 部门名称
     * @param dName 部门名称
     */
    public void setdName(String dName) {
        this.dName = dName == null ? null : dName.trim();
    }

    /**
     * 部门说明
     * @return d_description 部门说明
     */
    public String getdDescription() {
        return dDescription;
    }

    /**
     * 部门说明
     * @param dDescription 部门说明
     */
    public void setdDescription(String dDescription) {
        this.dDescription = dDescription == null ? null : dDescription.trim();
    }
}