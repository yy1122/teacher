/*
*
* SysRoleMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.dto.SysRoleSelect;
import cn.eva.entity.SysRole;
import cn.eva.example.SysRoleExample;

@Mapper
public interface SysRoleMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(SysRoleExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(SysRoleExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Long rId);

    /**
     *
     * @mbg.generated
     */
    int insert(SysRole record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(SysRole record);

    /**
     *
     * @mbg.generated
     */
    List<SysRole> selectByExample(SysRoleExample example);

    /**
     *
     * @mbg.generated
     */
    SysRole selectByPrimaryKey(Long rId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") SysRole record, @Param("example") SysRoleExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") SysRole record, @Param("example") SysRoleExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(SysRole record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(SysRole record);

    SysRoleSelect selectByid(Long id);
}