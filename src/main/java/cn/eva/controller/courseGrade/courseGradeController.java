package cn.eva.controller.courseGrade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.service.admin.courseGrade.courseGradeService;

/**
 * 
 * @author ANOTHER ONE 管理评分项目
 */
@RestController
@RequestMapping("/courseGrade")
public class courseGradeController {

	@Autowired
	private courseGradeService courseGradeService;

	@GetMapping("/totalPoints")
	@ResponseBody
	public boolean totalPoints() {
		return courseGradeService.totalPoint();
	}

}
