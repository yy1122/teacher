package cn.eva.dto;

import cn.eva.entity.EvaCourse;

import java.util.List;

public class SysCourseAdd extends EvaCourse {
    private static final long serialVersionUID = 3692121066628241637L;

    private List<Long> permissionIds;

    public List<Long> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Long> permissionIds) {
        this.permissionIds = permissionIds;
    }
}
