/*
*
* SysRoleMenuMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.SysRoleMenuKey;
import cn.eva.example.SysRoleMenuExample;

@Mapper
public interface SysRoleMenuMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(SysRoleMenuExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(SysRoleMenuExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(SysRoleMenuKey key);

    /**
     *
     * @mbg.generated
     */
    int insert(SysRoleMenuKey record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(SysRoleMenuKey record);

    /**
     *
     * @mbg.generated
     */
    List<SysRoleMenuKey> selectByExample(SysRoleMenuExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") SysRoleMenuKey record, @Param("example") SysRoleMenuExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") SysRoleMenuKey record, @Param("example") SysRoleMenuExample example);

    int insertBatch(List<SysRoleMenuKey> list);

}