package cn.eva.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.ResultJosn;
import cn.eva.entity.SysMenu;
import cn.eva.service.admin.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "菜单管理")
@RestController
@RequestMapping("/menus")
public class SysMenuController {

    @Autowired
    private SysMenuService menuService;

    @PostMapping
    @ApiOperation(value = "添加菜单")
    @PreAuthorize("hasAuthority('sys:menu:add')")
    public ResultJosn add(@RequestBody SysMenu menu) {
        ResultJosn resultJosn = menuService.add(menu);
        return resultJosn;
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "根据id删除菜单")
    @PreAuthorize("hasAuthority('sys:menu:del')")
    public ResultJosn delete(@PathVariable Long id) {
        ResultJosn resultJosn = menuService.delete(id);
        return resultJosn;
    }

    @PutMapping
    @ApiOperation(value = "更新菜单")
    @PreAuthorize("hasAuthority('sys:menu:update')")
    public ResultJosn update(@RequestBody SysMenu menu) {
        ResultJosn resultJosn = menuService.update(menu);
        return resultJosn;
    }

    @GetMapping
    @ApiOperation(value = "查询菜单")
    @PreAuthorize("hasAnyAuthority('sys:menu:query','sys:menu:update','sys:role:add','sys:role:update')")
    public ResultJosn select(SysMenu menu) {
        ResultJosn resultJosn = menuService.select(menu);
        return resultJosn;
    }

}
