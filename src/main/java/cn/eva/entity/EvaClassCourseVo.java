package cn.eva.entity;

import java.util.Date;

/**
 * @author Li-Ob
 *
 */
public class EvaClassCourseVo extends EvaTeacherCourseVo {
    /**
     * 班级唯一标识
     */
    private String classId;
    /*
     * 计算班级人数
     */
    private String sumStudnet;
    /**
     * 班级编号，例：1590011
     */
    private String classNum;

    /**
     * 班级名称
     */
    private String className;

    /**
     * 班级类型，1:本科，2:专科，3:高职，4:成教
     */
    private String classType;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否删除，0：未删除，1：已删除
     */
    private String flag;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getSumStudnet() {
        return sumStudnet;
    }

    public void setSumStudnet(String sumStudnet) {
        this.sumStudnet = sumStudnet;
    }

    public String getClassNum() {
        return classNum;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
