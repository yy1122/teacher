package cn.eva.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger接口文档
 * Copyright (c) 2017 by 
 * @ClassName: SwaggerConfig.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月6日 下午10:09:10
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("swagger接口文档")
                .apiInfo(new ApiInfoBuilder().title("swagger接口文档")
                        .contact(new Contact("教师评价系统", "", "1396513066@qq.com")).version("1.0").build())
                .select().paths(PathSelectors.any()).build();
    }
}