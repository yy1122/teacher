package cn.eva.service.Impl.admin;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaTeacherCourseMapper;
import cn.eva.dto.SysTeacherCourseVo;
import cn.eva.entity.EvaCourse;
import cn.eva.entity.EvaTeacherCourse;
import cn.eva.example.EvaTeacherCourseExample;
import cn.eva.service.admin.SysCourseService;
import cn.eva.service.admin.SysTeacherCourseService;
import cn.eva.utils.UUIDUtils;

@Service
public class SysTeacherCourseServiceImpl implements SysTeacherCourseService {
    @Autowired
    private EvaTeacherCourseMapper teacherCourseMapper;
    @Autowired
    SysCourseService sysCourseService;

    /**
     * 根据教师tid查询课程c_id,然后渲染复选框
     * @param tid
     */
    @Override
    public ResultJosn selectBytid(String tid) {
        if (StringUtils.isBlank(tid)) {
            throw new IllegalArgumentException("tid错误");
        }
        EvaTeacherCourseExample example = new EvaTeacherCourseExample();
        example.createCriteria().andTIdEqualTo(tid);
        List<EvaTeacherCourse> list = teacherCourseMapper.selectByExample(example);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), list);
    }

    @Override
    @Transactional
    public ResultJosn selectFilterCourse(String tId) {
        if (StringUtils.isBlank(tId)) {
            throw new IllegalArgumentException("tId错误");
        }
        //查询出中间表的已选课程，教师tId，课程cId
        EvaTeacherCourseExample example = new EvaTeacherCourseExample();
        example.createCriteria().andTIdNotEqualTo(tId);//构建条件，不是当前老师所选的课程
        List<EvaTeacherCourse> listChooseCourse = teacherCourseMapper.selectByExample(example);
        List<String> collect = listChooseCourse.parallelStream().map(EvaTeacherCourse::getcId)
                .collect(Collectors.toList());

        //查询所有课程
        LayuiTableResult allCourse = sysCourseService.select(new EvaCourse(), 1, 9999);
        allCourse.getData();
        List<EvaCourse> listAllCourse = (List<EvaCourse>) allCourse.getData();

        /*迭代移除其他教师已选课程*/
        Iterator<EvaCourse> iterator = listAllCourse.iterator();//构建迭代器
        while (iterator.hasNext()) {
            EvaCourse evaCourse = iterator.next();
            //去掉中间表其他老师选的课程
            if (collect.contains(evaCourse.getcId())) {
                iterator.remove();
            }

        }
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), listAllCourse);
    }

    /**
     * 删除教师选课信息，再保存信息
     * @param sysTeacherCourseVo
     * @return
     */
    @Override
    @Transactional
    public ResultJosn add(SysTeacherCourseVo sysTeacherCourseVo) {
        if (StringUtils.isBlank(sysTeacherCourseVo.gettId())) {
            throw new IllegalArgumentException("tId错误");
        } else {
            //删除老师id所教的课程信息
            delete(sysTeacherCourseVo.gettId());

            String tId = sysTeacherCourseVo.gettId();
            String tName = sysTeacherCourseVo.gettName();
            List<String> cIds = sysTeacherCourseVo.getcIds();
            EvaTeacherCourse tc = new EvaTeacherCourse();
            EvaTeacherCourseExample example = new EvaTeacherCourseExample();
            example.createCriteria().andCIdIn(cIds);
            for (int i = 0; i < cIds.size(); i++) {
                tc.setTcId(UUIDUtils.getUUID());
                tc.settId(tId);
                tc.setcId(cIds.get(i));
                tc.settName(tName);
                teacherCourseMapper.insertSelective(tc);
            }

            return new ResultJosn(HttpStatus.OK.value(), "选课成功", null);
        }

    }

    /**
     * 管理员删除该老师的所有课程，为了后续重新插入课程
     * @param tId
     * @return
     */
    @Transactional
    public ResultJosn delete(String tId) {
        if (tId.equals("") || tId == null) {
            throw new IllegalArgumentException("删除需要cId参数错误为空");
        }
        EvaTeacherCourseExample example = new EvaTeacherCourseExample();
        example.createCriteria().andTIdEqualTo(tId);
        teacherCourseMapper.deleteByExample(example);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "操作成功");

    }

}
