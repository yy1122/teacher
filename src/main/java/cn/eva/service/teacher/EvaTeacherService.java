package cn.eva.service.teacher;

import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaScoreTehVo;
import cn.eva.entity.EvaTeacherCourseVo;

public interface EvaTeacherService {
    // 查询自己的课程信息
    public ResultJosn selectWithMeCourses(String t_id);

    public ResultJosn selectOneDetail(EvaScoreTehVo evaScoreTehVo);

    public ResultJosn selectAllTcAndalikedpMH(EvaTeacherCourseVo eTeacherCourseVo);

    public ResultJosn insertByGrade(EvaScoreTehVo evaScoreTehVo);

    public ResultJosn selectMyselfTcAndalikedpMH(EvaTeacherCourseVo eTehCVo);

    // 查询我的学生
    public ResultJosn selectsMyStudents(Integer page, Integer pageSize, String classId);
}
