package cn.eva.entity;

import java.util.Date;

/**
 * 
 * @ClassName:  AddJobInfo   
 * @Description: 外部添加任务实体
 * @author: yuyong 
 * @date:   2018年9月19日 上午8:55:15   
 *     
 * @Copyright: 2018 www.xxx.com Inc. All rights reserved. 
 * @note: 注意：本内容仅限于xxx公司内部传阅，禁止外泄以及用于其他的商业目
 */
public class AddJobInfo {
    /**
     * 名称
     */
    private String jobName;

    /**
     * 组名（批次名，例如：201809）
     */
    private String jobGroupName;

    /**
     * 统计截至日期
     */
    private Date Deadline;

    public AddJobInfo() {
        super();
    }

    public AddJobInfo(String jobName, String jobGroupName, Date deadline) {
        super();
        this.jobName = jobName;
        this.jobGroupName = jobGroupName;
        Deadline = deadline;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroupName() {
        return jobGroupName;
    }

    public void setJobGroupName(String jobGroupName) {
        this.jobGroupName = jobGroupName;
    }

    public Date getDeadline() {
        return Deadline;
    }

    public void setDeadline(Date deadline) {
        Deadline = deadline;
    }
}
