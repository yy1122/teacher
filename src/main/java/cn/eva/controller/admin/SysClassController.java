package cn.eva.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaClass;
import cn.eva.service.admin.SysClassService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "班级")
@RequestMapping("/sysClass")
@RestController
public class SysClassController {

    @Autowired
    SysClassService classService;

    @PostMapping
    @ApiOperation(value = "添加班级", notes = "添加班级")
    @PreAuthorize("hasAnyAuthority('sys:class:add')")
    public ResultJosn add(@RequestBody EvaClass evaClass) {
        ResultJosn resultJosn = null;
        resultJosn = classService.add(evaClass);
        return resultJosn;
    }

    @DeleteMapping
    @ApiOperation(value = "单、批量删除班级")
    @PreAuthorize("hasAuthority('sys:class:del')")
    public ResultJosn delete(@RequestBody List<String> deleteids) {
        ResultJosn resultJosn = null;
        resultJosn = classService.delete(deleteids);
        return resultJosn;
    }

    @PutMapping
    @ApiOperation(value = "更新班级信息")
    @PreAuthorize("hasAuthority('sys:class:update')")
    public ResultJosn update(@RequestBody EvaClass evaClass) {
        ResultJosn resultJosn = null;
        resultJosn = classService.update(evaClass);
        return resultJosn;
    }

    @GetMapping
    @ApiOperation(value = "班级列表条件模糊查询")
    @PreAuthorize("hasAnyAuthority('sys:class:query','sys:class:update','sys:class:del')")
    public LayuiTableResult selectByExample(EvaClass evaClass, @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        evaClass.setFlag("1");//检索删除和不删除的班级 1代表要删除的
        LayuiTableResult resultJosn = classService.select(evaClass, page, pageSize);
        return resultJosn;
    }

    @GetMapping(value = "/{classId}")
    @ApiOperation(value = "根据 classId 查找班级详细信息")
    @PreAuthorize("hasAnyAuthority('sys:class:query','sys:class:update','sys:class:del')")
    public ResultJosn selectByUid(@PathVariable String classId) {
        ResultJosn resultJosn = classService.selectByclassId(classId);
        return resultJosn;
    }

}
