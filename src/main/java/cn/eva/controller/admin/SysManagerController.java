package cn.eva.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dto.SysManagerAdd;
import cn.eva.entity.EvaManager;
import cn.eva.service.admin.SysManagerService;
import io.swagger.annotations.ApiOperation;

/**
 * 后台管理员管理
 * Copyright (c) 2017 by 
 * @ClassName: SysManagerController.java
 * @Description: TODO
 * 
 * @author: 余勇
 * @version: V1.0  
 * @Date: 2018年9月7日 下午6:24:56
 */
@RequestMapping("/manager")
@RestController
public class SysManagerController {

    @Autowired
    private SysManagerService sysManagerService;

    @PostMapping
    @ApiOperation(value = "添加用户", notes = "添加用户")
    @PreAuthorize("hasAuthority('sys:admin:add')")
    public ResultJosn add(@RequestBody SysManagerAdd user_add) {
        ResultJosn resultJosn = null;
        resultJosn = sysManagerService.add(user_add);
        return resultJosn;
    }

    @DeleteMapping
    @ApiOperation(value = "单、批量删除用户")
    @PreAuthorize("hasAuthority('sys:admin:del')")
    public ResultJosn delete(@RequestBody List<String> deleteids) {
        ResultJosn resultJosn = null;
        resultJosn = sysManagerService.delete(deleteids);
        return resultJosn;
    }

    @PutMapping
    @ApiOperation(value = "更新用户信息")
    @PreAuthorize("hasAuthority('sys:admin:update')")
    public ResultJosn update(@RequestBody SysManagerAdd user) {
        ResultJosn resultJosn = null;
        resultJosn = sysManagerService.updateByExample(user);
        return resultJosn;
    }

    @GetMapping
    @ApiOperation(value = "用户列表条件模糊查询")
    @PreAuthorize("hasAnyAuthority('sys:admin:query','sys:admin:update')")
    public LayuiTableResult selectByExample(EvaManager user, @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        LayuiTableResult resultJosn = sysManagerService.selectByExample(user, page, pageSize);
        return resultJosn;
    }

    @GetMapping(value = "/{uid}")
    @ApiOperation(value = "根据uid查找、包括所拥有的角色")
    @PreAuthorize("hasAnyAuthority('sys:admin:query','sys:admin:update')")
    public ResultJosn selectByUid(@PathVariable String uid) {
        ResultJosn resultJosn = sysManagerService.selectByUid(uid);
        return resultJosn;
    }
}
