package cn.eva.service.Impl.job;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.eva.dao.JobAndTriggerMapper;
import cn.eva.entity.JobAndTrigger;
import cn.eva.service.job.IJobAndTriggerService;

@Service
public class JobAndTriggerImpl implements IJobAndTriggerService {

    @Autowired
    private JobAndTriggerMapper jobAndTriggerMapper;

    /**
     * 
     * <p>Title: getJobAndTriggerDetails</p>   
     * <p>Description: 查询job信息</p>   
     * @param pageNum
     * @param pageSize
     * @return   
     * @see cn.eva.service.job.IJobAndTriggerService#getJobAndTriggerDetails(int, int)
     */
    public PageInfo<JobAndTrigger> getJobAndTriggerDetails(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<JobAndTrigger> list = jobAndTriggerMapper.getJobAndTriggerDetails();
        PageInfo<JobAndTrigger> page = new PageInfo<JobAndTrigger>(list);
        return page;
    }

}