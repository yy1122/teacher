/*
*
* EvaDepartmentMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-12
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.eva.entity.EvaDepartment;
import cn.eva.example.EvaDepartmentExample;

@Mapper
public interface EvaDepartmentMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(EvaDepartmentExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(EvaDepartmentExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String dId);

    /**
     *
     * @mbg.generated
     */
    int insert(EvaDepartment record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(EvaDepartment record);

    /**
     *
     * @mbg.generated
     */
    List<EvaDepartment> selectByExample(EvaDepartmentExample example);

    /**
     *
     * @mbg.generated
     */
    EvaDepartment selectByPrimaryKey(String dId);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") EvaDepartment record, @Param("example") EvaDepartmentExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") EvaDepartment record, @Param("example") EvaDepartmentExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(EvaDepartment record);

    /**
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(EvaDepartment record);
}