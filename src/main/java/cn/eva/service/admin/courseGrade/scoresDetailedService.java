package cn.eva.service.admin.courseGrade;

import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaScoreStu;
import cn.eva.entity.EvaScoreTeh;

public interface scoresDetailedService {

	public ResultJosn selectStuDetail(EvaScoreStu evaScoreStu);
	
	public ResultJosn selectTehDetail(EvaScoreTeh evaScoreTeh);
}
