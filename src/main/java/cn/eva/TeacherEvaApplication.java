package cn.eva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeacherEvaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeacherEvaApplication.class, args);
	}
}
