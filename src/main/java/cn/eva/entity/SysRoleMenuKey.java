/*
*
* SysRoleMenuKey.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.entity;

import java.io.Serializable;

public class SysRoleMenuKey implements Serializable {
    /** 
    * @Fields serialVersionUID : TODO 
    */
    private static final long serialVersionUID = 8711601053871477907L;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 菜单id
     */
    private Long menuId;

    public SysRoleMenuKey() {
        super();
    }

    public SysRoleMenuKey(Long roleid, Long menuid) {
        super();
        this.roleId = roleid;
        this.menuId = menuid;
    }

    /**
     * 角色id
     * @return role_id 角色id
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * 角色id
     * @param roleId 角色id
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /**
     * 菜单id
     * @return menu_id 菜单id
     */
    public Long getMenuId() {
        return menuId;
    }

    /**
     * 菜单id
     * @param menuId 菜单id
     */
    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }
}