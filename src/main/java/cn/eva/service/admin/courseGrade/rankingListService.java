package cn.eva.service.admin.courseGrade;

import cn.eva.common.ResultJosn;
import cn.eva.entity.SyscourseGrade;

public interface rankingListService {

	
	/**
	 *  查询评价分数数据 传入分页数据
     * @param page
     * @param pageSize
	 * @return
	 */
	public ResultJosn selectInfo(Integer page, Integer pageSize,SyscourseGrade syscourseGrade);
	
	
	
	
}
