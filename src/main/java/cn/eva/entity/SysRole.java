/*
*
* SysRole.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.entity;

import java.io.Serializable;
import java.util.Date;

public class SysRole implements Serializable {
    /** 
    * @Fields serialVersionUID : TODO 
    */
    private static final long serialVersionUID = -5241320247020762546L;

    /**
     * 角色id
     */
    private Long rId;

    /**
     * 角色名称
     */
    private String rName;

    /**
     * 角色描述
     */
    private String rDescription;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime = new Date();

    /**
     * 角色id
     * @return r_id 角色id
     */
    public Long getrId() {
        return rId;
    }

    /**
     * 角色id
     * @param rId 角色id
     */
    public void setrId(Long rId) {
        this.rId = rId;
    }

    /**
     * 角色名称
     * @return r_name 角色名称
     */
    public String getrName() {
        return rName;
    }

    /**
     * 角色名称
     * @param rName 角色名称
     */
    public void setrName(String rName) {
        this.rName = rName == null ? null : rName.trim();
    }

    /**
     * 角色描述
     * @return r_description 角色描述
     */
    public String getrDescription() {
        return rDescription;
    }

    /**
     * 角色描述
     * @param rDescription 角色描述
     */
    public void setrDescription(String rDescription) {
        this.rDescription = rDescription == null ? null : rDescription.trim();
    }

    /**
     * 
     * @return create_time 
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 
     * @param createTime 
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 
     * @return update_time 
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 
     * @param updateTime 
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}