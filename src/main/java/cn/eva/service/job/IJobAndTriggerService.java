package cn.eva.service.job;

import com.github.pagehelper.PageInfo;

import cn.eva.entity.JobAndTrigger;

public interface IJobAndTriggerService {

    /**
     * 
     * @Title: getJobAndTriggerDetails   
     * @Description: 查询job信息
     * @param: @param pageNum
     * @param: @param pageSize
     * @param: @return      
     * @return: PageInfo<JobAndTrigger>      
     * @throws
     */
    public PageInfo<JobAndTrigger> getJobAndTriggerDetails(int pageNum, int pageSize);
}
