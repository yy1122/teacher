/*
*
* SysUserRoleKey.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.entity;

public class SysUserRoleKey {
    /**
     * 用户id，包括学生，老师，管理员
     */
    private String uid;

    /**
     * 角色id
     */
    private Long roleId;

    public SysUserRoleKey() {
        super();
    }

    public SysUserRoleKey(String uid, Long roleId) {
        super();
        this.uid = uid;
        this.roleId = roleId;
    }

    /**
     * 用户id，包括学生，老师，管理员
     * @return uid 用户id，包括学生，老师，管理员
     */
    public String getUid() {
        return uid;
    }

    /**
     * 用户id，包括学生，老师，管理员
     * @param uid 用户id，包括学生，老师，管理员
     */
    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    /**
     * 角色id
     * @return role_id 角色id
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * 角色id
     * @param roleId 角色id
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}