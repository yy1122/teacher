package cn.eva.service.Impl.admin;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaDepartmentMapper;
import cn.eva.entity.EvaDepartment;
import cn.eva.example.EvaDepartmentExample;
import cn.eva.example.EvaDepartmentExample.Criteria;
import cn.eva.service.admin.SysDepartmentService;
import cn.eva.utils.UUIDUtils;

/**
 * 部门接口实现类
 * @author wzh
 */
@Service
public class SysDepartmentServiceImpl implements SysDepartmentService {

    @Autowired
    private EvaDepartmentMapper departmentMapper;

    /**
     * 新增部门
     */
    @Override
    @Transactional
    public ResultJosn add(EvaDepartment evaDepartment) {
        if (null != this.checkDepartmentName(evaDepartment.getdName())) {
            throw new IllegalArgumentException(evaDepartment.getdName() + "已存在！");
        }
        evaDepartment.setdId(UUIDUtils.getUUID());
        departmentMapper.insertSelective(evaDepartment);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), null);
    }

    /**
     * 单个、批量删除
     */
    @Override
    @Transactional
    public ResultJosn delete(List<String> dId) {
        EvaDepartmentExample example = new EvaDepartmentExample();
        example.createCriteria().andDIdIn(dId);
        departmentMapper.deleteByExample(example);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "删除成功");
    }

    /**
     * 修改 部门信息
     */
    @Override
    @Transactional
    public ResultJosn update(EvaDepartment evaDepartment) {
        if (StringUtils.isBlank(evaDepartment.getdId())) {
            throw new IllegalArgumentException("dId 错误!");
        }
        EvaDepartment check = this.checkDepartmentName(evaDepartment.getdName());
        if (null != check && !(check.getdId()).equals(evaDepartment.getdId())) {
            throw new IllegalArgumentException("部门名称已存在！");
        }
        departmentMapper.updateByPrimaryKeySelective(evaDepartment);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "修改成功！");
    }

    /**
     * 查询全部部门，并且可以根据 部门名称  dName 进行模糊查找
     */
    @Override
    public LayuiTableResult select(EvaDepartment evaDepartment, Integer page, Integer pageSize) {
        LayuiTableResult result = new LayuiTableResult();
        //构造条件
        EvaDepartmentExample example = new EvaDepartmentExample();
        Criteria criteria = example.createCriteria();
        // 根据输入的部门名称 dName 进行模糊查找
        if (StringUtils.isNotBlank(evaDepartment.getdName())) {
            criteria.andDNameLike("%" + evaDepartment.getdName() + "%");
        }
        // 分页查找
        PageHelper.startPage(page, pageSize);
        List<EvaDepartment> list = departmentMapper.selectByExample(example);
        PageInfo<EvaDepartment> pageInfo = new PageInfo<>(list, pageSize);
        //设置返回前台的格式和数据，如code=0/success，message，查询的总数total，和数据集合data
        result.setCode(HttpStatus.OK.value());
        result.setMessage(HttpStatus.OK.getReasonPhrase());
        result.setCount(pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    @Override
    public ResultJosn selectBydId(String dId) {
        if (StringUtils.isBlank(dId)) {
            throw new IllegalArgumentException("dId 错误！");
        }
        EvaDepartment result = departmentMapper.selectByPrimaryKey(dId);
        return new ResultJosn(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), result);
    }

    /**
     * 根据部门名称  dName 来查找是否重复
     */
    public EvaDepartment checkDepartmentName(String dName) {
        EvaDepartmentExample example = new EvaDepartmentExample();
        example.createCriteria().andDNameEqualTo(dName);
        List<EvaDepartment> list = departmentMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return (EvaDepartment) list.get(0);
    }

}
