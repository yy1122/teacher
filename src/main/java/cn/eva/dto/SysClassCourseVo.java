package cn.eva.dto;

import java.util.List;

/**
 * @@author lishaoxing
 * @date 2018/10/12 17:15
 * @desc
 */
public class SysClassCourseVo {
    private String ccId;
    private String classId;
    private String cId;
    private String cName;
    /**
     *选的单门或者多门课程
     */
    private List<String> cIds;
    private List<String> cNames;
	public String getCcId() {
		return ccId;
	}
	public void setCcId(String ccId) {
		this.ccId = ccId;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getcId() {
		return cId;
	}
	public void setcId(String cId) {
		this.cId = cId;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public List<String> getcIds() {
		return cIds;
	}
	public void setcIds(List<String> cIds) {
		this.cIds = cIds;
	}
	public List<String> getcNames() {
		return cNames;
	}
	public void setcNames(List<String> cNames) {
		this.cNames = cNames;
	}
}
