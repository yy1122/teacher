package cn.eva.controller.courseGrade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.ResultJosn;
import cn.eva.dao.EvaScoreStuMapper;
import cn.eva.dao.EvaScoreTehMapper;
import cn.eva.entity.EvaScoreStu;
import cn.eva.entity.EvaScoreTeh;
import cn.eva.example.EvaScoreStuExample;
import cn.eva.example.EvaScoreStuExample.Criteria;
import io.swagger.annotations.ApiOperation;
import cn.eva.example.EvaScoreTehExample;
import cn.eva.service.admin.courseGrade.scoresDetailedService;

/**
 * 
 * @author ANOTHER ONE
 *查看给分明细
 */
@RestController
@RequestMapping("/scoresDetailed")
public class scoresDetailedController {
	

	
	@Autowired
	private scoresDetailedService scoresDetailedService;
	
	
	@PostMapping("/scoresStuDetailed")
	@ApiOperation(value = "根据cid,tid(UU获取)查询教师的评分明细(学生)")
	public ResultJosn stuScoresDetailed(@RequestBody EvaScoreStu stuScore){
		ResultJosn resultJosn = scoresDetailedService.selectStuDetail(stuScore);
		return resultJosn;
	}
	
	
	
	@PostMapping("/scoresTehDetailed")
	@ApiOperation(value = "根据cid,tid(UU获取)查询教师的评分明细（教师）")
	public ResultJosn tehScoresDetailed(@RequestBody EvaScoreTeh tehScore){
		ResultJosn resultJosn = scoresDetailedService.selectTehDetail(tehScore);
		return resultJosn;
	}
	
	

}
