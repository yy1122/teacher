package cn.eva.service.teacher;

import java.util.List;

import cn.eva.common.ResultJosn;
import cn.eva.entity.SyscourseGrade;

public interface ExamineEvaService {

	/**
	 * 传入tid
	 * 返回该教师所教课程的学生给他的评分
	 * @param page
     * @param pageSize
	 */
	
	public ResultJosn teachercourseList(Integer page, Integer pageSize,SyscourseGrade syscourseGrade);
	
	
}
