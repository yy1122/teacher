package cn.eva.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.LayuiTableResult;
import cn.eva.common.ResultJosn;
import cn.eva.entity.EvaDepartment;
import cn.eva.service.admin.SysDepartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "部门")
@RequestMapping("/sysDepartment")
@RestController
public class SysDepartmentController {

    @Autowired
    SysDepartmentService departmentService;

    @PostMapping
    @ApiOperation(value = "添加部门", notes = "添加部门")
    @PreAuthorize("hasAnyAuthority('sys:department:add  ')")
    public ResultJosn add(@RequestBody EvaDepartment evaDepartment) {
        ResultJosn result = null;
        result = departmentService.add(evaDepartment);
        return result;
    }

    @DeleteMapping
    @ApiOperation(value = "单、批量删除部门")
    @PreAuthorize("hasAuthority('sys:department:del')")
    public ResultJosn delete(@RequestBody List<String> deleteids) {
        ResultJosn result = null;
        result = departmentService.delete(deleteids);
        return result;
    }

    @PutMapping
    @ApiOperation(value = "更新部门信息")
    @PreAuthorize("hasAuthority('sys:department:update')")
    public ResultJosn update(@RequestBody EvaDepartment evaDepartment) {
        ResultJosn result = null;
        result = departmentService.update(evaDepartment);
        return result;
    }

    @GetMapping
    @ApiOperation(value = "部门列表条件模糊查询")
    @PreAuthorize("hasAnyAuthority('sys:department:query','sys:department:update','sys:department:del','sys:teacher:add')")
    public LayuiTableResult selectByExample(EvaDepartment evaDepartment, @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        LayuiTableResult resultJosn = departmentService.select(evaDepartment, page, pageSize);
        return resultJosn;
    }

    @GetMapping(value = "/{dId}")
    @ApiOperation(value = "根据tId查找部门详细信息")
    @PreAuthorize("hasAnyAuthority('sys:department:query','sys:department:update','sys:department:del')")
    public ResultJosn selectByUid(@PathVariable String dId) {
        ResultJosn result = departmentService.selectBydId(dId);
        return result;
    }
}
