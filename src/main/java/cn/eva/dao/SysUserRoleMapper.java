/*
*
* SysUserRoleMapper.java
* Copyright(C) 2017-2020 fendo公司
* @date 2018-09-06
*/
package cn.eva.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.eva.entity.SysRole;
import cn.eva.entity.SysUserRoleKey;
import cn.eva.example.SysUserRoleExample;

@Mapper
public interface SysUserRoleMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(SysUserRoleExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(SysUserRoleExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(SysUserRoleKey key);

    /**
     *
     * @mbg.generated
     */
    int insert(SysUserRoleKey record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(SysUserRoleKey record);

    /**
     *
     * @mbg.generated
     */
    List<SysUserRoleKey> selectByExample(SysUserRoleExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") SysUserRoleKey record, @Param("example") SysUserRoleExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") SysUserRoleKey record, @Param("example") SysUserRoleExample example);

    @Select("SELECT DISTINCT b.* FROM sys_user_role a LEFT JOIN sys_role b ON a.role_id=b.r_id WHERE a.uid=#{uid}")
    List<SysRole> selectRoleByUid(String uid);
}