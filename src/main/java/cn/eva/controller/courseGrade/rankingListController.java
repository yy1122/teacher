package cn.eva.controller.courseGrade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.eva.common.ResultJosn;
import cn.eva.entity.SyscourseGrade;
import cn.eva.service.admin.courseGrade.rankingListService;


/**
 * 
 * @author ANOTHER ONE
 *
 *查看评分排名
 */
@RestController
@RequestMapping("/rankingList")
public class rankingListController {
	
	@Autowired
private rankingListService rankinglistService;

	@GetMapping("/allrankingList")
	/* @ApiOperation(value = "查询分数排名") */
	public ResultJosn selectBySidFindAllName(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize,SyscourseGrade syscourseGrade) {
		// UserUtils.getLoginUserUUID() 获取登录UUId
		ResultJosn resultJosn = rankinglistService.selectInfo(page, pageSize,syscourseGrade);
		return resultJosn;
	}
		
	
}
