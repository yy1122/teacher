package cn.eva.service;

import cn.eva.dto.LoginUser;

public interface UserService {

    LoginUser getUserInfoByAccount(String username);

}
