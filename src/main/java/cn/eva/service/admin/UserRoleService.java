package cn.eva.service.admin;

import java.util.List;

import cn.eva.common.ResultJosn;
import cn.eva.entity.SysUserRoleKey;

public interface UserRoleService {

    /**
     * 
     * @Title: delByUids   
     * @Description: 根据uids批量删除
     * @param: @param uids
     * @param: @return      
     * @return: ResultJosn      
     * @throws
     */
    ResultJosn delByUids(List<String> uids);

    /**
     * 
     * @Title: insertBatch   
     * @Description: 批量插入
     * @param: @param list
     * @param: @return      
     * @return: ResultJosn      
     * @throws
     */
    ResultJosn saveUserRoles(List<SysUserRoleKey> list);

}
