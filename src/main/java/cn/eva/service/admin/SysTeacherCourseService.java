package cn.eva.service.admin;

import cn.eva.common.ResultJosn;
import cn.eva.dto.SysTeacherCourseVo;

public interface SysTeacherCourseService {

    public ResultJosn selectBytid(String tid);

    /**
     * 管理员给老师选课 先点击老师得到tId,再根据tId得到该老师所有课程，进行复选框勾选显示，方便后续插入
     * @param tId
     * @return
     */
    public ResultJosn selectFilterCourse(String tId);

    /**
     * 删除教师选课信息，再保存信息
     * @param sysTeacherCourseVo
     * @return
     */
    public ResultJosn add(SysTeacherCourseVo sysTeacherCourseVo);

}
